import { ITeamStats } from "./tasks";

export interface ISchool {
	/* -MXX6Ikhla0sOuHrjrKi */
	team_id: string;

	/** IKHLA0SOUHRJRKI */
	team_code: string;

	/** 52.879932 */
	lat: string;

	/** 40.794455 */
	lon: string;
	/** "6801300000100" */
	kladr_id: string;

	/** "МБОУ \"НИКИФОРОВСКАЯ СОШ №1\" */
	school: string;
	/** Россия */
	country: string;
	/** Тамбовская обл" */
	region: string;

	/** "рп Дмитриевка" */
	city: string;

	/** 1500 */
	points: number;

	/** 7 */
	members_count: number;
	/** 42 */
	tasks_count: number;

	/** Бейджи школы */
	bages: IBadge[],

	badges: (string | null)[],

	inn: string,

	/** Мы лучшие в проведении школьных аукционов */
	superpower: string;
	links: {
		vk: string | null;
		instagram: string | null;
		fb: string | null;
		tiktok: string | null;
	}
}

export interface IBadge {
	/** "bage1", */
	code: string;
	/** "Участник 1-го марафона", */
	title: string;
	/** "Выполнили все задания марафона", */
	description: string;
	/** "https://thumb.tildacdn.com/tild3261-3235-4637-a433-363031356361/-/resize/320x/-/format/webp/1.png" */
	icon_url: string;
}

export interface ISchoolInfo {
	school: ISchool,
	team_users: any,
	stats: ITeamStats,
	badges?: string[]
}