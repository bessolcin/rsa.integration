import { Express, Request, Response } from 'express';
import { database as db } from 'firebase-admin';
import { USERS_KEY, USER_STATS_KEY } from '../constants';
import { StatsManager } from '../managers';
import { IPersonalStats, IUser } from '../models';

export class AnalyticsController {
	/**
	 *
	 */
	constructor(app: Express, private db: db.Database, private statsManager: StatsManager) {
		this.configureRequestHandlers(app);
	}

	private configureRequestHandlers(app: Express,) {
		app.get('/rsa/analytics/users', this.onRsaAnalyticsUsers.bind(this));
	}

	private async onRsaAnalyticsUsers(req: Request, res: Response) {

		const allTasksTask = this.statsManager.getTasks();
		const usersLimit = 500;

		const users: { [key: string]: IUser } = (await this.db.ref(USERS_KEY).limitToFirst(usersLimit).once('value')).val();

		const usersStats = await Promise.all(Object.values(users).map((u: IUser) =>
			this.db.ref(`${USER_STATS_KEY}/${u.id}`).once('value').then(d => d.val())
				.then((userStats: IPersonalStats) => ({ user: u, stats: userStats }))
		));

		const allTasks = await allTasksTask;
		const allTasksCodes = Object.keys(allTasks);
		const usersData: any[] = [];

		for (let i = 200; i < usersStats.length; i++) {
			const userStats = usersStats[i];

			const completeTasks: any = {};
			allTasksCodes.forEach(code => completeTasks[code] = !!userStats.stats?.complete_tasks && !!userStats.stats?.complete_tasks[code]);

			const userData = {
				id: userStats.user.id,
				role: userStats.user.role,
				country: userStats.user.country,
				region: userStats.user.region,
				city: userStats.user.city || userStats.user.settlement,
				school: userStats.user.school,
				team_code: userStats.user.team_code,
				team_id: userStats.user.team_id,
				created_date: userStats.user.created_date,

				complete_tasks_codes: userStats.stats?.complete_tasks_codes?.join(',') || '',
				...completeTasks,
				...userStats.stats?.points,
			};

			usersData.push(userData);
		}

		res.json(usersData).status(200);
	}
}