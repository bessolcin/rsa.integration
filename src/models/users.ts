export interface IUser {
    location: any;
    id: string;
    status: UserStatus;
    team_id: string;
    team_code: string;

    firstname: string;
    lastname: string;
    phone: string;
    email: string;

    role: string;

    country: string;
    region: string;
    city: string;
    settlement: string;
    school: string;

    created_date: number;
    created_date_offset: number;
}

export interface IUserShortInfo {
    id: string;
    firstname: string;
    lastname: string;
    role: string;
}

export enum UserStatus {
    Leader = 'leader',
    Common = 'common'
}