import { SpreadsheetManager } from '@bessolcin/coolstory.bots.lib';
import { Express, Request, Response } from 'express';
import { database as db } from 'firebase-admin';

export class CacheController {
    /**
     *
     */
    constructor(app: Express, private db: db.Database, private spreadsheetManager: SpreadsheetManager) {
        this.configureRequestHandlers(app);
    }

    private configureRequestHandlers(app: Express) {
        app.get('/cache/clean', this.onCacheClean.bind(this));
    }

    private async onCacheClean(req: Request, res: Response) {
        this.spreadsheetManager.cleanCache();
        await this.db.ref('bot/content').update({ menu: null });
        res.send('pong').status(200);
    }
}