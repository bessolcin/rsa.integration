import { database as db } from 'firebase-admin';

import { ITasksDictionary } from "../../models/tasks";
import { ISchoolInfo } from "../../models";

const ALL_BADGES: ((db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) => Promise<string | null>)[] = [
	BADGE_MARATHON_1,
	BADGE_MARATHON_2,
	BADGE_MARATHON_3,
	BADGE_MARATHON_4,
	BADGE_MARATHON_5,
	BADGE_MARATHON_6,
	BADGE_MARATHON_7,
	BADGE_MARATHON_8,
	BADGE_MARATHON_9,

	BADGE_BRONZE_TEAM,
	BADGE_SILVER_TEAM,
	BADGE_GOLD_TEAM,

	BADGE_PIONEERS,
	BADGE_EXPIRIENCED,
	BADGE_SEASON3,

	BADGE_FINALIST,

	BADGE_grand_prix_season1,
	BADGE_grand_prix_season2,
	BADGE_grand_prix_season3,

	BADGE_nomination_graduate,
	BADGE_nomination_parent,
	BADGE_nomination_student,
	BADGE_nomination_teacher
];

const ALL_BADGES_DICT: { [code: string]: ((db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) => Promise<string | null>) } = {
	'team_building': BADGE_MARATHON_1,
	'brand_is_born': BADGE_MARATHON_2,
	'event_management': BADGE_MARATHON_3,
	'empowered_place': BADGE_MARATHON_4,
	'community_manager_course': BADGE_MARATHON_5,
	'fundrising': BADGE_MARATHON_6,
	'self-realization_environment': BADGE_MARATHON_7,
	'soft_skills': BADGE_MARATHON_8,
	'success_story': BADGE_MARATHON_9,
	'hardcore_bronze': BADGE_BRONZE_TEAM,
	'hardcore_silver': BADGE_SILVER_TEAM,
	'hardcore_gold': BADGE_GOLD_TEAM,
	'pioneers': BADGE_PIONEERS,
	'experienced': BADGE_EXPIRIENCED,
	'season3': BADGE_SEASON3,
	'finalist': BADGE_FINALIST
	// 'grand_prix_season1': BADGE_grand_prix_season1,
	// 'grand_prix_season2': BADGE_grand_prix_season2,
	// 'grand_prix_season3': BADGE_grand_prix_season3,
	// 'nomination_graduate': BADGE_nomination_graduate,
	// 'nomination_parent': BADGE_nomination_parent,
	// 'nomination_student': BADGE_nomination_student,
	// 'nomination_teache': BADGE_nomination_teacher
};

/**
 * Возвращает список кодов бейджей для школы
 */
export async function getBadgesForSchool(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary): Promise<string[]> {
	const badges = [];// await Promise.all(ALL_BADGES.map(b => b(db, schoolInfo, tasks)));

	for (const code in ALL_BADGES_DICT) {
		if (Object.prototype.hasOwnProperty.call(ALL_BADGES_DICT, code)) {
			const badgeFunc = ALL_BADGES_DICT[code];
			if (!schoolInfo.school?.badges || schoolInfo.school?.badges?.indexOf(code) == -1) {
				badges.push(badgeFunc);
			}
		}
	}

	const schoolBadges = <string[]>(await Promise.all(badges.map(b => b(db, schoolInfo, tasks)))).concat(schoolInfo.school.badges).filter(b => !!b);
	// return Array.from(new Set(schoolBadges));
	return schoolBadges;
}

async function BADGE_MARATHON_1(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (!schoolInfo.stats || !isAllTasksInMarathonPerformed(1, schoolInfo, tasks)) {
		return null;
	}
	return 'team_building';
}
async function BADGE_MARATHON_2(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (!schoolInfo.stats || !isAllTasksInMarathonPerformed(2, schoolInfo, tasks)) {
		return null;
	}
	return 'brand_is_born';
}
async function BADGE_MARATHON_3(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (!schoolInfo.stats || !isAllTasksInMarathonPerformed(3, schoolInfo, tasks)) {
		return null;
	}
	return 'event_management';
}
async function BADGE_MARATHON_4(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (!schoolInfo.stats || !isAllTasksInMarathonPerformed(4, schoolInfo, tasks)) {
		return null;
	}
	return 'empowered_place';
}
async function BADGE_MARATHON_5(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (!schoolInfo.stats || !isAllTasksInMarathonPerformed(5, schoolInfo, tasks)) {
		return null;
	}
	return 'community_manager_course';
}
async function BADGE_MARATHON_6(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (!schoolInfo.stats || !isAllTasksInMarathonPerformed(6, schoolInfo, tasks)) {
		return null;
	}
	return 'fundrising';
}
async function BADGE_MARATHON_7(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (!schoolInfo.stats || !isAllTasksInMarathonPerformed(7, schoolInfo, tasks)) {
		return null;
	}
	return 'self-realization_environment';
}
async function BADGE_MARATHON_8(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (!schoolInfo.stats || !isAllTasksInMarathonPerformed(8, schoolInfo, tasks)) {
		return null;
	}
	return 'soft_skills';
}
async function BADGE_MARATHON_9(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (!schoolInfo.stats || !isAllTasksInMarathonPerformed(9, schoolInfo, tasks)) {
		return null;
	}
	return 'success_story';
}
function isAllTasksInMarathonPerformed(marathon: number, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	let marathonTasksExist = false;
	for (const key in tasks) {
		if (Object.prototype.hasOwnProperty.call(tasks, key)) {
			const task = tasks[key];
			if (task.Code.startsWith(`M${marathon}`)) {
				marathonTasksExist = true;
				const taskCode = task.Code.substr(0, 4);
				if (!schoolInfo.stats?.complete_tasks_codes?.some(code => code.startsWith(taskCode))) {
					return false;
				}
			}
		}
	}

	return marathonTasksExist;
	// const marathonTasks = new Set(Object.values(tasks).filter(t => t.Code.startsWith(`M${marathon}`)).map(t => t.Code.substr(0, 4)));
	// const teamTasks = new Set(schoolInfo.stats?.complete_tasks_codes?.filter(code => code.startsWith(`M${marathon}`)).map(code => code.substr(0, 4)));

	// return marathonTasks.size > 0 && teamTasks.size == marathonTasks.size;
}

async function BADGE_BRONZE_TEAM(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (!schoolInfo.stats || (schoolInfo.stats.points.total < 400)) {
		return null;
	}
	return 'hardcore_bronze';
}
async function BADGE_SILVER_TEAM(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (!schoolInfo.stats || (schoolInfo.stats.points.total < 800)) {
		return null;
	}
	return 'hardcore_silver';
}
async function BADGE_GOLD_TEAM(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (!schoolInfo.stats || (schoolInfo.stats.points.total < 1200)) {
		return null;
	}
	return 'hardcore_gold';
}
async function BADGE_FINALIST(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (!schoolInfo.stats || (schoolInfo.stats.points.total < 1500)) {
		return null;
	}
	return 'finalist';
}

const PIONEERS_BADGE_CODE = 'pioneers';
const RASHKA = ['Россия', 'Российская Федерация'];
// подумать как сравнивать по школам
async function BADGE_PIONEERS(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	try {
		// const s1Users = await Promise.all(Object.keys(schoolInfo.team_users).map(phone => db.ref('users').orderByChild('phone').equalTo(phone).once('value').then(d => d.val())));

		let schoolDS = await db.ref(`old/schools/s1/by_inn/${schoolInfo.school.inn}`).once('value');
		if (schoolDS.exists()) {
			return PIONEERS_BADGE_CODE;
		}

		schoolDS = await db.ref('old/schools/s1/by_name').orderByChild('school').equalTo(schoolInfo.school.school).once('value');

		if (schoolDS.exists()) {
			const searchResults = schoolDS.val();

			for (const key in searchResults) {
				if (Object.prototype.hasOwnProperty.call(searchResults, key)) {
					const sr = searchResults[key];
					if ((sr.country == schoolInfo.school.country || (RASHKA.indexOf(sr.country) > -1 && RASHKA.indexOf(schoolInfo.school.country) > -1))
						&& (sr.city == schoolInfo.school.city || sr.settlement == schoolInfo.school.city)) {
						return PIONEERS_BADGE_CODE;
					}
				}
			}
		}
	} catch (error) {
		console.warn(`Error while getting badge pioneers for school ${schoolInfo.school.team_code}`);
	}
	return null;
}

const EXPIRIENCED_BADGE_CODE = 'experienced';

async function BADGE_EXPIRIENCED(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	try {
		let schoolDS = await db.ref(`old/schools/s2/by_inn/${schoolInfo.school.inn}`).once('value');
		if (schoolDS.exists()) {
			return EXPIRIENCED_BADGE_CODE;
		}

		schoolDS = await db.ref('old/schools/s2/by_name').orderByChild('school').equalTo(schoolInfo.school.school).once('value');

		if (schoolDS.exists()) {
			const searchResults = schoolDS.val();

			for (const key in searchResults) {
				if (Object.prototype.hasOwnProperty.call(searchResults, key)) {
					const sr = searchResults[key];
					if ((sr.country == schoolInfo.school.country || (RASHKA.indexOf(sr.country) > -1 && RASHKA.indexOf(schoolInfo.school.country) > -1))
						&& (sr.city == schoolInfo.school.city || sr.settlement == schoolInfo.school.city)) {
						return EXPIRIENCED_BADGE_CODE;
					}
				}
			}
		}
	} catch (error) {
		console.warn(`Error while getting badge pioneers for school ${schoolInfo.school.team_code}`);
	}
	return null;
}

async function BADGE_SEASON3(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	if (schoolInfo.school.superpower) {
		return 'season3';
	}
	return null;
}

async function BADGE_grand_prix_season1(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	let schoolDS = await db.ref(`champions/s1/prizewinners/${schoolInfo.school.inn}`).once('value');
	if (schoolDS.exists()) {
		return 'grand_prix_season1';
	}
	return null;
}

async function BADGE_grand_prix_season2(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	let schoolDS = await db.ref(`champions/s2/prizewinners/${schoolInfo.school.inn}`).once('value');
	if (schoolDS.exists()) {
		return 'grand_prix_season2';
	}
	return null;
}

async function BADGE_grand_prix_season3(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	let schoolDS = await db.ref(`champions/s3/prizewinners/${schoolInfo.school.inn}`).once('value');
	if (schoolDS.exists()) {
		return 'grand_prix_season3';
	}
	return null;
}

async function BADGE_nomination_teacher(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	return await getNominationBadge(db, schoolInfo, 'teacher');
}

async function BADGE_nomination_student(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	return await getNominationBadge(db, schoolInfo, 'student');
}

async function BADGE_nomination_parent(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	return await getNominationBadge(db, schoolInfo, 'parent');
}
async function BADGE_nomination_graduate(db: db.Database, schoolInfo: ISchoolInfo, tasks: ITasksDictionary) {
	return await getNominationBadge(db, schoolInfo, 'graduate');
}

async function getNominationBadge(db: db.Database, schoolInfo: ISchoolInfo, role: string) {
	const nomination = `nomination_${role}`;
	let ts = await Promise.all(Object.keys(schoolInfo.team_users).map(u => db.ref(`champions/s1/${role}/${u}`).once('value').then(d => d.exists())));
	if (ts.some(t => t)) {
		return nomination;
	}
	ts = await Promise.all(Object.keys(schoolInfo.team_users).map(u => db.ref(`champions/s2/${role}/${u}`).once('value').then(d => d.exists())));
	if (ts.some(t => t)) {
		return nomination;
	}
	// ts = await Promise.all(Object.keys(schoolInfo.team_users).map(u => db.ref(`champions/s3/${role}/${u}`).once('value').then(d => d.exists())));
	// if (ts.some(t => t)) {
	// 	return nomination;
	// }
	return null;
}

// nomination_teacher
// nomination_student
// nomination_parent
// nomination_graduate
// nomination_administration
// nomination_partner

/**
 * pioneers
experienced
season3
 */