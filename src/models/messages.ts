
export interface IMessage {
    type: MessageType;
    text: string;
    buttons?: string[];
    buttons_count?: number;
    inline_buttons?: any[];
}

export interface IImageMessage extends IMessage {
    image: string;
    vk_image: string | null;
}

export interface IGifMessage extends IMessage {
    gif: string;
    tg_gif: string;
    vk_gif: string | null;
}

export interface IVideoMessage extends IMessage {
    video: string | null;
    tg_video: string | null;
    vk_video: string | null;
}

export interface ICardMessage extends IMessage {
    card: any;
}

export interface IRedirectMessage extends IMessage {
    redirect: string;
}

export interface IAsyncMessage extends IMessage {
    async_code: string;
}

export interface IFormMessage extends IMessage {
    name: string;
    save_to: string | null;
    redirect: string;
    cancel: string;
    fields: any[];
}

export enum MessageType {
    text = 'text',
    image = 'image',
    gif = 'gif',
    video = 'video',
    card = 'card',
    inline = 'inline',
    async = 'async',
    redirect = 'redirect',
    form = 'form'
}