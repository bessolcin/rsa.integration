import { Request } from 'express';
import * as HB from 'handlebars';

import { EnvConfig, IBotMenuAsyncSectionHandler, IMenu, IMenuSection, IMessage } from '@bessolcin/coolstory.bots.lib';
import { RSAEnvConfig } from '../envconfig';
import { getCompleteTrackTasksLabel, getTaskDirectionsStatsView, enrichMenuWithCompleteTasks, checkComplete, equals, gte } from '../helpers';
import { IUsersManager, PrizesManager, StatsManager } from '../managers';
import { IUserShortInfo, ICompleteTask, IStats, ITeam } from '../models';
import { TeamsRepository } from '../repositories';

export class BotMenuSectionHandler implements IBotMenuAsyncSectionHandler {
    /**
     *
     */
    constructor(
        private usersManager: IUsersManager,
        private teamsRepo: TeamsRepository,
        private statsManager: StatsManager,
        private prizesManager: PrizesManager) {

    }

    public async handle(menu: IMenu, req: Request): Promise<IMenuSection> {
        const sectionCode = req.params.code;
        const bmId = req.query.bmid as string;

        const user = await this.usersManager.getUserByBmId(bmId);
        if (!user) {
            return <IMenuSection><unknown>null;
        }
        let stats: IStats | null = await this.statsManager.getTeamAndPersonalStats(user.team_id, user.id);
        if (!stats || !stats.team_stats || !stats.personal_stats) {
            stats = await this.statsManager.updateTeamStats(user.id, user.role);
            if (!stats || !stats.team_stats || !stats.personal_stats) {
                return <IMenuSection><unknown>null;
            }
        }
        let menuSectionTemplate = menu[sectionCode];

        // Отображение статистики команды
        if (sectionCode == 'team_stats_tpl') {
            const trackLabel = getCompleteTrackTasksLabel(user.role);

            menuSectionTemplate.messages[0].text = menuSectionTemplate.messages[0].text
                .replace('{{team.members.length}}', stats.team_stats.members.length.toString())
                .replace('{{team.complete_tasks.length}}', `${stats.team_stats.complete_tasks_codes ? stats.team_stats.complete_tasks_codes.length : 0}`)
                .replace('{{team.points.total}}', `${stats.team_stats.points.total}`)
                .replace('{{team.directions}}', getTaskDirectionsStatsView(stats.team_stats));

            menuSectionTemplate.messages[1].text = menuSectionTemplate.messages[1].text
                .replace('{{user.role}}', RSAEnvConfig.ROLE_LABELS[user.role])
                .replace('{{personal.complete_tasks_count}}', (stats.personal_stats.complete_tasks_count || 0).toString())
                .replace('{{personal.current_role_complete_tasks_count}}', trackLabel ? `${trackLabel} ${stats.personal_stats.role_complete_tasks_count || '0'}` : '')
                .replace('{{personal.points.total}}', `${stats.personal_stats.role_points?.total || '0'}`);

            return menuSectionTemplate;
        }

        // Отображение участников команды
        if (sectionCode == 'team_members_tpl') {
            const teamUsers = await Promise.all(stats.team_stats.members.map(uid => this.usersManager.getUserShortInfo(uid)));
            const teamMembers = teamUsers.map(u => `${u.firstname.trim()} ${u.lastname.trim()} - ${RSAEnvConfig.ROLE_LABELS[u.role]}`).sort();

            const chunks: any[] = [];
            const chunkSize = 60;
            let step = 1;
            while (chunkSize * step <= teamMembers.length) {
                chunks.push(teamMembers.slice(chunkSize * (step - 1), chunkSize * step).join('\n'));
                step++;
            }
            chunks.push(teamMembers.slice(chunkSize * (step - 1), chunkSize * step).join('\n'));

            const newMsgTemplate = Object.assign({}, menuSectionTemplate);
            const msg = menuSectionTemplate.messages[0];

            newMsgTemplate.messages = chunks.map((s: string, i: number) => {
                const message = Object.assign({}, msg);
                message.text = (i == 0 ? message.text.replace('{{team.members}}', s) : s);
                return message;
            });

            return newMsgTemplate;
        }

        if (sectionCode == 'team_feed') {

            const teamUsers = await Promise.all(stats.team_stats.members.map(uid => this.usersManager.getUserShortInfo(uid)));

            const teamFeed = await this.statsManager.getTeamFeed(stats.team_stats.members);
            const tasks = await this.statsManager.getTasks();

            let taskRowsCount = 0;
            const messageRowLimitSplitter = '||';

            const userResultsBlocks = teamUsers.map((user: IUserShortInfo) => {
                let userTaskResults = teamFeed[user.id];
                if (userTaskResults) {
                    userTaskResults = userTaskResults.filter(tr => tasks[tr.code].CreateTicket == 'TRUE');

                    if (userTaskResults.length) {
                        taskRowsCount += userTaskResults.length;
                        const resultsRows = userTaskResults.map((taskResult: ICompleteTask) =>
                            taskResult.date ? `📆 ${taskResult.date}\n✅ ${tasks[taskResult.code].Condition}` : `✅ ${tasks[taskResult.code].Condition}`
                        );
                        const resultRow = `${RSAEnvConfig.ROLE_LABELS[user.role]} ${user.firstname} ${user.lastname}\n${resultsRows.join('\n')}${taskRowsCount > 30 ? messageRowLimitSplitter : ''}`
                        if (taskRowsCount > 30) {
                            taskRowsCount = 0;
                        }
                        return resultRow;
                    }
                }
                return null;
            });

            const teamFeedStr = userResultsBlocks.filter(s => !!s).join('\n\n').split(messageRowLimitSplitter);
            const msg = menuSectionTemplate.messages[0];

            const newMsgTemplate = Object.assign({}, menuSectionTemplate);
            newMsgTemplate.messages = teamFeedStr.map((s: string, i: number) => {
                const message = Object.assign({}, msg);
                message.text = (i == 0 ? message.text.replace('{{team.feed}}', s) : s);
                return message;
            });

            return newMsgTemplate;
        }

        if (sectionCode == 'task_M1T3L1_intro2') {
            const team = <ITeam>(await this.teamsRepo.getTeamByCode(user.team_code));
            if (team.diagnostics_tool) {
                menuSectionTemplate = menu['task_M1T3L1_intro3'];

                menuSectionTemplate.messages[0].text = menuSectionTemplate.messages[0].text
                    .replace('{{user.team_code}}', user.team_code)
                    .replace('{{user.team_code}}', user.team_code);
                return menuSectionTemplate;
            }
            return menuSectionTemplate;
        }

        // Отображение призов если больше 300 кристаллов
        if (sectionCode == 'challenge_prize') {
            if (stats && stats.team_stats.points.total >= 1500) {
                return menu['final_prizes'];
            }
            else if (stats && stats.team_stats.points.total >= 900) {
                return menu['c3_prizes'];
            }
            else if (stats && stats.team_stats.points.total >= 300) {
                return menu['c1_prizes'];
            }
            else {
                return menu['challenge_prize'];
            }
        }

        // Отображение промокодов если они есть, а если нет то дефолтное сообщение
        if (sectionCode == 'c1_prizes_promocodes') {
            let [promocodes, teamPromocodes] = await Promise.all([
                this.prizesManager.getUserPromocodes(user.id, ''),
                this.prizesManager.getTeamPromocodes(user.team_id)
            ]);

            promocodes = promocodes || { v2_mybook: '' };
            teamPromocodes = teamPromocodes || {};

            Object.assign(promocodes, teamPromocodes);

            if (promocodes.v2_mybook) {
                const menuPromocodesSectionKey = 'c1_prizes_promocodes_list';
                for (const promoType in promocodes) {
                    if (Object.prototype.hasOwnProperty.call(promocodes, promoType)) {
                        menu[menuPromocodesSectionKey].messages[0].text = menu[menuPromocodesSectionKey].messages[0].text
                            .replace(`{{promocodes.${promoType}}}`, promocodes[promoType]);
                    }
                }
                return menu[menuPromocodesSectionKey];
            }

            return menu['c1_prizes_promocodes'];
        }

        if (sectionCode == 'special_challenges') {
            const roles: string[] = ['teacher', 'student', 'exstudent', 'parent'];

            if (stats.team_stats.points.total >= 900 &&
                roles.indexOf(user.role) != -1 &&
                stats.personal_stats?.current_role_complete_tasks_count >= 5) {

                return menu[`special_challenges_${user.role}`];
            }

            return menu[sectionCode];
        }

        if (sectionCode == 'team_bages') {
            const buttons: string[] = ['🚢 Открыватель'];

            if (stats.team_stats.complete_tasks && stats.team_stats.complete_tasks['C1W1T0'] && stats.team_stats.complete_tasks['C1W1T1']) {
                buttons.push('🧭 Первые задания');
            }

            if (stats.team_stats.points.total > 0) {
                buttons.push('💎 Первые шаги');
            }
            if (stats.team_stats.points.total > 300) {
                buttons.push('🚀 К сообществу');
            }
            if (stats.team_stats.points.total >= 1500) {
                buttons.push('⚡ Заряженный компас');
            }
            if (stats.team_stats.members.length >= 4) {
                buttons.push('👨‍👩‍👧‍👦 Прекрасная команда');
            }

            buttons.push('Назад');

            menuSectionTemplate.messages.forEach((msg: IMessage) => {
                msg.buttons_count = buttons.length;
                msg.buttons = buttons;
            });
            for (let i = 1; i <= 6; i++) {
                menu[`team_bage_${i}`].messages.forEach((msg: IMessage) => {
                    msg.buttons_count = buttons.length;
                    msg.buttons = buttons;
                });
            }
        }

        if (sectionCode == 'marathon_comics1') {
            if (!stats.team_stats.marathon_outro_permissions) {
                stats = await this.statsManager.updateTeamStats(user.id, user.role);
            }


            for (const button in menuSectionTemplate.redirects) {
                if (Object.prototype.hasOwnProperty.call(menuSectionTemplate.redirects, button)) {
                    const section = menuSectionTemplate.redirects[button];

                    if (section.startsWith('marathon_M') && section.indexOf('_outro') != -1 && (!stats?.team_stats?.marathon_outro_permissions || !stats.team_stats.marathon_outro_permissions[section])) {
                        menuSectionTemplate.redirects[button] = 'marathon_comics1_uk';
                    }
                }
            }
            return menuSectionTemplate;
        }

        if (sectionCode == 'levelone_prizes') {
            const marathon = 'M7';
            const userPromocodes = await this.prizesManager.getUserPromocodes(user.id, marathon);
            
            const sectionTemplateView = HB.compile(JSON.stringify(menuSectionTemplate));

            menuSectionTemplate = JSON.parse(sectionTemplateView({
                user: {
                    promocodes: userPromocodes
                }
            }));

            return menuSectionTemplate;
        }

        if (sectionCode.startsWith('marathon_M') && sectionCode.endsWith('prizes')) {

            const marathon = sectionCode.split('_')[1];
            if (!stats.team_stats.marathon_prizes_permissions) {
                stats = await this.statsManager.updateTeamStats(user.id, user.role);
            }

            if (!stats?.team_stats?.marathon_prizes_permissions || !stats?.team_stats?.marathon_prizes_permissions[sectionCode]) {
                return menu['marathon_prizes_access_denied'];
            }

            const [userPromocodes, userMarathonCertificate] = await Promise.all([
                this.prizesManager.getUserPromocodes(user.id, marathon),
                this.prizesManager.getUserMarathonCertificate(user, marathon)
            ]);

            const sectionTemplateView = HB.compile(JSON.stringify(menuSectionTemplate));
            menuSectionTemplate = JSON.parse(sectionTemplateView({
                user: {
                    promocodes: userPromocodes,
                    marathon_certificate: userMarathonCertificate
                }
            }));

            return menuSectionTemplate;
        }

        if (sectionCode.startsWith('marathon_M')) {
            const view = HB.compile(JSON.stringify(menuSectionTemplate));

            const rendered = view({
                stats: stats
            });
            menuSectionTemplate = JSON.parse(rendered);
            return menuSectionTemplate;
        }

        const view = HB.compile(JSON.stringify(menuSectionTemplate));

        const rendered = view({
            stats: stats,
            user: user
        });
        menuSectionTemplate = JSON.parse(rendered);
        return menuSectionTemplate;
    }


}