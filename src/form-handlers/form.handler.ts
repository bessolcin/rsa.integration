import { database as db } from 'firebase-admin';

import { IFormData } from '../models/forms';
import { TasksManager } from '../managers';
import { IUser } from '../models';

export class FormHandler {

    constructor(private db: db.Database, private tasksManager: TasksManager) {
    }

    public async handle(bucketName: string, formData: IFormData, recordKey: string, user?: IUser): Promise<void> {
        switch (bucketName) {
            case 'results':
                await await this.tasksManager.handleTaskResult(formData, recordKey, user);
                break;
            case 'rf_dt_form':
                await this.tasksManager.handleDiagnosticsToolForm(formData, recordKey, user);
                break;
            case 'partnership_application':
                await this.tasksManager.handlePratnershipFormData(formData, recordKey);
                break;
            default:
                break;
        }
    }

    public async handleResultCancellation(userId: string, userRole: string, resultId: string) {
        console.info(`Updating team stats on cancel... userid=${userId}`);

        // await this.statsManager.updateTeamStats(userId, userRole);
        console.info(`Updated team stats for userid=${userId}.`);
    }

}