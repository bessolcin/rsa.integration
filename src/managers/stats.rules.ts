import { IStats, ITasksDictionary, StatsType } from '../models';

export function combinePermissions(...permissions: ((stats: IStats, tasks: ITasksDictionary) => any)[]) {
	return function (stats: IStats, tasks: ITasksDictionary) {
		return permissions.reduce((initVal, sfunc) => {
			// console.log(initVal, sfunc);
			Object.assign(initVal, sfunc(stats, tasks));
			return initVal;
		}, {});
	}
}


export function teamIsMarathonOutroOpen(marathonNumber: number, useStats: StatsType) {
	return function (stats: IStats, tasks: ITasksDictionary) {
		if (stats[useStats]?.complete_tasks_codes.filter(code => code.startsWith(`M${marathonNumber}`)).some(code => tasks[code].CreateTicket == 'TRUE')) {
			const permission: any = {};
			permission[`marathon_M${marathonNumber}_outro1`] = true;
			return permission;
		}
		return null;
	}
}

export function teamIsMarathonPrizesOpen(marathonNumber: number, useStats: StatsType) {
	return function (stats: IStats, tasks: ITasksDictionary) {
		const marathonCode = `M${marathonNumber}`;
		const mTasks = new Set(Object.keys(tasks).filter(t => t.startsWith(marathonCode) && tasks[t].Required == 'TRUE').map(c => c.substr(0, 4)));
		const mCompleteTasks = new Set(stats[useStats]?.complete_tasks_codes.map(c => c.substr(0, 4)).filter(code => mTasks.has(code)) || []);

		if (mCompleteTasks.size >= (mTasks.size - 1)) {
			const permission: any = {};
			permission[`marathon_M${marathonNumber}_prizes`] = true;
			return permission;
		}
		return null;
	}
}