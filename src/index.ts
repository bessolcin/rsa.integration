//#region EXTERNAL IMPORTS
require('dotenv').config();

import express from 'express';
import cors from 'cors';
import * as HB from 'handlebars';
import fs from 'fs/promises';

//#endregion

import {
    BotContentController, StorageBotContentManager, callBotmotherWebhook,
    ILogger, SpreadsheetManager, TelegramNotificationLogger,
    SettingsController, RefController, registerAllHelpers, StateController, DefaultUserStateHandler, UsersRepository, DefaultFormStateHandler, FormsRepository, createErrorHandler
} from '@bessolcin/coolstory.bots.lib';

import { checkComplete } from './helpers';
import { PingController, UsersController, FormsController, CacheController, BotParamsController, StatsController, BotMenuHandler, BotMenuSectionHandler, SchoolsController, TasksController, AnalyticsController } from './controllers';
import { StatsManager, PrizesManager, UsersV2Manager, IUsersManager, TasksManager, combinePermissions, teamIsMarathonOutroOpen, teamIsMarathonPrizesOpen } from './managers';
import { TeamsRepository } from './repositories';
import { FormHandler } from './form-handlers';
import { db, firebaseApp } from './firebaseApp';

import { RSAEnvConfig } from './envconfig';
import { ITeamStats, IUser, StatsType } from './models';
import { PHONE_TO_BMID_KEY, SCHOOLS_KEY, TEAMS_KEY, TEAM_STATS_KEY, USERS_KEY, USER_STATS_KEY, USER_TO_TEAM_KEY } from './constants';
import { ScreenshotService } from './services/screenshot.service';
import { HtmlToImageService } from './services';

const app = express();
app.use(cors());
app.use(express.json());

app.listen(5555, () => {
    console.log('Express app listening for port 5555');
});

const teamsRepository = new TeamsRepository(db);
const logger: ILogger = new TelegramNotificationLogger({ alertBotToken: RSAEnvConfig.ALERT_BOT_TOKEN, alertChatId: RSAEnvConfig.ALERT_CHAT_ID.toString(), serviceName: 'rsa.chatbot' });
const spreadsheetManager = new SpreadsheetManager(RSAEnvConfig.SPREADSHEET_CLIENT_SECRET);


// const botContentManager = new BotContentManager(db, {
//     botMenuSectionsLimit: RSAEnvConfig.BOT_MENU_SECTIONS_LIMIT,
//     cacheLifetime: RSAEnvConfig.BOT_MENU_CACHE_LIFETIME,
//     keywordsCacheLifetime: RSAEnvConfig.BOT_MENU_CACHE_LIFETIME,
//     keywordsSheetName: RSAEnvConfig.BOT_KEYWORDS_SHEETNAME,
//     menuSheetName: RSAEnvConfig.BOT_MENU_SHEETNAME,
//     messagesSheetName: RSAEnvConfig.BOT_MESSAGES_SHEETNAME,
// }, spreadsheetManager);


const botContentManager = new StorageBotContentManager(firebaseApp, RSAEnvConfig.BOT_MENU_CACHE_LIFETIME);

const statsManager = new StatsManager(db, spreadsheetManager,
    combinePermissions(
        teamIsMarathonOutroOpen(1, StatsType.team_stats),
        teamIsMarathonOutroOpen(2, StatsType.team_stats),
        teamIsMarathonOutroOpen(3, StatsType.team_stats),
        teamIsMarathonOutroOpen(4, StatsType.team_stats),
        teamIsMarathonOutroOpen(5, StatsType.team_stats),
        teamIsMarathonOutroOpen(6, StatsType.team_stats),
        teamIsMarathonOutroOpen(7, StatsType.team_stats)),
    combinePermissions(
        teamIsMarathonPrizesOpen(1, StatsType.team_stats),
        teamIsMarathonPrizesOpen(2, StatsType.team_stats),
        teamIsMarathonPrizesOpen(3, StatsType.team_stats),
        teamIsMarathonPrizesOpen(4, StatsType.team_stats),
        teamIsMarathonPrizesOpen(5, StatsType.team_stats),
        teamIsMarathonPrizesOpen(6, StatsType.team_stats),
        teamIsMarathonPrizesOpen(7, StatsType.team_stats))
);
const tasksManager = new TasksManager(db, statsManager, teamsRepository);
const usersManager: IUsersManager = new UsersV2Manager(db, teamsRepository, statsManager);
const prizesManager = new PrizesManager(db);

const screenshoService = new ScreenshotService({});
const htmlToImageService = new HtmlToImageService(screenshoService);

const formHandler = new FormHandler(db, tasksManager);

const botmenuHandler = new BotMenuHandler(db, logger, usersManager, statsManager, prizesManager);
const botmenuSectionHandler = new BotMenuSectionHandler(usersManager, teamsRepository, statsManager, prizesManager);

const pingController = new PingController(app);
const cacheController = new CacheController(app, db, spreadsheetManager);
const botContentController = new BotContentController(app, botContentManager, logger, botmenuHandler, botmenuSectionHandler);
botContentController.replaceFirstname = false;

const formsController = new FormsController(app, db, spreadsheetManager, usersManager, teamsRepository, formHandler);
const usersController = new UsersController(app, db, logger, usersManager, teamsRepository, statsManager);
const botParamsController = new BotParamsController(app, db);
const refController = new RefController(app, db);
const botSettingsController = new SettingsController(app, db);

// const usersRepository = new UsersRepository(db);
// const defaultUserStateHandler = new DefaultUserStateHandler()
const formsRepository = new FormsRepository(db);
const defaultFormStateHandler = new DefaultFormStateHandler(formsRepository);
const botStateController = new StateController(app, logger);
botStateController.registerFormStateHandler(defaultFormStateHandler);

const statsController = new StatsController(logger, app, db, statsManager);
const schoolsController = new SchoolsController(app, db, statsManager, htmlToImageService);
const tasksController = new TasksController(app, db, logger, tasksManager);
const analyticsController = new AnalyticsController(app, db, statsManager);

registerAllHelpers();
HB.registerHelper('checkComplete', checkComplete);

app.use(createErrorHandler(logger));


async function migrate() {

    // const buffer = await fs.readFile('schools_database.json');
    // const schools: any[] = JSON.parse(buffer.toString());

    try {

        // for (let i = 0; i < pages.length; i++) {
        //     const n = pages[i];
        //     const url = `https://russiaedu.ru/_ajax/schools?edu_school_filter%5BschoolName%5D=&edu_school_filter%5Bregion%5D=&edu_school_filter%5Bdistrict%5D=&edu_school_filter%5BformType%5D=&edu_school_filter%5BownershipType%5D=&edu_school_filter%5B_token%5D=hbuW6naCaAgBOdL5nB-48lToOK-T54SZrep_Nsd6msk&pp=100&pageNumber=${n}&direction=`;
        //     const r: any = await got.get(url).json();
        //     schools.push(...r.eduSchools);
        // }

        // const pagesResult = await Promise.all(pages.map(n => {
        //     const url = `https://russiaedu.ru/_ajax/schools?edu_school_filter%5BschoolName%5D=&edu_school_filter%5Bregion%5D=&edu_school_filter%5Bdistrict%5D=&edu_school_filter%5BformType%5D=&edu_school_filter%5BownershipType%5D=&edu_school_filter%5B_token%5D=hbuW6naCaAgBOdL5nB-48lToOK-T54SZrep_Nsd6msk&pp=100&pageNumber=${n}&direction=`;
        //     return got.get(url).json();
        // })
        // );

        // pagesResult.forEach((r: any) => {
        //     schools.push(...r.eduSchools);
        // });

        // await fs.writeFile('schools_database.json', JSON.stringify(schools));

    } catch (error) {
        console.log(error);

    }

    // const comms = (await db.ref(`diagnostics_tool/communities`).orderByChild('email').equalTo('').once('value')).val();
    // const allForms = (await db.ref(`forms/rf_dt_form`).once('value')).val();
    // const results: any = {};
    // for (const key in allForms) {
    //     if (Object.prototype.hasOwnProperty.call(allForms, key)) {
    //         const form = allForms[key];
    //         if (comms[form.team_code]) {
    //             if (!results[form.team_code]) {
    //                 results[form.team_code] = form;
    //             }
    //         }
    //     }
    // }
    // const a = await Promise.all(Object.values(results).map(cid => db.ref(`forms/rf_dt_form`)
    //     .orderByChild('team_code')
    //     .equalTo(cid)
    //     .once('value').then(d => Object.values(d.val())[0])
    // ));

    // const update: any =  {};
    // for (const key in results) {
    //     if (Object.prototype.hasOwnProperty.call(results, key)) {
    //         const res = results[key];
    //         const email = (await db.ref(`v3/users/${res.user_id}/email`).once('value')).val();
    //         update[`diagnostics_tool/communities/${res.team_code}/email`] = email;
    //     }
    // }

    // await db.ref().update(update);
    // console.log(results, update);

    // let results = (await db.ref(`backup/forms/results_30082021`).orderByChild('team_id').equalTo('-MIZ3HAuZnLdrH5tcOzr').once('value')).val();
    // const bmids = await teamsRepository.getTeamBmIds(tid, '');
    // allBmIds.push(...bmids);

    // Достает все идшники по телефонам
    // const bmids = await Promise.all(allPhones.map((phone: string) => db.ref(`${PHONE_TO_BMID_KEY}/${phone}`)
    //     .once('value')
    //     .then((bmIds: database.DataSnapshot) => {
    //         const phoneToBmIds = bmIds.val();
    //         const phoneBmIds = [];
    //         for (const bmId in phoneToBmIds) {
    //             if (Object.prototype.hasOwnProperty.call(phoneToBmIds, bmId)) {
    //                 phoneBmIds.push(bmId)
    //             }
    //         }
    //         return phoneBmIds;
    //     })));

    // bmids.forEach(bm => allBmIds.push(...bm));

    // console.log(allBmIds);

    // const users: any = (await db.ref(PHONE_TO_BMID_KEY).once('value')).val();
    // const s = new Set();
    // for (const phone in users) {
    //     if (Object.prototype.hasOwnProperty.call(users, phone)) {
    //         const bmids = users[phone];
    //         Object.keys(bmids).forEach(k => s.add(k));
    //     }
    // }

    // console.log(Object.keys(users), s.values.length);

    // const bmidsList = await Promise.all(Object.values(users).map(u => db.ref(`${PHONE_TO_BMID_KEY}/${u.phone}`).once('value').then(d => Object.keys(d.val() || {}))));
    // const allBmIds: string[] = [];
    // bmidsList.forEach(b => allBmIds.push(...b));
    // await fs.writeFile('allbmids.txt', allBmIds.join('\n'));


    // const bmids = await Promise.all(Object.keys(users).map(id => db.ref(`${PHONE_TO_BMID_KEY}/${id}`).once('value').then(d => users[id]['bmid'] = d.exists())));
    // console.log(Object.keys(users).length);
    // console.log(bmids.length);
    // console.log(bmids.filter(v => !!v).length);

    // const update: any = {};
    // const date = new Date(2021, 10, 1);
    // let users = (await db.ref('v3/users').orderByChild('created_date').startAt(date.getTime()).once('value')).val();
    // const ud = Object.values(users).map((u: any) => { u.created_date = new Date(u.created_date); return u; })

    // let schools = Object.values(users).map((userData: any) => ({
    //     city: userData.city || null,
    //     country: userData.country || null,
    //     location: userData.location || null,
    //     region: userData.region || null,
    //     school: userData.school || null,
    //     settlement: userData.settlement || null
    // }));

    // let schoolsByInn: any = {};
    // let schoolsByName: any = {};

    // for (let i = 0; i < schools.length; i++) {
    //     const s = schools[i];
    //     if (s.location?.school?.inn) {
    //         if (!schoolsByInn[s.location.school.inn]) {
    //             schoolsByInn[s.location.school.inn] = s;
    //         }
    //         continue;
    //     }
    //     // const schoolName = [s.country, s.region, (s.city || s.settlement), s.school].filter(s => !!s).join('_');

    //     schoolsByName[<string>db.ref().push().key] = s;
    // }
    // update['old/schools/s1/by_inn'] = schoolsByInn;
    // update['old/schools/s1/by_name'] = schoolsByName;


    // schoolsByInn = {};
    // schoolsByName = {};
    // users = (await db.ref('v2_users').once('value')).val();

    // schools = Object.values(users).map((userData: any) => ({
    //     city: userData.city || null,
    //     country: userData.country || null,
    //     location: userData.location || null,
    //     region: userData.region || null,
    //     school: userData.school || null,
    //     settlement: userData.settlement || null
    // }));

    // for (let i = 0; i < schools.length; i++) {
    //     const s = schools[i];
    //     if (s.location?.school?.inn) {
    //         if (!schoolsByInn[s.location.school.inn]) {
    //             schoolsByInn[s.location.school.inn] = s;
    //         }
    //         continue;
    //     }
    //     // const schoolName = [s.country, s.region, (s.city || s.settlement), s.school].filter(s => !!s).join('_');

    //     schoolsByName[<string>db.ref().push().key] = s;
    // }

    // update['old/schools/s2/by_inn'] = schoolsByInn;
    // update['old/schools/s2/by_name'] = schoolsByName;

    // console.log(schools);
    //     const team = await teamsRepository.getTeamByCode('YMR1UV');
    //     // const testU = <IUser>await usersManager.getUserByPhone('79166525064');
    //     // const testT = await teamsRepository.getTeamByCode(testU?.team_code);
    //     // const testOrg = (await db.ref(`${ORGANIZATIONS_KEY}/${testT.org_id}`).once('value')).val();
    //     // const testSc = (await db.ref(`${SCHOOLS_KEY}/${testU.team_code}`).once('value')).val();

    //     // const userOrgData = {
    //     //     city: testU.city || null,
    //     //     country: testU.country || null,
    //     //     location: testU.location || null,
    //     //     region: testU.region || null,
    //     //     school: testU.school || null,
    //     //     settlement: testU.settlement || null
    //     // };

    //     // const currUId = '79166525060';
    //     // const currU = <IUser>await usersManager.getUserByPhone(currUId);
    //     // const currT = await teamsRepository.getTeamByCode(currU?.team_code);

    // Достает все сертификаты
    const marathons = ['M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7'];
    // const allTMembers = (await db.ref(`${USER_TO_TEAM_KEY}`).orderByValue().equalTo('-MpkQ4bz7s7KieJgq2B_').once('value')).val();
    const allTMembers = {
        '79526906158': '1',
        // '79526907159': '2'
    };
    const usersc = await Promise.all(Object.keys(allTMembers).map(uid => <IUser><unknown>usersManager.getUserByPhone(uid).then(u => {

        return Promise.all(marathons.map(m => prizesManager.getUserMarathonCertificate(<IUser>u, m).then(cid => { (<any>u)[m] = cid; return cid; }))).then(_ => u)
    })));
    var url = 'https://concept.rybakovfoundation.ru/api/api/certificate/';
    const certStr = usersc.filter(u => !!u).map((u: any) => [u.firstname, u.lastname, u.phone, url + u.M1, url + u.M2, url + u.M3, url + u.M4, url + u.M5, url + u.M6, url + u.M7].join(',')).join('\n');
    fs.writeFile('certificates.csv', certStr)

    // console.log(usersc);


    // const results = (await db.ref(`forms/results`).orderByChild('timestamp').startAt(1649859311128).once('value')).val();
    // const users: any = {};
    // Object.values(results).filter((r: any) => r.form_name.startsWith('M8')).forEach((r: any) => users[r.user_id] = r.role);

    // for (const uid in users) {
    //     if (Object.prototype.hasOwnProperty.call(users, uid)) {
    //         const role = users[uid];
    //         await statsManager.updateTeamStats(uid, role);
    //     }
    // }
    // console.log(users);


    //     // await db.ref().update(update);

    //     // await statsManager.updateTeamStats('79166525060', 'student');

    // Достать всех финалистов
    // const tstats: Record<string, ITeamStats> = (await db.ref(TEAM_STATS_KEY).orderByChild('points/total').startAt(1500).once('value')).val();
    // const schools = await Promise.all(Object.keys(tstats).map(tid =>
    //     db.ref(`${TEAMS_KEY}/${tid}/referral_code`).once('value').then(d => d.val())
    //         .then(refcode => db.ref(`${SCHOOLS_KEY}/${refcode}`).once('value').then(d => d.val())))
    // );

    // await Promise.all(schools.map(s => prizesManager.getSchoolMarathonCertificate(s, 'finalist').then(id => s.certid = id)));

    // console.log(schools);
    // const usersStats: string[] = [];
    // Object.values(tstats).forEach(t => usersStats.push(...t.members));

    // const ustats = (await db.ref(USER_STATS_KEY).orderByChild('role_points/total').startAt(135).once('value')).val();
    // const teamStats = Object.values(tstats);
    // const usersStats = Object.keys(ustats).filter(uid => teamStats.some((t: any) => t.members.indexOf(uid) > -1));
    const allBmIds: string[] = [];
    // // Достает все идшники по телефонам
    // const bmids = await Promise.all(usersStats.map((phone: string) => db.ref(`${PHONE_TO_BMID_KEY}/${phone}`)
    //     .once('value')
    //     .then((bmIds: database.DataSnapshot) => {
    //         const phoneToBmIds = bmIds.val();
    //         const phoneBmIds = [];
    //         for (const bmId in phoneToBmIds) {
    //             if (Object.prototype.hasOwnProperty.call(phoneToBmIds, bmId)) {
    //                 phoneBmIds.push(bmId)
    //             }
    //         }
    //         return phoneBmIds;
    //     })));

    // bmids.forEach(bm => allBmIds.push(...bm));
    // console.log(allBmIds.join('\n'));
    // await fs.writeFile('finalists.txt', allBmIds.join('\n'));

    // console.log(usersStats);


    // const promocodes = (await db.ref(`v3/promocodes/partners/levelone`).orderByValue().equalTo(true).once('value')).val();
    // console.log(Object.keys(promocodes));
}

// migrate().then(() => console.log('Ok'));

module.exports = {
    app: app
};

/**
 *
 * const testUser = {
    Name: 'Бессольцын Филипп',
    Phone: '89166525061',
    Email: 'asdf@asdf.com',
    role: 'Ученик',
    country: 'Россия',
    region: 'Москва',
    city: 'Москва',
    School: '1234',
    tranid: '1925321:1141279702',
    formid: 'form228788899'
};

usersManager.createUser(testUser).then(() => {
    return usersManager.createUser(testUser);
}).then();
 */