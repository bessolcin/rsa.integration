import { Express, Request, Response } from 'express';
import { database } from 'firebase-admin';

import { formatPhoneNumber, ILogger } from '@bessolcin/coolstory.bots.lib';

import { PHONE_TO_BMID_KEY, TEAMS_KEY, USERS_KEY } from '../constants';
import { RSAEnvConfig } from '../envconfig';

import { IUsersManager, StatsManager } from '../managers';
import { TeamsRepository } from '../repositories';

export interface IBindUserRequest {
    phone: string;
    botmotherId: string;
}

export class UsersController {

    /**
     *
     */
    constructor(
        app: Express,
        private db: database.Database,
        private logger: ILogger,
        private usersManager: IUsersManager,
        private teams: TeamsRepository,
        private statsManager: StatsManager) {
        this.configureRequestHandlers(app);
    }

    private configureRequestHandlers(app: Express) {
        app.get('/users/migrate', this.onUsersMigrate.bind(this));
        app.get('/users/:phone', this.onUsersByPhone.bind(this));
        app.get('/users/delete/:phone', this.onUsersDelete.bind(this));
        app.get('/users/bmid/:bmId', this.onUsersByBmId.bind(this));
        app.get('/users/promocodes/distribute', this.onUsersPromocodesDistribute.bind(this));

        app.get('/teams/:code', this.onTeamsCode.bind(this));


        app.post('/users/bind', this.onUsersBind.bind(this));
        app.post('/users/notify', this.onUsersNotify.bind(this));
        app.post('/users/join', this.onUsersJoin.bind(this));
        app.post('/users/join/byphone', this.onUsersJoinByPhone.bind(this));
        app.post('/users/updatestats/:phone', this.onUsersUpdateStatsByphone.bind(this));

        app.post('/users', this.onUsersCreate.bind(this));
        // app.post('/users', this.onUsersCreate.bind(this));

        app.put('/users/:phone', this.onUpdateUserByPhone.bind(this));
    }

    private async onUsersCreate(req: Request, res: Response) {
        try {
            if (req.body.test) {
                res.json({ result: 'ok' });
                return;
            }

            const userData = req.body;
            await this.usersManager.createUser(userData);
        } catch (error) {
            console.error('Error while creating user', JSON.stringify({ url: req.url, error: error, userData: req.body }));

            await this.logger.alert(`
            Error while creating user
            ${(<any>error).message}
            url: ${req.originalUrl}`);

            return res.status(400).json({ result: 'error', error: (<any>error).message });
        }
        return res.json({ result: 'ok' });
    }

    private async onUsersBind(req: Request, res: Response) {
        try {
            const bindRequest = req.body;
            bindRequest.phone = formatPhoneNumber(bindRequest.phone.replace('+8', '8')).replace('+', '');

            const user = await this.usersManager.bindUserWithBmId(bindRequest.phone, bindRequest.bmid);

            res.json({ user: user });
        } catch (error) {
            console.error('Error while binding user with bmId', { url: req.url, error: error, bindRequest: req.body });

            await this.logger.alert(`
            Error while binding user with bmId
            url: ${req.originalUrl}`);
        }
    }

    private async onUsersNotify(req: Request, res: Response) {
        try {
            const bmId = req.body.bmid;

            await this.usersManager.notifyUserLogin(bmId, req.body.platform);
        } catch (error) {
            console.error('Error while notifying user with bmId', { url: req.url, error: error, bindRequest: req.body });

            await this.logger.alert(`
            Error while binding user with bmId
            url: ${req.originalUrl}`);
        }

        res.json({ result: 'ok' });
    }

    private async onUsersByPhone(req: Request, res: Response) {
        let user = null;
        try {
            user = await this.usersManager.getUserByPhone(req.params.phone);
            const originalTeam = user ? (await this.db.ref(TEAMS_KEY).orderByChild('leader').equalTo(user.phone || user.id).once('value')).val() : null;
            const userBmIds = user ? (await this.db.ref(`${PHONE_TO_BMID_KEY}/${req.params.phone}`).once('value')).val() : null;

            const teamStats = user ? await this.statsManager.getTeamAndPersonalStats(user.team_id, user.id) : null;

            const canceledTasks = user ? (await this.db.ref('forms/results').orderByChild('user_id').equalTo(req.params.phone).once('value')).val() : null;

            res.setHeader('Access-Control-Allow-Origin', '*');
            res.json({ user: user, team_stats: teamStats, origin_team: originalTeam, bmids: userBmIds, all_tasks: canceledTasks });
        } catch (error) {
            console.error('Error while getting user', error, user);
        }
    }

    private async onUsersByBmId(req: Request, res: Response) {
        const user = await this.usersManager.getUserByBmId(req.params.bmId);
        const originalTeam = user ? (await this.db.ref(TEAMS_KEY).orderByChild('leader').equalTo(user.phone).once('value')).val() : null;
        const teamStats = user ? await this.statsManager.getFullStats(user?.team_id, user?.id, user?.role) : null;

        res.json({ user: user, team_stats: teamStats, origin_team: originalTeam });
    }

    private async onUsersDelete(req: Request, res: Response) {
        await this.usersManager.deleteUser(req.params.phone);

        res.send('User deleted');
    }

    private async onUsersJoin(req: Request, res: Response) {
        try {
            const joinRequest: any = req.body;
            console.info(`Joining user with bmid ${joinRequest.bmid} with team ${joinRequest.team_code}`);

            const result = await this.usersManager.joinUsersTeam(
                joinRequest.bmid,
                joinRequest.team_code);

            return res.json({ result: result });
        } catch (error) {
            console.error('Error while joining team', { url: req.url, error: error });

            await this.logger.alert(`
            Error while joining team
            url: ${req.originalUrl}`);
        }
        return res.status(400);
    }

    private async onUsersJoinByPhone(req: Request, res: Response) {
        try {
            const joinRequest: any = req.body;
            console.info(`Joining user with bmid ${joinRequest.bmid} with team ${joinRequest.team_code}`);

            const result = await this.usersManager.joinUsersTeamByPhone(
                joinRequest.phone,
                joinRequest.team_code);

            return res.json({ result: result });
        } catch (error) {
            console.error('Error while joining team', { url: req.url, error: error });

            await this.logger.alert(`
            Error while joining team
            url: ${req.originalUrl}`);
        }
        return res.status(400);
    }

    private async onUsersUpdateStatsByphone(req: Request, res: Response) {
        try {
            const phone = req.params.phone;
            const user = await this.usersManager.getUserByPhone(phone);
            if (user) {
                const stats = await this.statsManager.updateTeamStats(user.id, user.role);
                return res.json(stats);
            }
        } catch (error) {
            console.error('Error while joining team', { url: req.url, error: error });

            await this.logger.alert(`
            Error while joining team
            url: ${req.originalUrl}`);
        }
        return res.status(400);
    }

    private async onUsersPromocodesDistribute(req: Request, res: Response) {

        console.info('Updaing mybook promocodes...');
        const mybookUpdate = await this.distributePromocodesForCompany('v2_mybook');
        console.info('Updated mybook promocodes.');
        console.info('Updaing foxford promocodes...');
        const foxfordUpdate = await this.distributePromocodesForCompany('foxford');
        console.info('Updated foxford promocodes.');

        console.info('Updaing arzamas promocodes...');
        const arzamasUpdate = await this.distributePromocodesForCompany('arzamas');
        console.info('Updated arzamas promocodes.');

        res.json({ mybook: mybookUpdate, foxford: foxfordUpdate, arzamas: arzamasUpdate });
    }

    private async distributePromocodesForCompany(companyCode: string) {
        //res.json('No applications');
        const applications = await this.db.ref(`forms/promocodes_${companyCode}`).orderByChild('processed').equalTo(null).once('value');

        if (!applications.numChildren()) {
            return null;
        }
        const availableCompanyPromocodes = (await this.db.ref(`promocodes/${companyCode}`).orderByValue().limitToFirst(applications.numChildren()).once('value')).val();

        const companyPromocodes = Object.keys(availableCompanyPromocodes);

        const promocodesUpdate: any = {};
        const apps: any[] = [];
        applications.forEach((app: database.DataSnapshot) => {
            if (app.key) {
                const userId = app.child('user_id').val();
                apps.push({
                    appId: app.key,
                    userId: userId
                });
            }
        });

        const exists = await Promise.all(apps.map(a => {
            const pKey = `promocodes/by_user/${a.userId}/${companyCode}`;
            return this.db.ref(pKey).once('value')
                .then((d: database.DataSnapshot) => { return { key: pKey, value: d.exists() }; });
        }));
        const userCompanyPromocodeExists = exists.reduce((o: any, kv: any) => { o[kv.key] = kv.value; return o; }, {});

        let promocodeIndex = 0;
        for (let i = 0; i < apps.length; i++) {
            const app = apps[i];
            promocodesUpdate[`forms/promocodes_${companyCode}/${app.appId}/processed`] = true;
            const userCompanyPromocodeKey = `promocodes/by_user/${app.userId}/${companyCode}`;

            if (!promocodesUpdate[userCompanyPromocodeKey] && !userCompanyPromocodeExists[userCompanyPromocodeKey]) {
                const promocode = companyPromocodes[promocodeIndex];
                promocodesUpdate[userCompanyPromocodeKey] = promocode;
                promocodesUpdate[`promocodes/${companyCode}/${promocode}`] = true;
                promocodeIndex++;
            }
        }

        await this.db.ref().update(promocodesUpdate);

        return promocodesUpdate;
    }

    private async onUpdateUserByPhone(req: Request, res: Response) {
        try {
            const userUpdateRequest = req.body;
            const phone = req.params.phone;
            const user = await this.usersManager.getUserByPhone(phone);

            if (user) {
                const update: any = {};
                if (userUpdateRequest.firstname) {
                    update[`${USERS_KEY}/${phone}/firstname`] = userUpdateRequest.firstname;
                }
                if (userUpdateRequest.lastname) {
                    update[`${USERS_KEY}/${phone}/lastname`] = userUpdateRequest.lastname;
                }
                if (userUpdateRequest.role && !!RSAEnvConfig.ROLE_LABELS[userUpdateRequest.role]) {
                    update[`${USERS_KEY}/${phone}/role`] = userUpdateRequest.role;
                }

                await this.db.ref().update(update);
                if (userUpdateRequest.role && !!RSAEnvConfig.ROLE_LABELS[userUpdateRequest.role]) {
                    await this.statsManager.updateTeamStats(phone, userUpdateRequest.role);
                }
                return res.json({ result: 'ok' });
            }
            else {
                return res.status(400).json({ result: 'error', error: 'User not found' });
            }

        } catch (error) {
            return res.status(400).json({ result: 'error', error: error });
        }
    }

    private async onTeamsCode(req: Request, res: Response) {
        let team = null;
        try {
            team = await this.teams.getTeamByCode(req.params.code);

            res.setHeader('Access-Control-Allow-Origin', '*');
            res.json(team || null);
        } catch (error) {
            console.error('Error while getting team', error, team);
            res.status(400).send((<any>error).message);
        }
    }

    private async onUsersMigrate(req: Request, res: Response) {
        // const date = new Date(2021, 1, 10);
        // const users = (await this.db.ref('users').orderByChild('created_date').startAt('2021-02-01T00:00:00.000Z').once('value')).val();
        // const dups = (await this.db.ref('duplicates/users').orderByChild('created_date').startAt('2021-02-01T00:00:00.000Z').once('value')).val();

        // const filtered = (await this.db.ref('duplicates/users').orderByChild('phone').equalTo('79166525060').once('value')).val();
        // const empTeamIds = ["0BIKGB1Q4FFUXDL", "0UTJ8XB4MOVMNYB", "478XLQOOW7EEY5S", "48X0NWVUW14E1YT", "4BAFBW5NEQG3PMK", "5C0MZFEKATYKAX1", "5EV5U3V2XWJHVTP", "5FJFCDARRHIX1C3", "68OQP3MKQX9ZC5N", "6DWYUZDTWVWG5OH", "6PQG4GFPBZVZSZX", "7KU2LLWPPGELQGE", "7NB2S9ZJXYWI6MI", "80NBMMHGUACSZYP", "9ET1OUV94EEQ2WK", "9WMUS68RJXK24MO", "A253HIZMXAJLJGS", "ADPRDLZKPYCIBVX", "AQWE7SGKQIMW5XF", "AYAP2QHWGSKTSJQ", "BEDWKA6LHKPZ8KK", "BEMMX2A7Q7JE5NK", "BZXOU2O9TPMTHYQ", "C8SVNNCCQCPHAU8", "CNUBQEN3YXRO219", "CWOHVNC1RJQ9W6B", "DBZCIWF6O7YAT5U", "DE6XOE0CRPEIOHD", "DTY2H2MKW57NKWV", "E1UK2WKXXVQFVXK", "E89CXW4VIBHD0QB", "EPY7QWVOYENS9B6", "FA97B4TBGIMK75R", "FQGKCSGGT5SK3MG", "FUV6YHTWQ2ANVHX", "G3WTHLI8VJHEPXN", "GHDRFRO1ONVYWPQ", "GYX4MLMBPCXBKMC", "H54QYUVAWEZKAI0", "HC5JKJITIQODBK3", "HGISE1IB3AJWXCN", "HHCYKOYPOKWLNSI", "HJERA5IEGKI60WR", "HNLW6TUVMKXWSYS", "I32IJHYTDMKRDYK", "IPCQMHROF0S8IJA", "JIP8IJ93HAIO3KM", "JJHCAXRCKFAMTJA", "K1A3TRCJLJYLODF", "KITVDI4W3DVAA2U", "KSNUDC3AQQNHEHL", "KXUTQ4ALUZXJ4MN", "L4S2DUQ2QRQX54J", "LFPSNPX5VDNJABI", "NEOPJ7JKV2YBXPP", "NKB3VDSI9Q5HSUJ", "NKJXVTJ8B61XOGM", "NPBNZN2DFGNPXZ6", "NU3UBYYS9TIMVLG", "NXHPWZRV7QMCOC1", "P7TMFCOIL4RJAG7", "PA7J4Y5BQIT89WL", "PZHSAPCHWWEABIG", "QMDXGJS0FKIJJQP", "QX262TYMBQBZZ6U", "RIWXYML4UNPF3BR", "RRCXIADNOW8R9WY", "RYGPPIPESOA49VF", "ULDE64GKVCTQ03X", "UVLOJZOMULYSAQI", "UYHDRMV7SIDLZI7", "V0Z2HFNK3Z7PYNF", "VN2QEHAK4GG0RSM", "VRTRRD9NXJVZ2VD", "W9ZXSRCSIN27HZU", "WG5HXXAVZQSZBFI", "XADTL7SPDP1TDQI", "XASKJQCRMDSGRQ8", "XLJ2DV53ITFANPC", "YCIAX7N9NHTB1ZI", "YUBLGR1XJNOMGTZ", "ZJDHUY7TEM9AMUX"];
        // const leaders = await Promise.all(
        //     empTeamIds.map((tid: string) => this.db.ref(`v2_teams`).orderByChild('referral_code').equalTo(tid).once('value').then(ds => (<any>Object.values(ds.val())[0]).leader).then(id => this.usersManager.getUserByPhone(id)))
        // );
        const users = (await this.db.ref('v2_users').orderByChild('team').once('value')).val();

        let filtered = Object.values(users).filter((u: any) => !!u.team && (u.team.length != 15) && u.team.startsWith('?')); //
        // filtered = filtered.filter((u: any) => !u.country);

        const teamIds = await Promise.all(
            filtered.map((u: any) => this.db.ref(`v2_teams`)
                .orderByChild('leader')
                .equalTo(u.id)
                .once('value')
                .then((ds: database.DataSnapshot) => ds.val()))
            // filtered.map((u: any) => this.db.ref(`v2_user_to_team/${u.id}`)
            //     .once('value')
            //     .then((ds: database.DataSnapshot) => ds.val()))
        );

        // const teamIds = await Promise.all(
        //     filtered.map((u: any) => u.team.split('?')[0]).map((t: any) => this.db.ref(TEAMS_KEY).orderByChild('referral_code').equalTo(t).once('value').then(ds => ds.val()))
        // );

        const teams = await Promise.all(teamIds.map((tid: string) => this.db.ref(`v2_teams/${Object.keys(tid)[0]}`).once('value').then(ds => { return Object.assign({}, ds.val(), { id: Object.keys(tid)[0] }) })));
        // const teams = await Promise.all(teamIds.map((tid: string) => this.db.ref(`v2_teams/${tid}`).once('value').then(ds => { return Object.assign({}, ds.val(), { id: tid }) })));
        const teamOvj = Object.values(teams);
        const duplicates = await Promise.all(
            filtered.map((u: any) => this.db.ref(`duplicates/v2_users`).orderByChild('phone').equalTo(u.id).once('value').then((ds: database.DataSnapshot) => ds.val()))
        );

        const update: any = {};

        duplicates.forEach((duplicate: any) => {
            if (!duplicate) {
                return;
            }
            const dupUsers = Object.values(duplicate);
            const user: any = dupUsers[dupUsers.length - 1];

            if (!user) {
                return;
            }
            const organization: any = this.getOrganizationInfo(user);
            const teamKey = teamOvj.filter((t: string) => (<any>t).leader == user.phone)[0];
            if (teamKey) {
                update[`${TEAMS_KEY}/${teamKey.id}/organization`] = organization;
            }

            for (const key in organization) {
                if (Object.prototype.hasOwnProperty.call(organization, key)) {
                    const val = organization[key];
                    update[`${USERS_KEY}/${user.id}/${key}`] = val;
                }
            }
            update[`${USERS_KEY}/${user.id}/team`] = null;
        });

        // await this.db.ref().update(update);


        return res.send({ users: filtered, teams: teams, bmids: duplicates, update: update });


        // const stats: any[] = [];
        // const allUserToTeams: any = await this.db.ref('user_to_team').once('value').then((utot: database.DataSnapshot) => {
        //     return utot.val();
        // });

        // const allTeamStats: any = {};
        // let allTeamBmIds: string[] = [];
        // const users = Object.keys(allUserToTeams);// await Promise.all(stats)

        // for (let i = 0; i < users.length; i++) {
        //     const userId = users[i];
        //     if (!allTeamStats[allUserToTeams[userId]]) {
        //         allTeamStats[allUserToTeams[userId]] = true;
        //     }
        // }

        // const teamIds = Object.keys(allTeamStats);
        // let batch = teamIds.slice(0, 500);
        // let index = 0;
        // while (index * 500 <= teamIds.length) {
        //     batch = teamIds.slice(index * 500, (index + 1) * 500);
        //     await Promise.all(batch.map((teamId: string) => {
        //         return this.teamManager.getTeamStats(teamId, undefined, undefined).then(stats => allTeamStats[teamId] = stats);
        //     }))
        //     index++;
        // }

        // // await Promise.all(Object.keys(allTeamStats).map((teamId: string) => {
        // //     return this.teamManager.getTeamStats(teamId, undefined, undefined).then(stats => allTeamStats[teamId] = stats);
        // // }));

        // const allMembers: string[] = [];

        // for (const teamId in allTeamStats) {
        //     if (Object.prototype.hasOwnProperty.call(allTeamStats, teamId)) {
        //         const stats = allTeamStats[teamId];
        //         if (stats == null) {
        //             continue;
        //         }

        //         if (stats.points.total < 300) {
        //             allMembers.push(...stats.members);
        //             //const teamBmIdsPromises = stats.members
        //         }
        //     }
        // }

        // const teamBmids: string[][] = await Promise.all(allMembers.map((userId: string) =>
        //     this.db.ref(`users/${userId}/phone`).once('value')
        //         .then((phoneSnap: database.DataSnapshot) => phoneSnap.val())
        //         .then((phone: string) => phone ? this.db.ref(`phone_to_bmid/${phone}`).once('value') : null)
        //         .then((bmIds: database.DataSnapshot | null) => {
        //             if (bmIds) {
        //                 const phoneToBmIds = bmIds.val();
        //                 return phoneToBmIds ? Object.keys(phoneToBmIds) : [];
        //             }
        //             return [];
        //         })));

        // allTeamBmIds = teamBmids.reduce((allbmids: string[], teamIds: string[]) => { allbmids.push(...teamIds); return allbmids; }, []);

        // await callBotmotherWebhook('https://app.botmother.com/api/bot/action/PLazPqei9/CyByBBCb4BQCXBaBngDWCHDOKD8sD6CqBjyCWB4FcB1BKtCnDYgD4WBDgBiFC4CA', '',
        //     allTeamBmIds.slice(0, 1000), {});
        // await callBotmotherWebhook('https://app.botmother.com/api/bot/action/PLazPqei9/CyByBBCb4BQCXBaBngDWCHDOKD8sD6CqBjyCWB4FcB1BKtCnDYgD4WBDgBiFC4CA', '',
        //     allTeamBmIds.slice(1000, 2000), {});
        // await callBotmotherWebhook('https://app.botmother.com/api/bot/action/PLazPqei9/CyByBBCb4BQCXBaBngDWCHDOKD8sD6CqBjyCWB4FcB1BKtCnDYgD4WBDgBiFC4CA', '',
        //     allTeamBmIds.slice(2000, 3000), {});
        // await callBotmotherWebhook('https://app.botmother.com/api/bot/action/PLazPqei9/CyByBBCb4BQCXBaBngDWCHDOKD8sD6CqBjyCWB4FcB1BKtCnDYgD4WBDgBiFC4CA', '',
        //     allTeamBmIds.slice(3000, 4000), {});
        // await callBotmotherWebhook('https://app.botmother.com/api/bot/action/PLazPqei9/CyByBBCb4BQCXBaBngDWCHDOKD8sD6CqBjyCWB4FcB1BKtCnDYgD4WBDgBiFC4CA', '',
        //     allTeamBmIds.slice(4000, 5000), {});
        // await callBotmotherWebhook('https://app.botmother.com/api/bot/action/PLazPqei9/CyByBBCb4BQCXBaBngDWCHDOKD8sD6CqBjyCWB4FcB1BKtCnDYgD4WBDgBiFC4CA', '',
        //     allTeamBmIds.slice(5000, 6000), {});
        // await callBotmotherWebhook('https://app.botmother.com/api/bot/action/PLazPqei9/CyByBBCb4BQCXBaBngDWCHDOKD8sD6CqBjyCWB4FcB1BKtCnDYgD4WBDgBiFC4CA', '',
        //     allTeamBmIds.slice(6000, 6500), {});
        // return res.json({ bmids: allTeamBmIds });

        const getSchoolInfo = async (userIds: string[]) => {
            const users = await Promise.all(userIds.map((uid: string) => this.db.ref(`users/${uid}`).once('value').then(ds => ds.val())));

            const trustedLocationUser = users.filter(u => !!u.location)[0] || null;
            return {
                lat: parseFloat(trustedLocationUser?.location?.settlement?.geo_lat) || null,
                lon: parseFloat(trustedLocationUser?.location?.settlement?.geo_lon) || null,
                kladr_id: trustedLocationUser?.location?.settlement?.kladr_id || null,
                school: trustedLocationUser?.school || users.map(u => u.school).join(';'),
                country: trustedLocationUser?.country || users.map(u => u.country).join(';'),
                city: (trustedLocationUser?.city || trustedLocationUser?.region) || users.map(u => u.city).join(';'),
                contacts: users.map(u => { return { name: `${u.firstname} ${u.lastname}`, role: RSAEnvConfig.ROLE_LABELS[u.role], phone: '+' + u.phone }; })
            };
        }

        // const teamsStats: { [key: string]: ITeamStats } = (await (this.db.ref('team_stats').orderByChild('points/total').limitToLast(100).once('value'))).val();
        // // const allUserIds = (Object.values(teamsStats) as ITeamStats[]).reduce((users: string[], ts: ITeamStats) => { users.push(...ts.members); return users; }, []);
        // let fullStats = await Promise.all(Object.keys(teamsStats).map(async (teamId: string) => teamsStats[teamId].points.total > 1000 ? this.teamManager.getFullStats(teamId, undefined, undefined) : Promise.resolve(null)));

        // fullStats = fullStats.filter(fs => !!fs);

        // await Promise.all(fullStats.map((fs: any) => {
        //     return getSchoolInfo(fs?.team_stats.members).then(info => fs['school'] = info);
        // }));

        // // let teamLocations: any[] = [];
        // // await Promise.all(Object.keys(teamsStats).map(async (teamId: string) => {
        // //     const stats = teamsStats[teamId];

        // //     const users = await Promise.all(teamsStats[teamId]
        // //         .members.map((uid: string) => this.db.ref(`users/${uid}`).once('value').then(ds => ds.val())));

        // //     const trustedLocationUser = users.filter(u => !!u.location)[0] || null;
        // //     teamLocations.push({
        // //         team_id: teamId,
        // //         lat: parseFloat(trustedLocationUser?.location?.settlement?.geo_lat) || null,
        // //         lon: parseFloat(trustedLocationUser?.location?.settlement?.geo_lon) || null,
        // //         kladr_id: trustedLocationUser?.location?.settlement?.kladr_id || null,
        // //         school: trustedLocationUser?.school || users.map(u => u.school).join(';'),
        // //         country: trustedLocationUser?.country || users.map(u => u.country).join(';'),
        // //         city: (trustedLocationUser?.city || trustedLocationUser?.region) || users.map(u => u.city).join(';'),
        // //         points: stats.points.total,
        // //         complete_tasks_codes: stats.complete_tasks_codes.join(', '),
        // //         members_count: stats.members.length,
        // //         tasks_count: stats.complete_tasks_codes.length,
        // //         contacts: users.map(u => { return { name: `${u.firstname} ${u.lastname}`, role: EnvConfig.ROLE_LABELS[u.role], phone: '+' + u.phone }; })
        // //     });
        // // }));

        // // teamLocations = teamLocations.filter(t => t.points >= 280).sort((a: any, b: any) => a.points - b.points);

        // //const users = await Promise.all(allUserIds.map(uid => this.db.ref(`users/${uid}`).once('value').then(ds => ds.val())));
        // return res.json((fullStats as any).sort((fs1: any, fs2: any) => fs1.team_stats.points.total - fs2.team_stats.points.total));



    }

    private getOrganizationInfo(userData: any) {
        const data = {
            city: userData.city || null,
            country: userData.country || null,
            location: userData.location || null,
            region: userData.region || null,
            school: userData.school || null,
            settlement: userData.settlement || null
        };

        if (Object.values(data).every(v => !v)) {
            return null;
        }

        return data;
    }

}