
export function escapeJson(str: string) {
	if (!str) {
		return;
	}
	const jsonStr = JSON.stringify(str);
	return jsonStr.substr(1, jsonStr.length - 2);
}

export function zeroIfNull(val: any) {
	if (val == null || typeof (val) == 'undefined') {
		return '0'
	}
	return val;
}

export function checkComplete(this: any, val: any) {
	if (val == null || typeof (val) == 'undefined') {
		return ''
	}
	else if (this.stats?.team_stats?.complete_tasks &&
		(this.stats.team_stats.complete_tasks[val] ||
			this.stats.team_stats.complete_tasks[val.replace('L1', 'L2')] ||
			this.stats.team_stats.complete_tasks[val.replace('L1', 'L3')])
	) {
		return '✅ ';
	}
	return '';
}

export function escapeQuotes(this: any, val: any) {
	if (!val) {
		return val;
	}
	if (typeof (val) != 'string') {
		return val;
	}
	return val.replace(/"/g, '\\"');
}

export function equals(this: any, arg1: any, arg2: any, options: Handlebars.HelperOptions) {
	if (arg1 == arg2) {
		return options.fn(this);
	}
	return options.inverse(this);
}

export function gte(this: any, arg1: any, arg2: any, options: Handlebars.HelperOptions) {
	if (arg1 >= arg2) {
		return options.fn(this);
	}
	return options.inverse(this);
}