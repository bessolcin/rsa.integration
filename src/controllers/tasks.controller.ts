import { ILogger } from '@bessolcin/coolstory.bots.lib';
import { Express, Request, Response } from 'express';
import { database as db } from 'firebase-admin';
import { TasksManager } from '../managers';

export class TasksController {
	/**
	 *
	 */
	constructor(app: Express, private db: db.Database, private logger: ILogger, private tasksManager: TasksManager) {
		this.configureRequestHandlers(app);
	}

	private configureRequestHandlers(app: Express) {
		app.post('/tasks/update', this.onTasksUpdate.bind(this));
	}

	private async onTasksUpdate(req: Request, res: Response) {
		try {
			if (req.body.update.title == 'taskresult_comment_update') {
				await this.tasksManager.commentTaskResult(req.body);
			}
			else {
				await this.tasksManager.changeStateTaskResult(req.body);
			}
			return res.json({ result: 'ok' });
		} catch (error) {
			const errorText = `Error while updating task state. request: ${JSON.stringify(req.body)}`;
			console.error(errorText, error);
			this.logger.alert(errorText);
			return res.status(400).json({ result: 'error', error: errorText });
		}
	}
}