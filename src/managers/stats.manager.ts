import { database as db } from 'firebase-admin';

import { getDiffInMinutes, ICacheItem, SpreadsheetManager } from '@bessolcin/coolstory.bots.lib';

import { IBaseStats, ICompleteTask, IPersonalStats, IStats, ITask, ITasksDictionary, ITeamStats, PointsStats, TaskResultState } from '../models';
import { TEAM_STATS_KEY, USERS_KEY, USER_STATS_KEY, USER_TO_TEAM_KEY } from '../constants';
import { RSAEnvConfig } from '../envconfig';

const M1T1L1: ITaskResult = {
    form_name: 'M1N1L1',
    form_value: {
        'value1': 'Собрали все роли в команде!'
    },
    bm_id: '',
    date: '',
    dialog_url: '',
    firstname: '',
    lastname: '',
    team_id: '',
    time: '',
    timestamp: 0,
    platform: '',
    platform_id: '',
    user_id: '',
    state: TaskResultState.Approved
};

export class StatsManager {

    private tasksCache: ICacheItem | null = null;

    private tasksRef: db.Reference;

    constructor(
        private db: db.Database,
        private spreadsheetManager: SpreadsheetManager,
        private outroPermissionsFunc: ((stats: IStats, tasks: ITasksDictionary) => any) | null = null,
        private prizesPermissionsFunc: ((stats: IStats, tasks: ITasksDictionary) => any) | null = null
    ) {
        this.tasksRef = this.db.ref('bot/content/tasks');
    }

    public async getTeamAndPersonalStats(teamId: string | undefined, userId: string | undefined): Promise<IStats> {
        const [teamStats, personalStats] = await Promise.all([this.getTeamStats(teamId), this.getPersonalStats(userId)]);

        return {
            personal_stats: personalStats,
            team_stats: teamStats
        };
    }

    public async getTeamStats(teamId: string | undefined): Promise<ITeamStats | null> {
        if (!teamId) {
            return null;
        }
        const teamStats = (await this.db.ref(`${TEAM_STATS_KEY}/${teamId}`).once('value')).val();

        return teamStats;
    }

    public async getPersonalStats(userId: string | undefined): Promise<IPersonalStats | null> {
        if (!userId) {
            return null;
        }
        const personalStats = (await this.db.ref(`${USER_STATS_KEY}/${userId}`).once('value')).val();

        return personalStats;
    }

    public async updateTeamStats(currentUserId: string | undefined, currentUserRole: string | undefined): Promise<IStats | null> {
        const teamId = (await this.db.ref(`${USER_TO_TEAM_KEY}/${currentUserId}`).once('value')).val();
        const tasks = await this.getTasks();

        const stats = await this.calculateTeamStats(teamId, currentUserId, currentUserRole, tasks);

        if (stats) {
            this.applyPermissions(stats.team_stats, stats, tasks);

            if (currentUserId) {
                await this.db.ref(`${USER_STATS_KEY}/${currentUserId}`).set(stats.personal_stats);
            }

            await this.db.ref(`${TEAM_STATS_KEY}/${teamId}`).set(stats.team_stats);
        }

        return stats;
    }

    /**
     * Считает статистику для всей команды и текущего пользователя
     * @param teamId ИД команды
     * @param currentUserId ИД текущего пользователя
     * @param currentUserRole Роль текущего пользователя
     */
    private async calculateTeamStats(teamId: string | undefined, currentUserId: string | undefined, currentUserRole: string | undefined, tasks: ITasksDictionary): Promise<IStats | null> {
        if (!teamId) {
            return null;
        }

        const userToTeamValue = await this.db.ref(`${USER_TO_TEAM_KEY}`).orderByValue().equalTo(teamId).once('value').then(ds => ds.val());

        if (userToTeamValue) {
            const teamUserIds = Object.keys(userToTeamValue);
            return await this.calculateStatsForUserIds(teamUserIds, currentUserId, currentUserRole, tasks);
        }

        return null;
    }

    /**
     * Считает полную статистику по пользователям и текущему
     * @param teamUserIds Ид пользователей, для которых собирается статистика по заданиям
     * @param currentUserId Текущий пользователь
     * @param currentUserRole Роль текущего пользователя для подсчета заданий в треке
     * @param tasks Индекс заданий
     */
    private async calculateStatsForUserIds(teamUserIds: string[], currentUserId: string | undefined, currentUserRole: string | undefined, tasks: ITasksDictionary): Promise<IStats> {
        const allTeamUserResults: { [key: string]: ITaskResult } = await this.getAllTeamUsersResults(teamUserIds);

        let currentUserTasksCount = 0;
        let currentUserRoleTasksCount = 0;
        const userTasksDictionary: { [key: string]: ITaskResult } = {};
        const userRoleTasksDictionary: { [key: string]: ITaskResult } = {};
        const teamTasksDictionary: { [key: string]: ITaskResult } = {};

        for (const key in allTeamUserResults) {
            if (Object.prototype.hasOwnProperty.call(allTeamUserResults, key)) {
                const taskResult: ITaskResult = allTeamUserResults[key];

                if (taskResult.state !== TaskResultState.Approved) {
                    continue;
                }

                if (taskResult.user_id == currentUserId && !userTasksDictionary[taskResult.form_name]) {
                    currentUserTasksCount++;
                    if (tasks[taskResult.form_name] && tasks[taskResult.form_name].Roles == currentUserRole) {
                        currentUserRoleTasksCount++;
                        userRoleTasksDictionary[taskResult.form_name] = taskResult;
                    }
                    userTasksDictionary[taskResult.form_name] = taskResult;
                }

                if (!teamTasksDictionary[taskResult.form_name]) {
                    teamTasksDictionary[taskResult.form_name] = taskResult;
                    continue;
                }

                if (taskResult.timestamp > teamTasksDictionary[taskResult.form_name].timestamp) {
                    teamTasksDictionary[taskResult.form_name] = taskResult;
                }
            }
        }

        const isTeamHasAllRoles = await this.isTeamHasAllRoles(teamUserIds);

        if (isTeamHasAllRoles) {
            M1T1L1.timestamp = new Date().getTime();
            teamTasksDictionary['M1T1L1'] = M1T1L1;
        }

        const teamBaseStats = this.calculateStats(teamTasksDictionary, tasks);

        // console.log(results)
        const teamStats: ITeamStats = {
            members: teamUserIds,
            complete_tasks: teamBaseStats.complete_tasks,
            complete_tasks_codes: teamBaseStats.complete_tasks_codes,
            complete_tasks_count: teamBaseStats.complete_tasks_count,
            points: teamBaseStats.points,
            marathon_outro_permissions: {},
            marathon_prizes_permissions: {},
            timestamp: new Date().getTime()
        };

        const userBaseStats = this.calculateStats(userTasksDictionary, tasks);
        const userRoleBaseStats = this.calculateStats(userRoleTasksDictionary, tasks);
        const personalStats: IPersonalStats = {
            current_role_complete_tasks_count: currentUserRoleTasksCount,

            role_complete_tasks: userRoleBaseStats.complete_tasks,
            role_complete_tasks_codes: userRoleBaseStats.complete_tasks_codes,
            role_complete_tasks_count: userRoleBaseStats.complete_tasks_count,
            role_points: userRoleBaseStats.points,

            complete_tasks: userBaseStats.complete_tasks,
            complete_tasks_codes: userBaseStats.complete_tasks_codes,
            complete_tasks_count: userBaseStats.complete_tasks_count,
            points: userBaseStats.points,

            marathon_outro_permissions: {},
            marathon_prizes_permissions: {}
        };

        return <IStats>{
            personal_stats: personalStats,
            team_stats: teamStats
        };
    }

    private async getAllTeamUsersResults(teamUserIds: string[]): Promise<{ [key: string]: ITaskResult }> {
        const allTeamUserResults: { [key: string]: ITaskResult } = {};
        const resultsByUser = await Promise.all(teamUserIds.map((uid: string) => this.db.ref(`${RSAEnvConfig.PARTICIPANTS_TASKS_RESULTS_PATH}`).orderByChild('user_id').equalTo(uid).once('value').then(ds => ds.val())));
        Object.assign(allTeamUserResults, ...resultsByUser);
        return allTeamUserResults;
    }

    private async isTeamHasAllRoles(userIds: string[]) {
        const teamUserRoles = await Promise.all(userIds.map(uid => this.db.ref(`${USERS_KEY}/${uid}/role`).once('value').then(d => d.val())));
        const rolesMap = teamUserRoles.filter(r => r != 'notfromschool').reduce((o: any, role: string) => { o[role] = true; return o; }, {});
        return Object.keys(rolesMap).length == 6;
    }

    /**
     * Считает статистику по выбранным заданиям
     * @param tasksDictionary Задания по которым считаются очки
     * @param allTasks Индекс всех заданий, что есть
     */
    private calculateStats(tasksDictionary: { [key: string]: ITaskResult }, allTasks: any): IBaseStats {
        const points: PointsStats = new PointsStats();

        const completeTasks: { [key: string]: ICompleteTask } = {};
        const completeTasksCodes: string[] = [];
        for (const key in tasksDictionary) {
            if (Object.prototype.hasOwnProperty.call(tasksDictionary, key)) {
                if (!allTasks[key]) {
                    continue;
                }

                const taskResult = tasksDictionary[key];
                completeTasks[key] = {
                    code: key,
                    value: taskResult.form_value,
                    date: taskResult.date,
                    time: taskResult.time,
                    points: allTasks[key].Points
                };

                completeTasksCodes.push(key);

                const task = allTasks[key];

                let needToAddPoints = true;
                if (!key.endsWith('L1')) {
                    const code = `${key.substring(0, 4)}L1`;
                    if (tasksDictionary[code]) {
                        needToAddPoints = false;
                    }
                }
                if (key.startsWith('M8')) {
                    needToAddPoints = false;
                }
                if (task && needToAddPoints) {
                    points.add(task.Points);
                }
            }
        }

        const baseStats: IBaseStats = {
            complete_tasks: completeTasks,
            complete_tasks_codes: completeTasksCodes,
            complete_tasks_count: completeTasksCodes.length,
            points: points,
            marathon_outro_permissions: {},
            marathon_prizes_permissions: {}
        };

        return baseStats;
    }

    private applyPermissions(currentStats: IBaseStats | null, stats: IStats, tasks: ITasksDictionary) {
        if (!currentStats) {
            return;
        }

        if (this.outroPermissionsFunc) {
            currentStats.marathon_outro_permissions = this.outroPermissionsFunc(stats, tasks);
        }

        if (this.prizesPermissionsFunc) {
            currentStats.marathon_prizes_permissions = this.prizesPermissionsFunc(stats, tasks);
        }
    }

    public async getTasks(): Promise<ITasksDictionary> {
        if (this.tasksCache) {
            const now = (new Date()).getTime();

            if (getDiffInMinutes(now, this.tasksCache.timestamp) < 240) {
                return this.tasksCache.value;
            }
            this.tasksCache = null;
        }

        const tasksDictionary: ITasksDictionary = (await this.tasksRef.once('value')).val();

        this.tasksCache = {
            timestamp: (new Date()).getTime(),
            value: tasksDictionary
        };

        return tasksDictionary;
    }

    public async updateTasks() {
        const tasksDictionary: any = {};
        const rows: any[] = await this.spreadsheetManager.readCellsAsJson(RSAEnvConfig.BOT_TASKS_SPREADSHEET_ID, 'tasks_season_3', 100, 0);

        rows.filter(r => !!r.Condition).forEach(row => {
            const points = PointsStats.fromObject(row);
            row['Points'] = points;
            tasksDictionary[row.Code] = row;
        });
        await this.tasksRef.set(tasksDictionary);

        this.tasksCache = null;

        return tasksDictionary;
    }

    //** Возвращает словарь заданий по каждому юзеру */
    public async getTeamFeed(teamUserIds: string[]) {
        const allTeamUserStats = await Promise.all(teamUserIds.map((uid: string) => this.getPersonalStats(uid).then(s => { return { id: uid, stats: s } })));
        const userToTasksDictionary: { [phone: string]: ICompleteTask[] } = {};

        allTeamUserStats.filter(ps => !!ps.stats && !!ps.stats.complete_tasks).forEach(ps => {
            userToTasksDictionary[ps.id] = Object.values((<IPersonalStats>ps.stats).complete_tasks).sort((a: ICompleteTask, b: ICompleteTask) => {
                if (!a.date || !b.date) return 0;
                if (a.date > b.date) return 1;
                if (a.date < b.date) return -1;
                return 0;
            });
        });

        return userToTasksDictionary;
    }

    public async saveStatsHistory() {
        const allTeamStats = (await this.db.ref(TEAM_STATS_KEY).once('value')).val();
        await this.db.ref(`v3/history/team_stats/${(new Date()).getTime()}`).set(allTeamStats);

        const allPersonalStats = (await this.db.ref(USER_STATS_KEY).once('value')).val();
        await this.db.ref(`v3/history/user_stats/${(new Date()).getTime()}`).set(allPersonalStats);
    }


    public async getFullStats(teamId: string | undefined, currentUserId: string | undefined, currentUserRole: string | undefined) {
        if (!teamId) {
            return null;
        }

        const [tasks, userToTeamSnap] = await Promise.all([
            this.getTasks(),
            this.db.ref(`user_to_team`).orderByValue().equalTo(teamId).once('value')
        ]);

        const userToTeamValue = userToTeamSnap.val(); //(await this.db.ref(`user_to_team`).orderByValue().equalTo(teamId).once('value')).val();

        const allTeamUserResults: any = {};
        const teamUserIds = Object.keys(userToTeamValue);
        const resultsByUser = await Promise.all(teamUserIds.map((uid: string) => this.db.ref(`forms/results`).orderByChild('user_id').equalTo(uid).once('value').then(s => {
            return s.val();
        })));
        Object.assign(allTeamUserResults, ...resultsByUser);

        const users = await Promise.all(teamUserIds.map(uid => this.db.ref(`users/${uid}`).once('value').then(uds => {
            const user = uds.val();
            user['id'] = uid;
            return user;
        })));


        const usersStats: any[] = [];
        const allTeamUserResultsArray: ITaskResult[] = Object.values(allTeamUserResults);
        users.forEach(user => {
            usersStats.push({
                firstname: user.firstname,
                lastname: user.lastname,
                phone: user.phone,
                id: user.id,
                role: user.role,
                performed_tasks: allTeamUserResultsArray.filter((r: ITaskResult) => r.user_id == user.id).map(tr => {
                    return {
                        date: tr.date,
                        time: tr.time,
                        team_id: tr.team_id,
                        dialog: tr.dialog_url,
                        code: tr.form_name,
                        result: tr.form_value,
                        task: tasks[tr.form_name].Condition,
                        points: tasks[tr.form_name].Points,
                        // direction: tasks[tr.form_name].Direction,
                        canceled: tr.canceled || false
                    }
                })
            })
        });

        let currentUserTasksCount = 0;
        let currentUserRoleTasksCount = 0;
        const userTasksDictionary: { [key: string]: ITaskResult } = {};
        const tasksDictionary: { [key: string]: ITaskResult } = {};

        for (const key in allTeamUserResults) {
            if (Object.prototype.hasOwnProperty.call(allTeamUserResults, key)) {
                const taskResult: ITaskResult = allTeamUserResults[key];

                if (taskResult.canceled === true) {
                    continue;
                }

                if (taskResult.user_id == currentUserId && !userTasksDictionary[taskResult.form_name]) {
                    currentUserTasksCount++;
                    if (tasks[taskResult.form_name] && tasks[taskResult.form_name].Roles == currentUserRole) {
                        currentUserRoleTasksCount++;
                    }
                    userTasksDictionary[taskResult.form_name] = taskResult;
                }

                if (!tasksDictionary[taskResult.form_name]) {
                    tasksDictionary[taskResult.form_name] = taskResult;
                    continue;
                }

                // todo: use timestamps
                if (taskResult.timestamp > tasksDictionary[taskResult.form_name].timestamp) {
                    tasksDictionary[taskResult.form_name] = taskResult;
                }
            }
        }

        const points: PointsStats = new PointsStats();

        const completeTasks: { [key: string]: ICompleteTask } = {};
        const completeTasksCodes: string[] = [];
        for (const key in tasksDictionary) {
            if (Object.prototype.hasOwnProperty.call(tasksDictionary, key)) {
                const taskResult = tasksDictionary[key];
                completeTasks[key] = {
                    code: key,
                    value: taskResult.form_value.value1 + ' ' + (taskResult.form_value.value2 || ''),
                    points: tasks[key].Points,
                    task_text: tasks[key].Condition
                };

                completeTasksCodes.push(key);

                const task = tasks[key];
                if (task) {
                    points.add(task.Points)
                }
            }
        }

        // console.log(results)
        const stats: ITeamStats = {
            // current_member_complete_tasks: currentUserTasksCount,
            // current_member_role_complete_tasks: currentUserRoleTasksCount,
            members: teamUserIds,
            complete_tasks: completeTasks,
            complete_tasks_codes: completeTasksCodes.sort(),
            complete_tasks_count: completeTasksCodes.length,
            points: points,
            marathon_outro_permissions: {},
            marathon_prizes_permissions: {},
            timestamp: new Date().getTime()
        };

        try {
            await this.db.ref(`team_stats/${teamId}`).set(stats);
        } catch (error) {
            console.error(`Error while updating team stats for ${teamId}`);
        }

        return {
            team_stats: stats,
            //users_stats: usersStats,
            all_tasks: Object.keys(tasks)
                .filter(code => tasks[code].Published === 'TRUE' && tasks[code].CreateTicket === 'TRUE')
                .reduce((dict: any, code: string) => {
                    if (!dict[code]) {
                        dict[code] = {
                            results: [],
                            points: tasks[code].Points,
                            // direction: tasks[code].Direction,
                            task_text: tasks[code].Condition
                        };
                    }
                    dict[code].results.push(...allTeamUserResultsArray.filter(userRes => userRes.form_name == code).map(res => {
                        return {
                            name: `${res.firstname} ${res.lastname}`,
                            result: res.form_value.value1 + ' ' + (res.form_value.value2 || ''),
                            date: `${res.date} ${res.time}`
                        };
                    }));
                    return dict;
                }, {})
        };
    }
}

export interface ITaskResult {
    lastname: any;
    firstname: any;
    bm_id: string;
    timestamp: number;
    date: string;
    dialog_url: string;
    form_name: string;
    form_value: any;
    platform: string;
    platform_id: string;
    team_id: string;
    time: string;
    user_id: string;
    canceled?: boolean;
    state: TaskResultState;
}
