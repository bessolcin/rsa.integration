import { readFile } from 'fs/promises';
import * as path from 'path';

import { ScreenshotService } from './screenshot.service';

export class HtmlToImageService {

	private templates: any = {};

	private templateIds: string[] = ['M1.html', 'M2.html', 'M3.html', 'M4.html', 'M5.html', 'M6.html', 'M7.html', 'finalist.html'];

	constructor(
		private screenshoService: ScreenshotService) {
	}

	public async loadTemplates(ids: string[]) {
		if (!this.templates[ids[0]]) {
			// console.log('Loading templates...', __dirname, __filename);
			const readFileTasks = await Promise.all(
				ids.map(
					id => readFile(path.join(__dirname, `../../templates/${id}`))
						.then(r => ({ id: id, result: r.toString() }))
				)
			);
			for (let i = 0; i < readFileTasks.length; i++) {
				const result = readFileTasks[i];
				this.templates[result.id] = result.result;
			}
			console.log('Templates loaded.');
			// console.log('loading complete', process.pid, this.templates, this);
			return this.templates;
		}
		return this.templates;
	}

	public async generateFromTemplate(type: string, templateId: string, data: any) {
		const templates = await this.loadTemplates(this.templateIds);
		const templateHtml = templates[templateId];

		if (!templateHtml) {
			throw new Error(`Template with id=${templateId} not found`);
		}

		const image = await this.screenshoService.screenshot({
			type: type,
			html: templateHtml,
			content: data,
			quality: 100
		});

		return image;
	}

	public async generatePDFFromTemplate(templateId: string, data: any) {
		const templates = await this.loadTemplates(this.templateIds);
		const templateHtml = templates[templateId];

		if (!templateHtml) {
			throw new Error(`Template with id=${templateId} not found`);
		}

		const image = await this.screenshoService.pdf({
			html: templateHtml,
			content: data,
			quality: 100
		});

		return image;
	}
}