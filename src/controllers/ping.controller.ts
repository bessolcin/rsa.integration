import { Express, Request, Response } from 'express';

export class PingController {
    /**
     *
     */
    constructor(app: Express) {
        this.configureRequestHandlers(app);
    }

    private configureRequestHandlers(app: Express) {
        app.get('/ping', this.onPing);
    }

    private onPing(req: Request, res: Response) {
        res.send('pong').status(200);
    } 
}