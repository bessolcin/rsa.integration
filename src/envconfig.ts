import { IEnvConfig, EnvConfig } from '@bessolcin/coolstory.bots.lib';

export const RSAEnvConfig: IRSAEnvConfig = {
    ...EnvConfig,

    BOT_MENU_CACHE_LIFETIME: 180,

    DB_SERVICE_ACCOUNT: process.env.DB_SERVICE_ACCOUNT ? JSON.parse(process.env.DB_SERVICE_ACCOUNT || '{}') : require("./../coolstoryprointegration-firebase-adminsdk-32r3r-d26e4fa347.json"),
    DB_DATABASE_URL: 'https://rsa-integration.firebaseio.com',

    CURRENT_APP_URL: process.env.CURRENT_APP_URL ? process.env.CURRENT_APP_URL : 'https://europe-west1-coolstoryprointegration.cloudfunctions.net/rsaApp',

    SPREADSHEET_CLIENT_SECRET: process.env.SPREADSHEET_CLIENT_SECRET ? JSON.parse(process.env.SPREADSHEET_CLIENT_SECRET || '') : require("./../client_secret.json"),
    BOTMOTHER_USER_CREATED_CALLBACK: process.env.BOTMOTHER_USER_CREATED_CALLBACK || 'https://app.botmother.com/api/bot/action/D0qfpkuPh/DzBWMnEC_D4C9hChD6KCipBLC8QDsB09DkDtSD4DAD4CKCZBImBnBJBVDIBmCxFA',
    BOTMOTHER_NEW_TEAM_MEMBER_CALLBACK: 'https://app.botmother.com/api/bot/action/9DUwSW_JI/LCkDGFCpDSDndDODBCSBPCkCPB0Cu7BZhDtDRDIw6D9CZHBLyBcDZFBDGCMACcB9',
    BOTMOTHER_REDIRECT_TO_SECTION_CALLBACK: 'https://app.botmother.com/api/bot/action/GMeqpgz3w/Dh4CuBlD6BlCrfCTDFBFC7BACG3BGDMCKCjDBxDjCMQC3DjDinDXDHBLfUDeBnDL',
    BOTMOTHER_RESULT_ACCEPTED_CALLBACK: 'https://app.botmother.com/api/bot/action/3nGMmktgD/DTCPAB0B8FCDB-CCC3BgDhDMBkCTCODTC-B_C_BTCjrCL1DyXCiC-KDgBCHC9D-C',
    BOTMOTHER_RESULT_COMMENT_CALLBACK: 'https://app.botmother.com/api/bot/action/xhnYpNtov/BpDxBnDNC3CRCGBaBrDGRC7D7fDVBRDoBzB4C_lCYDsgf3DKBFDCBWBrCVhCQCQC',

    GA_ID: 'UA-179183344-1',

    USEDESK_API_TOKEN: '73651f3a76497cb51623ef72420fedc59619791b',

    ROLE_LABELS: {
        "administration": "🏫 Представитель администрации",
        "director": "👨‍💼 Директор",
        "exstudent": "👨‍🎓 Выпускник",
        "notfromschool": "✌️ Не из школы",
        "parent": "👩‍👧‍👦 Представитель семьи",
        "partner": "🤵 Партнер",
        "student": "🙋 Ученик",
        "teacher": "👩‍🏫 Педагог"
    },

    PARTICIPANTS_TASKS_RESULTS_PATH: 'forms/results',

    BOT_ADMIN_SPREADSHEET_ID: '13z60t37Xizrot9Onc0ufUxf41HFvgXJwRK6X3WIuAvk',
    BOT_TASKS_SPREADSHEET_ID: '12bWfpLdv524EIeZNIPTtTbu8akGiW_eODgwCObyVjKw',

    MENU_TEMPLATE_CACHE_LIFETIME: process.env.MENU_TEMPLATE_CACHE_LIFETIME ? parseInt(process.env.MENU_TEMPLATE_CACHE_LIFETIME || '') : 2,

    CS_STORAGE_BUCKET_URL: 'coolstoryprointegration.appspot.com'
};

export interface IRSAEnvConfig extends IEnvConfig {
    BOTMOTHER_USER_CREATED_CALLBACK: string;
    BOTMOTHER_NEW_TEAM_MEMBER_CALLBACK: string;
    BOTMOTHER_REDIRECT_TO_SECTION_CALLBACK: string;
    BOTMOTHER_RESULT_ACCEPTED_CALLBACK: string;
    BOTMOTHER_RESULT_COMMENT_CALLBACK: string;

    GA_ID: string;

    ALERT_BOT_TOKEN: string;
    ALERT_CHAT_ID: number;

    BOT_MENU_SECTIONS_LIMIT: number;
    BOT_MENU_CACHE_LIFETIME: number;

    USEDESK_API_TOKEN: string;

    ROLE_LABELS: { [key: string]: string };

    PARTICIPANTS_TASKS_RESULTS_PATH: string;

    BOT_ADMIN_SPREADSHEET_ID: string;
    BOT_TASKS_SPREADSHEET_ID: string;

    MENU_TEMPLATE_CACHE_LIFETIME: number;
}