import { IPointsStats, ITeamStats } from './../models';

const COMPLETE_TASK_MARK = '✅';

const COMPLETE_TRACK_TASKS_LABEL = '🏅 Из них в номинации';

const TRACKS_LABELS: any = {
    'student': '«Ученики»',
    'teacher': '«Учителя»',
    'parent': '«Представитель семьи»',
    'exstudent': '«Выпускники»'
    // 'partner': '«Предприниматели»'
};

const directionMaxValues: IPointsStats = {
    V: 165,
    I: 165,
    SH: 165,
    U: 165,
    S: 165,
    L: 165,
    R: 165,
    O: 165,
    K: 165,
    B: 15,
    total: 1500
};

const directionLabels: any = {
    V: 'Выпускники',
    I: 'Индивидуальность',
    SH: 'Школьники',
    U: 'Учителя',
    S: 'Сообщество',
    L: 'Лидерство',
    R: 'Родители',
    O: 'Общество',
    K: 'Коммуникация',
    B: 'Бонусные',
    total: 'Всего'
};

export function getTaskDirectionsStatsView(teamStats: ITeamStats): string {

    return `💎 ${directionLabels.V}: ${teamStats.points.V} / ${directionMaxValues.V}
💎 ${directionLabels.I}: ${teamStats.points.I} / ${directionMaxValues.I}
💎 ${directionLabels.SH}: ${teamStats.points.SH} / ${directionMaxValues.SH}
💎 ${directionLabels.U}: ${teamStats.points.U} / ${directionMaxValues.U}
💎 ${directionLabels.S}: ${teamStats.points.S} / ${directionMaxValues.S}
💎 ${directionLabels.L}: ${teamStats.points.L} / ${directionMaxValues.L}
💎 ${directionLabels.R}: ${teamStats.points.R} / ${directionMaxValues.R}
💎 ${directionLabels.O}: ${teamStats.points.O} / ${directionMaxValues.O}
💎 ${directionLabels.K}: ${teamStats.points.K} / ${directionMaxValues.K}
💎 ${directionLabels.B}: ${teamStats.points.B} / ${directionMaxValues.B}
💎 ${directionLabels.total}: ${teamStats.points.total} / ${directionMaxValues.total}`;
}

export function getCompleteTrackTasksLabel(role: string) {
    if (!TRACKS_LABELS[role]) {
        return null;
    }
    return `${COMPLETE_TRACK_TASKS_LABEL} ${TRACKS_LABELS[role]}:`;
}

export function enrichMenuWithCompleteTasks(menu: any, stats: ITeamStats): any {

    const switchedRedirects: any = {};
    if (!stats.complete_tasks_codes) {
        return menu;
    }
    // Перебираем коды завершенных заданий
    for (let i = 0; i < stats.complete_tasks_codes.length; i++) {
        const completeTaskCode = stats.complete_tasks_codes[i];

        // достаем неделю для завершенного задания
        const weekCode = `marathon_M${completeTaskCode[1]}`;
        const week = menu[weekCode];


        let redirectsSwitch: any = {}; // переворачиваем редиректы Названия кнопок-Коды => Коды-Названия кнопок
        if (!switchedRedirects[weekCode] && !!week) {
            for (const key in week.redirects) {
                if (Object.prototype.hasOwnProperty.call(week.redirects, key)) {
                    const redirectTarget = week.redirects[key];
                    redirectsSwitch[redirectTarget] = key;
                }
            }
            switchedRedirects[weekCode] = redirectsSwitch;
        }
        redirectsSwitch = switchedRedirects[weekCode];

        if (week && week.messages) {
            let taskSectionKey = `task_${completeTaskCode}`;

            // Если раздела с таким кодом задания нет, то меняем у него уровень. по умолчанию это 1. M1T4L2 => M1T4L1
            if (!menu[taskSectionKey]) {
                taskSectionKey = `task_${completeTaskCode.substr(0, 5)}1`;
            }

            const newRedirects: any = {};

            if (!redirectsSwitch[taskSectionKey]) {
                continue;
            }

            newRedirects[`${COMPLETE_TASK_MARK} ${redirectsSwitch[taskSectionKey]}`] = taskSectionKey;

            // вставляем сообщение в раздел с заданием о том что задание выполнено
            const taskSection = menu[taskSectionKey]
            if (taskSection) {
                const task = stats.complete_tasks[completeTaskCode]; //.find((task) => task.code == completeTaskCode);
                const taskPerformedMessage = getTaskPerformedMessage(taskSection, menu, task?.value);

                taskSection.messages.push(taskPerformedMessage);
            }

            // затем для всех сообщений в разделе недели обновляем кнопки, чтобы они были с галочками
            for (let j = 0; j < week.messages.length; j++) {
                const message = week.messages[j];
                if (!message.buttons) {
                    continue;
                }
                for (let k = 0; k < message.buttons.length; k++) {
                    const button = message.buttons[k];

                    if (button == redirectsSwitch[taskSectionKey]) {
                        message.buttons[k] = COMPLETE_TASK_MARK + ' ' + button;
                    }
                }
            }

            if (week.redirects) {
                Object.assign(week.redirects, newRedirects);
            }
        }

    }

    return menu;
}

export function enrichMenuWithCompleteTaskResults(menu: any, stats: ITeamStats) {
    if (!stats || !stats.complete_tasks_codes) {
        return menu;
    }
    for (let i = 0; i < stats.complete_tasks_codes.length; i++) {
        const completeTaskCode = stats.complete_tasks_codes[i];
        let taskSectionKey = `task_${completeTaskCode}`;
        // Если раздела с таким кодом задания нет, то меняем у него уровень. по умолчанию это 1. M1T4L2 => M1T4L1
        if (!menu[taskSectionKey]) {
            taskSectionKey = `task_${completeTaskCode.substr(0, 5)}1`;
        }
        // вставляем сообщение в раздел с заданием о том что задание выполнено
        const taskSection = menu[taskSectionKey]
        if (taskSection) {
            const task = stats.complete_tasks[completeTaskCode];
            const taskPerformedMessage = getTaskPerformedMessage(taskSection, menu, task?.value);

            taskSection.messages.push(taskPerformedMessage);
        }
    }

    return menu;
}

function getTaskPerformedMessage(taskSection: any, menu: any, taskAnswer: any) {
    const taskSectionButtons = taskSection.messages[0].buttons;
    const taskPerformedMessage = Object.assign({}, menu['task_performed'].messages[0]);
    if (taskSectionButtons) {
        taskPerformedMessage.buttons = new Array(...taskSectionButtons);
        taskPerformedMessage.buttons_count = taskPerformedMessage.buttons.length;
    }
    else {
        taskPerformedMessage.buttons_count = 0;
    }

    let taskAnswers: string[] = [];
    for (const key in taskAnswer) {
        if (Object.prototype.hasOwnProperty.call(taskAnswer, key)) {
            const value = taskAnswer[key];
            taskAnswers.push(`✏️ ${value}`);
        }
    }
    taskPerformedMessage.text = taskPerformedMessage.text.replace('{task.answer}', taskAnswers.join('\n\n'));
    return taskPerformedMessage;
}