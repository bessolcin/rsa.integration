export interface IBaseStats {
    complete_tasks_count: number;
    complete_tasks: { [key: string]: ICompleteTask };
    complete_tasks_codes: string[];
    points: IPointsStats;

    marathon_outro_permissions: { [key: string]: boolean };
    marathon_prizes_permissions: { [key: string]: boolean };
}

export interface ITeamStats extends IBaseStats {
    // current_member_complete_tasks: number;
    // current_member_role_complete_tasks: number;
    members: string[];
    timestamp?: number;
}

export interface IPersonalStats extends IBaseStats {
    current_role_complete_tasks_count: number;

    role_complete_tasks_count: number;
    role_complete_tasks: { [key: string]: ICompleteTask };
    role_complete_tasks_codes: string[];
    role_points: IPointsStats;
}

export interface IStats {
    team_stats: ITeamStats | null;
    personal_stats: IPersonalStats | null;
}

export enum StatsType {
    team_stats = 'team_stats',
    personal_stats = 'personal_stats'
}


export interface ICompleteTask {
    code: string;
    value: any;
    points: IPointsStats;
    // direction: string;
    task_text?: string;
    date?: string;
    time?: string;
}

export interface IPointsStats {
    /**
     *  В — выпускники. Взаимодействие школы с выпускниками. 
        И — индивидуальность. Поддержка индивидуального прогресса учеников.
        Ш — школьники. Среда для самореализации школьников. 
        У — учителя. Комфортная среда для учителей. 
        С — сообщество. Взаимодействие внутри школьного сообщества. 
        Л — лидерство. Совместное принятие решений и распределенное лидерство. 
        Р — родители. Вовлечение родителей и семей. 
        О — общество. Общественная активность (волонтерство).
        К — коммуникация. Бренд, соцсети, СМИ. 
     */
    V: number;
    I: number;
    SH: number;
    U: number;
    S: number;
    L: number;
    R: number;
    O: number;
    K: number;
    B: number;
    total: number;
}

export class PointsStats implements IPointsStats {

    public static directions: string[] = ['V', 'I', 'SH', 'U', 'S', 'L', 'R', 'O', 'K', 'B'];

    public V: number = 0;
    public I: number = 0;
    public SH: number = 0;
    public U: number = 0;
    public S: number = 0;
    public L: number = 0;
    public R: number = 0;
    public O: number = 0;
    public K: number = 0;
    public B: number = 0;
    public total: number = 0;

    public add(points: IPointsStats): void {
        this.total = 0;
        for (let i = 0; i < PointsStats.directions.length; i++) {
            const d = PointsStats.directions[i];
            (<any>this)[d] += (<any>points)[d];
            this.total += (<any>this)[d];
        }
    }

    public static fromObject(o: any): PointsStats {
        const points = new PointsStats();
        const addPoints: any = { total: 0 };

        for (let i = 0; i < PointsStats.directions.length; i++) {
            const d = PointsStats.directions[i];
            addPoints[d] = !!o[d] ? parseInt(o[d]) : 0;
            addPoints.total += addPoints[d];
        }
        points.add(addPoints);

        return points;
    }

}

export enum TaskResultState {
    Approved = 'approved',
    Declined = 'declined',
    Review = 'review'
}

export interface ITask {
    Marathon: string;
    Number: string;
    Level: string;
    Code: string;
    Condition: string;
    V: string;
    I: string;
    SH: string;
    U: string;
    S: string;
    L: string;
    R: string;
    O: string;
    K: string;
    B: string;
    Roles: string;
    Answer: string;
    Published: string;
    CreateTicket: string;
    Points: IPointsStats;
    Required: string;
}

export interface ITasksDictionary {
    [key: string]: ITask;
}