import { ILogger } from '@bessolcin/coolstory.bots.lib';
import { Express, Request, Response } from 'express';
import { database as db } from 'firebase-admin';
import { TEAMS_KEY } from '../constants';
import { StatsManager } from '../managers';

const headerObject = {
    date: null,
    time: null,
    firstname: null,
    lastname: null,
    role: null,
    form_name: null,
    country: null,
    region: null,
    city: null,
    settlement: null,
    school: null,
    bm_id: null,
    platform: null,
    user_id: null,
    team_id: null,
    platform_id: null,
    dialog_url: null
};

export class StatsController {
    /**
     *
     */
    constructor(private logger: ILogger, app: Express, private db: db.Database, private statsManager: StatsManager) {
        this.configureRequestHandlers(app);
    }

    private configureRequestHandlers(app: Express) {
        app.get('/stats/task/:taskCode', this.onStatsTaskCode.bind(this));
        app.get('/stats/tasks/update', this.onStatsTasksUpdate.bind(this));
        app.post('/stats/save', this.onStatsBackup.bind(this));
    }

    private async onStatsTaskCode(req: Request, res: Response) {
        const taskCode = req.params.taskCode;

        if (!taskCode) {
            return res.json(null);
        }

        const challengeStartDate = new Date(2021, 8, 1);
        const challengeStartDateTS = challengeStartDate.getTime();

        const tasks = (await this.db.ref(`forms/results`).orderByChild('form_name').equalTo(taskCode).once('value')).val();

        if (tasks) {
            const relevantTasks = await Promise.all(Object.values(tasks)
                .filter((t: any) => !t.canceled && t.timestamp >= challengeStartDateTS)
                .map((t: any) => {

                    return this.db.ref(`${TEAMS_KEY}/${t.team_id}/organization`).once('value').then(d => d.val()).then(org => {
                        return {
                            date: t.date,
                            time: t.time,

                            firstname: t.firstname,
                            lastname: t.lastname,
                            role: t.role,
                            form_name: t.form_name,
                            ...t.form_value,

                            country: org?.country || `uid_${t.user_id}`,
                            region: org?.region || null,
                            city: org?.city || null,
                            settlement: org?.settlement || null,
                            school: org?.school || null,

                            bm_id: t.bm_id,
                            platform: t.platform,
                            user_id: t.user_id,
                            team_id: t.team_id,
                            platform_id: t.platform_id,
                            dialog_url: t.dialog_url
                        };
                    })
                }));

            if (req.query.csv === 'true') {
                const separator = ',';
                const rows: string[] = [];
                const headerRow = Object.keys(relevantTasks.length ? relevantTasks[0] : headerObject).join(separator);

                rows.push(headerRow);
                rows.push(...relevantTasks.map(t => (<string[]>Object.values(t)).map(v => `"${v ? v.replace(/\n/g, ' ') : ''}"`).join(separator)));

                const csv = rows.join('\n');

                res.setHeader('Content-Type', 'application/vnd.ms-excel');
                res.setHeader('Content-disposition', `attachment; filename=stats_${taskCode}_${(new Date()).toISOString()}.csv`)
                res.setHeader('Content-Length', Buffer.byteLength(csv, 'utf8'));

                return res.send(csv);
            }

            return res.json(relevantTasks);
        }
        return res.json(null);
    }

    private async onStatsTasksUpdate(req: Request, res: Response) {
        const tasks = await this.statsManager.updateTasks();

        return res.json(tasks);
    }

    private async onStatsBackup(req: Request, res: Response) {
        try {
            console.info('Saving stats history...');
            await this.statsManager.saveStatsHistory();
            console.info('Stats history saved.');

            return res.json({ result: 'ok' });
        } catch (error) {
            console.error('Error while saving stats history', error);
            this.logger.alert('Error while saving stats history ' + (<any>error)?.message);
            return res.status(400).json({ result: 'error' });
        }
    }
}