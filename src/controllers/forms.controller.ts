import { Express, Request, Response } from 'express';
import { database } from 'firebase-admin';

import { formatDateRu, SendAlertToTelegram, SpreadsheetManager } from '@bessolcin/coolstory.bots.lib';

import { IUsersManager } from './../managers';
import { FormHandler } from './../form-handlers';
import { IFormData, IPointsStats, IUser } from '../models';
import { RSAEnvConfig } from '../envconfig';
import { isSocialLink } from '../helpers';
import { TeamsRepository } from '../repositories';
import { TEAM_STATS_KEY } from '../constants';

export class FormsController {

    private formValidators: any = {};

    constructor(
        app: Express,
        private db: database.Database,
        private spreadsheetManager: SpreadsheetManager,
        private usersManager: IUsersManager,
        private teams: TeamsRepository,
        private formHandler: FormHandler) {
        this.configureRequestHandlers(app);

        this.formValidators['results'] = this.resultsFormValidator.bind(this);
        this.formValidators['rf_dt_form'] = this.diagnosticsToolFormValidator.bind(this);
        this.formValidators['partnership_application'] = this.teamWithCodeExistsValidator.bind(this);
    }

    private configureRequestHandlers(app: Express) {
        app.post('/form/:formId/save', this.onFormIdSave.bind(this));
        app.post('/form/:formId/validate', this.onFormIdValidate.bind(this));
        app.get('/form/results/cancel', this.onFormResultsCancel.bind(this));
        app.get('/forms/sync', this.onFormsSync.bind(this));
    }

    public async onFormIdSave(req: Request, res: Response) {
        const bucketId = req.params.formId;
        const data: any = req.body;
        const needLog = req.query.needLog === 'true';

        const formBucket = (await this.db.ref(`bot/settings/form_buckets/${bucketId}`).once('value')).val();

        if (!formBucket) {
            console.error(`Form bucket with id ${bucketId} not found.`);
            res.status(400).json({ result: `Form bucket with id ${bucketId} not found.` });
            return;
        }

        try {
            if (needLog) {
                console.log('Request body', JSON.stringify(req.body));
            }

            const user: IUser | null = await this.usersManager.getUserByBmId(data.bm_id);

            if (!user) {
                console.warn(`User not found and saves form ${data.form_name} bmid= ${data.bm_id}`);
            }

            if (this.formValidators[bucketId]) {
                const validatorFunc = this.formValidators[bucketId];
                const validationResult = await validatorFunc(data, user);
                console.info(`Validation results is ${validationResult}`);

                if (validationResult !== 'ok') {
                    return res.json({ result: validationResult });
                }
            }

            const now = new Date();
            const timestamp = now.getTime();
            now.setMinutes(180 + now.getMinutes());
            const date = formatDateRu(now);
            const time = now.toLocaleTimeString();

            const sheetData: IFormData = {
                ...data,
                date: date,
                time: time,
                user_id: user?.id || null,
                team_id: user?.team_id || null,
                team_code: user?.team_code || null,
                firstname: user?.firstname || null,
                lastname: user?.lastname || null,
                role: user?.role || null,
                timestamp: timestamp
            };

            const recordRef = await this.db.ref(`forms/${bucketId}`).push(sheetData);

            if (formBucket.need_save_to_doc !== false) {
                console.info(`Saving data to form sheet ${formBucket.sheet_name}...`);
                await this.spreadsheetManager.appendDataToSheet(
                    formBucket.doc_id,
                    formBucket.sheet_name,
                    {
                        ...data.form_value,
                        ...sheetData
                    });
                console.info(`Data saved to form sheet ${formBucket.sheet_name}`);
            }

            console.info(`Handling form ${bucketId} results...`);
            await this.formHandler.handle(bucketId, sheetData, recordRef.key || '', user || undefined);
            console.info(`Handled form ${bucketId} results.`);
        } catch (error) {
            console.error('Error while adding data to form bucket ', formBucket, { url: req.url, error: error });

            await SendAlertToTelegram(RSAEnvConfig.ALERT_BOT_TOKEN, RSAEnvConfig.ALERT_CHAT_ID, `
            Error while adding data to form bucket ${bucketId}
            url: ${req.originalUrl}
            bm_id: ${data.bm_id}`);
        }

        return res.json({ result: 'ok' });
    }

    // TODO Переделать на нормальный синк как в санте
    public async onFormsSync(res: Response) {
        const bucketId = 'master_questions';
        const formBucket = (await this.db.ref(`bot/settings/form_buckets/${bucketId}`).once('value')).val();

        const questions = (await this.db.ref(`forms/master_questions`).orderByChild('date').equalTo('25.10.2020').once('value')).val();

        for (const question in questions) {
            if (Object.prototype.hasOwnProperty.call(questions, question)) {
                const element = questions[question];
                await this.spreadsheetManager.appendDataToSheet(formBucket.doc_id, formBucket.sheet_name, element);

            }
        }
        return res.json(questions);
    }

    public async onFormResultsCancel(req: Request, res: Response) {
        const resultId = req.query.res;
        try {
            if (resultId) {
                const resultRef = this.db.ref(`forms/results/${resultId}`);
                await resultRef.update({ canceled: true });
                const userId = (await resultRef.child('user_id').once('value')).val();
                const taskCode = (await resultRef.child('form_name').once('value')).val();
                const user = await this.usersManager.getUserByPhone(userId);

                await this.formHandler.handleResultCancellation(userId, user?.role || '', '' + resultId);

                return res.send(`У участника ${user?.firstname} ${user?.lastname} за задание ${taskCode} с ид= ${resultId} кристаллики сняли.`);
            }
            else {
                return res.send(`Результат участника с ид=${resultId} не найден`);
            }
        } catch (error) {
            console.error('Error while adding data to form bucket ', resultId, { url: req.url, error: error });

            await SendAlertToTelegram(RSAEnvConfig.ALERT_BOT_TOKEN, RSAEnvConfig.ALERT_CHAT_ID, `
            Error while adding data to form bucket
            url: ${req.originalUrl}
            resultId: ${resultId}`);

            return res.send(`При снятии кристалликов произошла ошибка :(`);
        }
    }

    public async onFormIdValidate(req: Request, res: Response) {
        const bucketId = req.params.formId;
        const data: any = req.body;

        const formBucket = (await this.db.ref(`bot/settings/form_buckets/${bucketId}`).once('value')).val();

        const needLog = req.query.needLog === 'true';

        if (!formBucket) {
            console.error(`Form bucket with id ${bucketId} not found.`);
            res.json({ result: `Form bucket with id ${bucketId} not found.` });
            return;
        }

        try {
            if (needLog) {
                console.log('Request body', req.body);
            }

            if (this.formValidators[bucketId]) {
                const validatorFunc = this.formValidators[bucketId];
                const validationResult = validatorFunc(data);
                console.info(`Validation results is ${validationResult}`);

                return res.json({ result: validationResult });
            }

        } catch (error) {
            console.error(`Error while validating form value for ${bucketId}`, { url: req.originalUrl, error: error });
        }
        return res.json({ result: 'ok' });
    }

    private async resultsFormValidator(data: IFormData, user: IUser | null): Promise<string> {
        const [requiredAnswerType, requiredRole, requiredPoints] = await Promise.all([
            this.db.ref(`bot/content/tasks/${data.form_name}/Answer`).once('value').then(ds => ds.val()),
            this.db.ref(`bot/content/tasks/${data.form_name}/Roles`).once('value').then(ds => ds.val()),
            this.db.ref(`bot/content/tasks/${data.form_name}/Points`).once('value').then(ds => ds.val())
        ]);

        if (!user) {
            return 'form_user_not_found';
        }
        else if (requiredRole != 'all' && requiredRole != user.role) {
            return `form_cannot_save_role_mb_${requiredRole}`;
        }

        if (!data) {
            return 'form_value_is_null';
        }
        if (!data.form_value) {
            return 'form_value_is_null';
        }

        if (data.form_name == 'M4T4L1') {
            return await this.teamWithCodeExistsValidator(data, user)
        }

        if (requiredAnswerType !== 'link') {
            return 'ok';
        }

        if (data.form_name.startsWith('M8')) {
            if ((<IPointsStats>requiredPoints).total >= 900) {
                const teamPoints: number = (await this.db.ref(`${TEAM_STATS_KEY}/${user.team_id}/points/total`).once('value')).val();

                if (teamPoints < (<IPointsStats>requiredPoints).total) {
                    return 'not_enough_points';
                }
            }
        }

        const formValue: IResultsFormValue = data.form_value;

        if (data.form_name == 'M1T2L1') {
            if (!data.form_value.superpower || data.form_value.superpower.length > 140) {
                return 'task_invalid_superpower';
            }

            const SKIP_BTN = 'ПРОПУСТИТЬ';
            if (!(<string>data.form_value.link_vk).trim().toUpperCase().endsWith(SKIP_BTN) && !isSocialLink(data.form_value.link_vk)) {
                return 'task_link_not_social';
            }
            if (!(<string>data.form_value.link_instagram).trim().toUpperCase().endsWith(SKIP_BTN) && !isSocialLink(data.form_value.link_instagram)) {
                return 'task_link_not_social';
            }
            if (!(<string>data.form_value.link_fb).trim().toUpperCase().endsWith(SKIP_BTN) && !isSocialLink(data.form_value.link_fb)) {
                return 'task_link_not_social';
            }
            if (!(<string>data.form_value.link_tiktok).trim().toUpperCase().endsWith(SKIP_BTN) && !isSocialLink(data.form_value.link_tiktok)) {
                return 'task_link_not_social';
            }

            return 'ok';
        }

        if (!formValue.value1) {
            return 'task_link_is_null';
        }

        if (!isSocialLink(formValue.value1)) {
            return 'task_link_not_social';
        }
        return 'ok';
    }

    private diagnosticsToolFormValidator(data: IFormData, user?: IUser) {
        const formValue: any = data.form_value;

        if (!formValue.students_count || !formValue.teachers_count || !formValue.graduate_estimate) {
            return 'form_value_is_null';
        }

        let val = parseInt(formValue.students_count.replace(/\D/g, ''));
        if (typeof (val) != 'number' || isNaN(val)) {
            return 'rf_dt_form_value_invalid';
        }

        val = parseInt(formValue.teachers_count.replace(/\D/g, ''));
        if (typeof (val) != 'number' || isNaN(val)) {
            return 'rf_dt_form_value_invalid';
        }

        val = parseInt(formValue.graduate_estimate.replace(/\D/g, ''));
        if (typeof (val) != 'number' || isNaN(val)) {
            return 'rf_dt_form_value_invalid';
        }

        return 'ok';
    }

    private async teamWithCodeExistsValidator(data: IFormData, user?: IUser) {

        if (!data.form_value.team_code || data.form_value.team_code.length != 6) {
            return 'team_code_invalid';
        }

        if (data.form_value.team_code == user?.team_code) {
            return 'team_code_wrong';
        }

        const team = await this.teams.getTeamByCode(data.form_value.team_code)
        if (!team) {
            return 'team_not_exists';
        }

        return 'ok';
    }
}


export interface IResultsFormValue {
    value1: string;
}