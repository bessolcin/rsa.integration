export interface ITeam {
	id: string;
	leader: string;
	org_id: string;
	referral_code: string;
	partners?: IPartner[];
	diagnostics_tool?: boolean;
}

export interface IPartner {
	code: string;
	name: string;
}