import { database as db } from 'firebase-admin';
import { ORGANIZATIONS_KEY, PHONE_TO_BMID_KEY, TEAMS_KEY, USERS_KEY, USER_TO_TEAM_KEY } from '../constants';
import { ITeam } from '../models';

export class TeamsRepository {
    /**
     *
     */
    constructor(private db: db.Database) {

    }

    public async getTeamBmIds(teamId: string, excludeUserId: string): Promise<string[]> {
        const teamMembersUserIdsSnap = await this.db.ref(USER_TO_TEAM_KEY).orderByValue().equalTo(teamId).once('value');
        const teamMembersIdsObject = teamMembersUserIdsSnap.val();

        const teamMembersUserIds: string[] = []; //Object.keys(teamMembersIdsObject);
        for (const teamUserId in teamMembersIdsObject) {
            if (Object.prototype.hasOwnProperty.call(teamMembersIdsObject, teamUserId)) {
                if (teamUserId != excludeUserId) {
                    teamMembersUserIds.push(teamUserId);
                }
            }
        }

        const bmIdsPromises = teamMembersUserIds.map((userId: string) =>
            this.db.ref(`${USERS_KEY}/${userId}/phone`).once('value')
                .then((phoneSnap: db.DataSnapshot) => phoneSnap.val())
                .then((phone: string) => this.db.ref(`${PHONE_TO_BMID_KEY}/${phone}`).once('value'))
                .then((bmIds: db.DataSnapshot) => {
                    const phoneToBmIds = bmIds.val();
                    const phoneBmIds = [];
                    for (const bmId in phoneToBmIds) {
                        if (Object.prototype.hasOwnProperty.call(phoneToBmIds, bmId)) {
                            phoneBmIds.push(bmId)
                        }
                    }
                    return phoneBmIds;
                })
        );

        const allBmIds = [];
        const bmidsArray = await Promise.all(bmIdsPromises);
        for (let i = 0; i < bmidsArray.length; i++) {
            const userBmIds = bmidsArray[i];
            allBmIds.push(...userBmIds);
        }

        return allBmIds;
    }

    public async getTeamById(teamId: string): Promise<ITeam | null> {
        const teamById = (await this.db.ref(`${TEAMS_KEY}/${teamId}`).once('value')).val();
        return teamById;
    }

    public async getTeamByCode(teamCode: string): Promise<ITeam | null> {
        try {
            const teamsByCode = (await this.db.ref(TEAMS_KEY).orderByChild('referral_code').equalTo(teamCode).limitToFirst(1).once('value')).val();

            if (!teamsByCode) {
                return null;
            }

            for (const teamId in teamsByCode) {
                if (Object.prototype.hasOwnProperty.call(teamsByCode, teamId)) {
                    const team = teamsByCode[teamId];
                    team.id = teamId;

                    return team;
                }
            }

            return null;

        } catch (error) {
            console.error(`Team with code not found! ${teamCode}`, error);
            return null;
        }
    }

    public async updateTeam(id: string, update: any): Promise<any> {
        return await this.db.ref(`${TEAMS_KEY}/${id}`).update(update);
    }

    public async getOrganization(orgId: string): Promise<any> {
        return (await this.db.ref(`${ORGANIZATIONS_KEY}/${orgId}`).once('value')).val();
    }

    public async getTeamIdAndCode() {
        let newTeamId = <string>this.db.ref(TEAMS_KEY).push().key;
        let teamCode = this.shortenCode(newTeamId, 6);

        let counter = 0;
        while ((await this.db.ref(TEAMS_KEY).orderByChild('referral_code').equalTo(teamCode).once('value')).exists()) {
            newTeamId = <string>this.db.ref(TEAMS_KEY).push().key;
            teamCode = this.shortenCode(newTeamId, 6);
            counter++;
            console.log(`Created teamcode in ${counter} time.`)
            if (counter > 10) {
                throw new Error('Cannot create unique team code 10 times!');
            }
        }

        return [newTeamId, teamCode];
    }

    private shortenCode(code: string, length: number) {
        return code?.replace(/[_\-]/g, '').substr(-1 * length).toUpperCase()
    }
}