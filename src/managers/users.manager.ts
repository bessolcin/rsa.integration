import { database as db } from 'firebase-admin';
import { callBotmotherWebhook, formatPhoneNumber } from '@bessolcin/coolstory.bots.lib';

import { RSAEnvConfig } from '../envconfig';

import { IUser, IUserShortInfo, UserStatus } from '../models/users';
import { TeamsRepository } from '../repositories';

const PHONE_TO_BMID_KEY = 'phone_to_bmid';
const BMID_TO_PHONE_KEY = 'bmid_to_phone';
const BMID_TO_USERID_KEY = 'bmid_to_userid';

// const rolesNameToCodeMapping: { [key: string]: string } = {
//     'Ученик': 'student',
//     'Выпускник': 'exstudent',
//     'Учитель': 'teacher',
//     'Партнер': 'partner',
//     'Родитель': 'parent',
//     'Директор': 'director',
//     'Администрация школы': 'admin'
// };

// const rolesCodeToNameMapping: { [key: string]: string } = {
//     'student': '🙋 Ученик',
//     'exstudent': '👨‍🎓 Выпускник',
//     'teacher': '👩‍🏫 Учитель',
//     'partner': '🤵 Партнер',
//     'parent': '👩‍👧‍👦 Родитель',
//     'director': '👨‍💼 Директор',
//     'admin': '🏫 Администрация школы',
//     'not_from_school': '✌️ Не из школы'
// };

export class UsersManager {

    private users: db.Reference;

    /**
     *
     */
    constructor(private db: db.Database, private teamsRepository: TeamsRepository) {
        this.users = this.db.ref('users');
    }

    public async getUserByBmId(bmId: string): Promise<IUser | null> {
        if (!bmId) {
            return null;
        }

        return this.db.ref(`bmid_to_phone/${bmId}`).once('value').then(phoneSnap => this.getUserByPhone(phoneSnap.val()));
        // const phone = (await this.db.ref(`bmid_to_phone/${bmId}`).once('value')).val();

        // return await this.getUserByPhone(phone);
    }

    public async getAllUserBmIds(bmId: string): Promise<string[]> {
        const phone = (await this.db.ref(`bmid_to_phone/${bmId}`).once('value')).val();
        const bmIds = (await this.db.ref(`phone_to_bmid/${phone}`).once('value')).val();

        return Object.keys(bmIds) || [];
    }

    public async createUser(userData: any): Promise<void> {
        const user: IUser = this.mapToUser(userData);
        user.status = UserStatus.Leader;
        user.created_date = (new Date()).getTime();
        user.created_date_offset = (new Date()).getTimezoneOffset();

        const usersWithPhoneSnapshot = await this.getUserSnapshotByPhone(user.phone);

        if (usersWithPhoneSnapshot.hasChildren()) {
            const duplicateRef = await this.db.ref('duplicates/users').push(user);
            console.error(`User with phone number already exists ${user.phone}. Saved duplicate with id = ${duplicateRef?.key}`);
            return;
        }

        const newTeamId = this.db.ref('teams').push().key;
        const newUserId = this.db.ref('users').push().key;

        const update: any = {};
        update['teams/' + newTeamId + '/leader'] = newUserId;
        update['teams/' + newTeamId + '/referral_code'] = newTeamId?.replace(/[_\-]/g, '').substr(-15).toUpperCase();
        update['users/' + newUserId] = user;
        update['user_to_team/' + newUserId] = newTeamId;

        const bmIds: string[] = [];
        const phoneToBmIdSnap = await this.db.ref(`${PHONE_TO_BMID_KEY}/${user.phone}`).once('value');
        // Если уже есть пользователь в чатботе
        if (phoneToBmIdSnap.exists() && phoneToBmIdSnap.hasChildren()) {
            phoneToBmIdSnap.forEach((bmIdSnap: db.DataSnapshot) => {
                bmIds.push(bmIdSnap.key || '');
                // То заодно добавим индекс идБотмамы к юзерИД
                update[`${BMID_TO_USERID_KEY}/${bmIdSnap.val()}`] = newUserId;
            });
        }

        await this.db.ref().update(update);

        if (bmIds.length) {
            console.info('Found user to bmid relation, calling webhook...');
            await callBotmotherWebhook(RSAEnvConfig.BOTMOTHER_USER_CREATED_CALLBACK, 'BOTMOTHER_USER_CREATED_CALLBACK', bmIds, user);
        }
    }

    public async bindUserWithBmId(phone: string, bmId: string): Promise<IUser | null> {
        console.info(`User with bmid=${bmId} trying to get user with phone XXXXX${phone.substr(5)}.`);
        const currentBmIdToPhone = (await this.db.ref(`${BMID_TO_PHONE_KEY}/${bmId}`).once('value'));
        if (currentBmIdToPhone.exists()) {
            const oldPhone = currentBmIdToPhone.val();
            await this.db.ref(`${PHONE_TO_BMID_KEY}/${oldPhone}/${bmId}`).set(null);
        }

        const userByPhone = await this.getUserByPhone(phone);
        await this.bindPhoneWithBmId(phone, userByPhone?.id, bmId);
        console.info(`User with bmid=${bmId} got user with phone XXXXX${phone.substr(5)}. UserId = ${userByPhone?.id || null}`);

        return userByPhone;
    }

    public async notifyUserLogin(bmId: string, platform: string) {
        if (bmId) {
            const bmids = (await this.getAllUserBmIds(bmId)).filter(bmid => bmid != bmId);
            await callBotmotherWebhook(
                RSAEnvConfig.BOTMOTHER_REDIRECT_TO_SECTION_CALLBACK,
                'BOTMOTHER_REDIRECT_TO_SECTION_CALLBACK/login_notification',
                bmids,
                { redirect: 'login_notification' });
            console.warn(`User with bmid=${bmId} logged in in ${platform}`);
        }
        else {
            console.warn('BmId is empty in onUsersNotify');
        }
    }

    public async deleteUser(phone: string) {
        const update: any = {};
        update[`${PHONE_TO_BMID_KEY}/${phone}`] = null;

        const phoneToBmIdSnap = await this.db.ref(`${PHONE_TO_BMID_KEY}/${phone}`).once('value');
        phoneToBmIdSnap.forEach((s: db.DataSnapshot) => {
            update[`${BMID_TO_PHONE_KEY}/${s.key}`] = null;
            update[`${BMID_TO_PHONE_KEY}/${s.key}`] = null;
        });

        const userSnapshot = await this.getUserSnapshotByPhone(phone);
        userSnapshot.forEach((u: db.DataSnapshot) => {
            update[`users/${u.key}`] = null;
            update[`user_to_team/${u.key}`] = null;
        });

        await this.db.ref().update(update);
    }

    public async joinUsersTeam(currentUserBmId: string, teamCode: string, connectToPhone: string | null) {
        let currentUserPhone: string = (await this.db.ref(`${BMID_TO_PHONE_KEY}/${currentUserBmId}`).once('value')).val();

        console.info('Getting user membership for joining, bmid=', currentUserBmId);
        const currentUserMembership = await this.getUserTeamMembership(currentUserPhone);

        if (!currentUserMembership) {
            console.warn(`Current user membership object not found!`);
            return 'team_user_notexist';
        }

        // если текущий юзер капитан команды в которой есть кто-то кроме него
        if (currentUserMembership &&
            currentUserMembership.userId == currentUserMembership.usersTeamLeaderId &&
            currentUserMembership.teamMembersCount > 1) {
            console.info(`User with bmid=${currentUserBmId} already leads team with members.`);
            return 'team_cannot_join_youre_leader';
        }
        const update: any = {};

        let targetLeaderMembership: any;
        if (connectToPhone) {
            targetLeaderMembership = await this.getUserTeamMembership(connectToPhone);
            if (targetLeaderMembership == null) {
                console.info(`User with phone=${connectToPhone} not found.`);
                return 'team_leader_with_phone_not_found';
            }

            update[`user_to_team/${currentUserMembership.userId}`] = targetLeaderMembership.usersTeamId;
            update[`users/${currentUserMembership.userId}/status`] = UserStatus.Common;
        } else if (teamCode) {
            targetLeaderMembership = await this.getUserTeamMembershipByCode(teamCode);
            if (targetLeaderMembership == null) {
                console.info(`Team with code=${teamCode} not found.`);
                return 'team_with_code_not_found';
            }
            update[`user_to_team/${currentUserMembership.userId}`] = targetLeaderMembership.usersTeamId;
            update[`users/${currentUserMembership.userId}/status`] = UserStatus.Common;
        }

        // if (newLeaderMembership.userId != newLeaderMembership.usersTeamLeaderId) {
        //     return 'cannot_join_hes_not_leader';
        // }

        await this.db.ref().update(update);
        const roleLabel = RSAEnvConfig.ROLE_LABELS[currentUserMembership.user.role]; // (await this.db.ref(`role_labels/${currentUserMembership.user.role}`).once('value')).val();

        const teamBmIds = await this.teamsRepository.getTeamBmIds(targetLeaderMembership.usersTeamId, currentUserMembership.userId);
        await callBotmotherWebhook(RSAEnvConfig.BOTMOTHER_NEW_TEAM_MEMBER_CALLBACK, 'BOTMOTHER_NEW_TEAM_MEMBER_CALLBACK', teamBmIds, {
            redirect: 'меню',
            message_section: 'team_new_member',
            new_member: `${currentUserMembership.user.firstname} ${currentUserMembership.user.lastname || ''} – ${roleLabel || ''}.` || (<any>currentUserMembership.user).name
        });
        console.info(`User with bmid=${currentUserBmId} joined team of ${connectToPhone}.`);
        return 'team_join_success';
    }


    private async getUserTeamMembership(phone: string) {
        let user: IUser | null = null;
        user = await this.getUserByPhone(phone);

        if (!user) {
            return null;
        }

        let teamMembersCount = 0;
        const teamMembersSnap = await this.db.ref('user_to_team').orderByValue().equalTo(user.team_id).once('value');
        teamMembersCount = teamMembersSnap.numChildren();

        const currentTeamLeaderId = (await this.db.ref(`teams/${user.team_id}/leader`).once('value')).val();

        return {
            user: user,
            userId: user.id,
            usersTeamId: user.team_id,
            usersTeamLeaderId: currentTeamLeaderId,
            teamMembersCount: teamMembersCount
        };
    }

    private async getUserTeamMembershipByCode(teamCode: string) {
        try {
            const teamsByCode = (await this.db.ref('teams').orderByChild('referral_code').equalTo(teamCode).limitToFirst(1).once('value')).val();

            const teamIds = Object.keys(teamsByCode);
            if (!teamIds || teamIds.length == 0) {
                return null;
            }
            return { usersTeamId: teamIds[0] };

        } catch (error) {
            console.error(`Team with code not found! ${teamCode}`, error);
            return null;
        }
    }


    private async bindPhoneWithBmId(phone: string, userId: string | undefined, bmId: string): Promise<void> {
        await this.db.ref(`${BMID_TO_PHONE_KEY}/${bmId}`).remove();

        const update: any = {};
        update[`${PHONE_TO_BMID_KEY}/${phone}/${bmId}`] = true;
        update[`${BMID_TO_PHONE_KEY}/${bmId}`] = phone;
        if (userId) {
            update[`${BMID_TO_USERID_KEY}/${bmId}`] = userId;
        }
        await this.db.ref().update(update);
    }

    private getUserSnapshotByPhone(phone: string): Promise<db.DataSnapshot> {
        return this.users.orderByChild('phone').equalTo(phone).once('value');
    }

    public async getUserByPhone(phone: string): Promise<IUser | null> {
        const userSnapshot = await this.users.orderByChild('phone').equalTo(phone).once('value');
        const userByPhone = userSnapshot.val();

        let user: IUser | null = null;
        let id: string | null = null;
        for (const key in userByPhone) {
            if (Object.prototype.hasOwnProperty.call(userByPhone, key)) {
                user = userByPhone[key];
                id = key;
            }
        }

        if (user) {
            (<IUser>user).team_id = (await this.db.ref(`user_to_team/${id}`).once('value')).val();
            (<IUser>user).team_code = (await this.db.ref(`teams/${user.team_id}/referral_code`).once('value')).val();
            (<IUser>user).id = id as unknown as string;
        }

        return user;
    }

    public async getUserShortInfo(userId: string): Promise<IUserShortInfo> {
        const [firstname, lastname, role] = await
            Promise.all([
                this.db.ref(`users/${userId}/firstname`).once('value').then(ds => ds.val()),
                this.db.ref(`users/${userId}/lastname`).once('value').then(ds => ds.val()),
                this.db.ref(`users/${userId}/role`).once('value').then(ds => ds.val())
            ]);
        return {
            id: userId,
            firstname: firstname,
            lastname: lastname,
            role: role
        };
    }


    private mapToUser(userData: any): IUser {
        const user: any = {};
        for (const key in userData) {
            if (Object.prototype.hasOwnProperty.call(userData, key)) {
                const value = userData[key];
                const userKey = key.toLowerCase();
                user[userKey] = value;
                if (userKey === 'phone') {
                    user[userKey] = formatPhoneNumber(value.replace('+8', '8')).replace('+', '');
                }
                // if (userKey === 'role') {
                //     user[userKey] = rolesNameToCodeMapping[value];
                // }
            }
        }

        if (user.firstname) {
            user.firstname = user.firstname.trim();
        }

        if (user.lastname) {
            user.lastname = user.lastname.trim();
        }

        return user as IUser;
    }

}