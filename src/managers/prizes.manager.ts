import { database as db } from 'firebase-admin';
import { ISchool, ITeam, IUser } from '../models';

const PROMO_PATHS: any = {
    BY_PARTNERS: 'v3/promocodes/partners',
    BY_USER: 'v3/promocodes/by_user',
    BY_TEAM: 'v3/promocodes/by_team',
};

const CERT_PATHS: any = {
    BY_USER: 'v3/certs/by_user',
    BY_TEAM: 'v3/certs/by_team',
    CERT_DATA: 'v3/certs/data'
}

export class PrizesManager {

    /**
     *
     */
    constructor(private db: db.Database) {

    }

    public async getUserPromocodes(userId: string | undefined, currentMarathon: string) {
        if (!userId) {
            return null;
        }

        const promocodesBotSettings = (await this.db.ref('bot/settings/promocodes/user').once('value')).val();
        const promocodePartners = Object.keys(promocodesBotSettings);
        const userPromocodesRef = this.db.ref(`${PROMO_PATHS.BY_USER}/${userId}`);

        const userPromocodes = (await userPromocodesRef.once('value')).val() || {};

        // если у юзера есть промокоды не по всем партнерам
        if (promocodePartners.some(p => !userPromocodes[p])) {

            // идем по всем партнерам
            for (let i = 0; i < promocodePartners.length; i++) {
                const partner = promocodePartners[i];
                // если по этому партнеру нет промокода, то выпускаем его
                if (!userPromocodes[partner] && promocodesBotSettings[partner] == currentMarathon) {
                    userPromocodes[partner] = await this.getUserPromocodeFromPartner(partner);
                }
            }
            await userPromocodesRef.update(userPromocodes);
        }

        return userPromocodes;
    }

    private async getUserPromocodeFromPartner(partner: string) {
        const partnerPromocodesRef = this.db.ref(`${PROMO_PATHS.BY_PARTNERS}/${partner}`);
        const firstFreeCodeSnap = await partnerPromocodesRef.orderByValue().equalTo(false).limitToFirst(1).once('value');

        if (firstFreeCodeSnap.exists()) {
            const promocode = Object.keys(firstFreeCodeSnap.val())[0];
            await partnerPromocodesRef.child(promocode).set(true);
            return promocode;
        }
        return '-';
    }

    public async getTeamPromocodes(teamId: string) {
        const teamPromocodesSnap = await this.db.ref(`promocodes/by_team/${teamId}`).once('value');
        if (teamPromocodesSnap.exists()) {
            return teamPromocodesSnap.val();
        }
        else {
            const promocodeSnap = await this.db.ref('promocodes/firstsept')
                .orderByValue().equalTo(false).limitToFirst(1).once('value');

            if (promocodeSnap.exists()) {
                const promocodeVal = promocodeSnap.val();
                const key = Object.keys(promocodeVal)[0];
                if (key) {
                    const update: any = {};
                    update[`promocodes/by_team/${teamId}/firstsept`] = key;
                    update[`promocodes/firstsept/${key}`] = true;

                    await this.db.ref().update(update);

                    return {
                        firstsept: promocodeSnap.key
                    };
                }
            }
        }
        return null;
    }

    public async getUserMarathonCertificate(user: IUser, marathonNumber: string) {

        const certData = {
            firstname: user.firstname.trim(),
            lastname: user.lastname.trim(),
            org: user.school.trim()
        };

        return await this.getOrCreateCertificate(
            `${CERT_PATHS.BY_USER}/${user.id}/${marathonNumber}`,
            certData,
            marathonNumber,
            `${user.firstname} ${user.lastname} - Сертификат за прохождение марафона №${marathonNumber.replace('M', '')} RSA`);

        // const certId = (await this.db.ref(`${CERT_PATHS.BY_USER}/${user.id}/${marathonNumber}`).once('value')).val();

        // if (!certId) {


        //     const cert = {
        //         template_id: `${marathonNumber}.html`,
        //         name: `${user.firstname} ${user.lastname} - Сертификат за прохождение марафона №${marathonNumber.replace('M', '')} RSA`,
        //         data: certData
        //     };

        //     const key = this.db.ref(CERT_PATHS.CERT_DATA).push().key;
        //     const update: any = {};

        //     update[`${CERT_PATHS.CERT_DATA}/${key}`] = cert;
        //     update[`${CERT_PATHS.BY_USER}/${user.id}/${marathonNumber}`] = key;

        //     await this.db.ref().update(update);

        //     return key;
        // }

        // return certId;
    }

    public async getSchoolMarathonCertificate(school: ISchool, certCode: string) {
        const certData = {
            org: school.school.trim()
        };

        return await this.getOrCreateCertificate(
            `${CERT_PATHS.BY_TEAM}/${school.team_code}/${certCode}`,
            certData,
            certCode,
            `${school.school.trim()} - Сертификат за прохождение RSA`);
    }

    private async getOrCreateCertificate(certPath: string, certData: any, certCode: string, certName: string): Promise<string> {
        const certId = (await this.db.ref(certPath).once('value')).val();

        if (!certId) {

            const cert = {
                template_id: `${certCode}.html`,
                name: certName,
                data: certData
            };

            const key = <string>this.db.ref(CERT_PATHS.CERT_DATA).push().key;
            const update: any = {};

            update[`${CERT_PATHS.CERT_DATA}/${key}`] = cert;
            update[certPath] = key;

            await this.db.ref().update(update);

            return key;
        }

        return certId;
    }
}