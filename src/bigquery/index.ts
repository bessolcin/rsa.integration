import { database, initializeApp, credential } from 'firebase-admin';
import { Storage } from '@google-cloud/storage';
import { BigQuery, JobLoadMetadata } from '@google-cloud/bigquery';
import * as fs from 'fs';

import { RSAEnvConfig } from '../envconfig';
import { ORGANIZATIONS_KEY, TEAMS_KEY, USERS_KEY, USER_TO_TEAM_KEY } from '../constants';
import { TeamsRepository } from '../repositories';

const bigquery = new BigQuery({ credentials: require('../../rsa-analytics-334007-5d93a05f241e.json'), projectId: 'rsa-analytics-334007' });
const storage = new Storage({ credentials: require('../../rsa-analytics-334007-5d93a05f241e.json') });

const CS_BUCKET_NAME = 'rsa_analytics_data';

const CS_RSA_ORGS_DATA_FILE_NAME = 'rsa.jsonl';
const CS_RSA_TASKS_RESULTS_DATA_FILE_NAME = 'rsa_performed_tasks.jsonl';
const CS_RSA_TASKS_DATA_FILE_NAME = 'rsa_tasks.jsonl';
const CS_DT_COMMUNITIES_FILE_NAME = 'dt_communities.jsonl';
const CS_DT_RESPONSES_FILE_NAME = 'dt_all_responses.jsonl';


const BQ_DATASET_ID = "rsa";

const BQ_ORGANIZATIONS_TABLE = "organizations";
const BQ_TASKS_RESULTS_TABLE = "task_results";
const BQ_TASKS_TABLE = "tasks";
const BQ_DT_COMMUNITIES_TABLE = "dt_communities";
const BQ_DT_RESPONSES_TABLE = "dt_all_responses";

initializeApp({
	credential: credential.cert(RSAEnvConfig.DB_SERVICE_ACCOUNT),
	databaseURL: RSAEnvConfig.DB_DATABASE_URL
});
const db = database();
const teams = new TeamsRepository(db);

Promise.all([
	getOrganizationsDataset()
		.then((dataset: any[]) => {
			console.log('loading organizations dataset...');
			return loadDataSetJson(CS_RSA_ORGS_DATA_FILE_NAME, BQ_DATASET_ID, BQ_ORGANIZATIONS_TABLE, dataset).then(r => console.log(r));
		}),

	getPerformedTasksDataset()
		.then((dataset: any[]) => {
			console.log('loading performed tasks dataset...');
			return loadDataSetJson(CS_RSA_TASKS_RESULTS_DATA_FILE_NAME, BQ_DATASET_ID, BQ_TASKS_RESULTS_TABLE, dataset)
				.then(r => console.log(r));
		}),

	getTasksDataset()
		.then((dataset: any[]) => {
			console.log('loading tasks dataset...');
			return loadDataSetJson(CS_RSA_TASKS_DATA_FILE_NAME, BQ_DATASET_ID, BQ_TASKS_TABLE, dataset)
				.then(r => console.log(r));
		}),

	getDiagnosticsToolSchoolsDataset()
		.then((dataset: any[]) => {
			console.log('loading DT communities dataset...');
			return loadDataSetJson(CS_DT_COMMUNITIES_FILE_NAME, BQ_DATASET_ID, BQ_DT_COMMUNITIES_TABLE, dataset)
				.then(r => console.log(r));
		})])
	.then(() => console.log('Loading data set finished'));

// getDiagnosticsToolResponsesDataset()
// 	// Promise.resolve([])
// 	.then((dataset: any[]) => {
// 		// console.log({ d: dataset });
// 		console.log('loading DT responses dataset...');
// 		return loadDataSetJson(CS_DT_RESPONSES_FILE_NAME, BQ_DATASET_ID, BQ_DT_RESPONSES_TABLE, dataset)
// 			.then(r => console.log(r));
// 	})
// 	.catch(e => {
// 		console.error(e);
// 	});

async function loadDataSetJson(fileName: string, dataset_id: string, table_id: string, dataSet: any[]) {
	const filePath = `./${fileName}`;
	const contents = dataSet.map((o: any) => JSON.stringify(o)).join('\n');

	const writeFile = new Promise((resolve, reject) => {
		fs.writeFile(filePath, contents, () => {
			resolve(true);
		});
	});

	await writeFile;

	await uploadFileToGoogleCloudStorage(CS_BUCKET_NAME, filePath, fileName).catch(console.error);
	await uploadDataFromGoogleCloudToBigQuery(dataset_id, table_id, CS_BUCKET_NAME, fileName);

	return dataSet;
}

async function getOrganizationsDataset() {
	const dataSet: any = {};

	const allTeams = (await db.ref(TEAMS_KEY).once('value')).val();
	let teamsCount = 0;
	let usersCount = 0;
	let allTeamsKeys = Object.keys(allTeams);

	while (allTeamsKeys.length) {
		const teamKeys = allTeamsKeys.splice(0, 100);

		await Promise.all(teamKeys.map(key => {
			const team = allTeams[key];
			if (team.referral_code == 'PQYMDL') {
				debugger;
			}
			team.id = key;
			return db.ref(`${USER_TO_TEAM_KEY}`).orderByValue().equalTo(key).once('value')
				.then(d => d.val())
				.then(usersToTeam => {
					return usersToTeam ? Promise.all(Object.keys(usersToTeam)
						.map((uid: string) => db.ref(`${USERS_KEY}/${uid}`).once('value').then(d => getFlatObject(d.val())))) :
						Promise.resolve(null);
				}).then(users => {
					if (team.referral_code == 'PQYMDL') {
						debugger;
					}
					team.users = users;
					if (!dataSet[team.org_id]) {

						return teams.getOrganization(team.org_id).then(org => {
							// org = getFlatObject(org);
							org.id = team.org_id;
							dataSet[team.org_id] = dataSet[team.org_id] || org;
							dataSet[team.org_id].teams = dataSet[team.org_id].teams || [];
							dataSet[team.org_id].teams.push(team);

							// if (team.referral_code == 'PQYMDL' || org.school.indexOf("РЫБ") > -1) {
							// 	debugger;
							// }

							teamsCount++;
							usersCount += users ? users.length : 0;
						}).then(() => team);
					}
					dataSet[team.org_id].teams.push(team);

					teamsCount++;
					usersCount += users ? users.length : 0;

					return Promise.resolve(team);
				});//.then(() => console.log(`teams ${teamsCount} users ${usersCount}`))
		}));
	}

	// for (const key in allTeams) {
	// 	if (Object.prototype.hasOwnProperty.call(allTeams, key)) {
	// 		const team = allTeams[key];

	// 		const usersToTeam = (await db.ref(`${USER_TO_TEAM_KEY}`).orderByValue().equalTo(key).once('value')).val();
	// 		team.users = usersToTeam ?
	// 			await Promise.all(Object.keys(usersToTeam).map((uid: string) => db.ref(`${USERS_KEY}/${uid}`).once('value').then(d => getFlatObject(d.val())))) :
	// 			null;

	// 		if (!dataSet[team.org_id]) {
	// 			const org = getFlatObject(await teams.getOrganization(team.org_id));
	// 			dataSet[team.org_id] = org;
	// 			dataSet[team.org_id].teams = dataSet[team.org_id].teams || [];
	// 		}

	// 		dataSet[team.org_id].teams.push(team);
	// 		teamsCount++;
	// 		usersCount += team.users ? team.users.length : 0;

	// 		console.log(`teams ${teamsCount} users ${usersCount}`);
	// 	}
	// }

	return Object.values(dataSet);
}

async function getPerformedTasksDataset() {
	const allResults = (await db.ref(`forms/results`).once('value')).val();
	const resultsDataset: any[] = [];
	for (const id in allResults) {
		if (Object.prototype.hasOwnProperty.call(allResults, id)) {
			const result = allResults[id];
			result.id = id;
			result.timestamp = toTimestamp(result.timestamp);
			if (result.comments) {
				result.comments = Object.values(result.comments);
				// console.log(Object.values(result.comments).length);
			}

			resultsDataset.push(result);
		}
	}

	return resultsDataset;
}


async function getTasksDataset() {
	const allTasks = (await db.ref('bot/content/tasks').once('value')).val();
	return Object.values(allTasks).map((t: any) => { return Object.assign(t, t.Points); });
}

async function getDiagnosticsToolSchoolsDataset() {
	const allComms = (await db.ref(`diagnostics_tool/communities`).once('value')).val();

	const communitiesDataset: any[] = [];

	let comIds = Object.keys(allComms);

	while (comIds.length) {
		const idsBunch = comIds.splice(0, 100);

		await Promise.all(idsBunch.map(id => db.ref(`diagnostics_tool/community_stats/${id}`).once('value')
			.then(d => d.val())
			.then(comStats => {
				const com = allComms[id];
				com.id = id;
				com._timestamp = toTimestamp(com._timestamp);
				com.surveys = null;

				comStats = comStats ? Object.values(comStats)[0] : null;

				if (comStats) {
					Object.keys(comStats.directions).forEach(d => com[d] = comStats.directions[d].index || null);
					Object.keys(comStats.tasks).forEach(t => com[t] = comStats.tasks[t].index || null);
					com.responses_count = comStats.count;
					com.progress = Math.round(comStats.count * 100 / com.approx_community_size) / 100;
				}

				communitiesDataset.push(com);
			})
		)
		);
	}

	// for (const id in allComms) {
	// 	if (Object.prototype.hasOwnProperty.call(allComms, id)) {
	// 		const com = allComms[id];
	// 		// const teamDS = (await db.ref(`v3/teams`).orderByChild('referral_code').equalTo(id).once('value'));
	// 		// if (teamDS.exists()) {
	// 		// 	update[`diagnostics_tool/communities/${id}/_is_rsa`] = true;
	// 		// }
	// 		com.id = id;
	// 		com._timestamp = toTimestamp(com._timestamp);
	// 		com.surveys = null; //com.surveys ? Object.values(com.surveys) : null;

	// 		communitiesDataset.push(com);
	// 	}
	// }

	return communitiesDataset;
}

const DMAX = {
	D1: 70,
	D2: 116,
	D3: 112,
	D4: 63,
	D5: 83,
	D6: 68,
	D7: 75,
	D8: 146,
	D9: 100,
	D1T1: 34,
	D1T2: 14,
	D1T3: 22,
	D2T1: 25,
	D2T2: 21,
	D2T3: 70,
	D3T1: 41,
	D3T2: 39,
	D3T3: 32,
	D4T1: 27,
	D4T2: 16,
	D4T3: 20,
	D5T1: 30,
	D5T2: 25,
	D5T3: 28,
	D6T1: 34,
	D6T2: 22,
	D6T3: 12,
	D7T1: 23,
	D7T2: 35,
	D7T3: 17,
	D8T1: 71,
	D8T2: 36,
	D8T3: 39,
	D9T1: 23,
	D9T2: 48,
	D9T3: 29
};

async function getDiagnosticsToolResponsesDataset() {
	const allResponses = (await db.ref(`diagnostics_tool/all_responses`).once('value')).val();

	const dataSet = [];

	for (const id in allResponses) {
		if (Object.prototype.hasOwnProperty.call(allResponses, id)) {
			const response = allResponses[id];

			const data: any = {
				id: id
			};
			const v = getEmptyVector();

			for (const key in response) {
				if (Object.prototype.hasOwnProperty.call(response, key)) {
					const value = response[key];

					if (!key.startsWith('m_question') && !key.startsWith('question')) {
						if (key == 'r_func-Comment') {
							data['r_func_Comment'] = value;
						}
						else {
							data[key] = value;
						}
					}

					if (key.startsWith('question') && typeof (response[key]) == 'object') {
						// todo calc points
						const answers: string[] = response[key];
						answers.forEach(a => {
							if (a == 'none') {
								return;
							}
							const D = a.substr(0, 2);
							const T = a.substr(0, 4);
							const L = a.substr(5, 1);
							const points = parseInt(L);
							v[D] += points;
							v[T] += points;
						});
					}
				}
			}

			const result = divideVectors(v, DMAX);
			Object.assign(data, result);
			data._timestamp = toTimestamp(data._timestamp);
			if (!data._request_data) {
				data._request_data = { host: null, ip: null, ips: ['0.0.0.0'] };
			}
			dataSet.push(data);
		}
	}

	return dataSet;
}

function getEmptyVector(): any {
	return {
		D1: 0,
		D2: 0,
		D3: 0,
		D4: 0,
		D5: 0,
		D6: 0,
		D7: 0,
		D8: 0,
		D9: 0,
		D1T1: 0,
		D1T2: 0,
		D1T3: 0,
		D2T1: 0,
		D2T2: 0,
		D2T3: 0,
		D3T1: 0,
		D3T2: 0,
		D3T3: 0,
		D4T1: 0,
		D4T2: 0,
		D4T3: 0,
		D5T1: 0,
		D5T2: 0,
		D5T3: 0,
		D6T1: 0,
		D6T2: 0,
		D6T3: 0,
		D7T1: 0,
		D7T2: 0,
		D7T3: 0,
		D8T1: 0,
		D8T2: 0,
		D8T3: 0,
		D9T1: 0,
		D9T2: 0,
		D9T3: 0
	};
}

function divideVectors(v: any, divider: any) {
	const vresult = getEmptyVector();

	for (const key in divider) {
		if (Object.prototype.hasOwnProperty.call(divider, key)) {
			const dividerCoord = divider[key];
			const divident = v[key];
			vresult[key] = Math.round(divident * 1000 / dividerCoord) / 1000;
		}
	}

	return vresult;
}

async function uploadFileToGoogleCloudStorage(bucketName: string, filePath: string, destFileName: string) {
	await storage.bucket(bucketName).upload(filePath, {
		destination: destFileName
	});

	console.log(`${filePath} uploaded to ${bucketName}`);
}

function getFlatObject(user: any) {
	if (!user) {
		return null;
	}
	const cleanUser = Object.keys(user)
		.filter(k => typeof (user[k]) != 'object')
		.reduce((o: any, k: string) => {
			o[k] = user[k];
			if (k == 'timestamp' || k == 'created_date') {
				o[k] = toTimestamp(user[k]);
			}
			return o;
		}, {});
	return cleanUser;
}

/**
 * This sample loads the json file at
 * https://storage.googleapis.com/cloud-samples-data/bigquery/us-states/us-states.json
 *
 */

export async function uploadDataFromGoogleCloudToBigQuery(dataset_id: string, table_id: string, cs_bucket: string, cs_filename: string) {
	// Imports a GCS file into a table with manually defined schema.
	// Configure the load job. For full list of options, see:
	// https://cloud.google.com/bigquery/docs/reference/rest/v2/Job#JobConfigurationLoad
	const metadata: JobLoadMetadata = {
		sourceFormat: 'NEWLINE_DELIMITED_JSON',
		// schema: {
		// 	fields: [
		// 		{
		// 			name: '_request_data', type: 'RECORD', fields: [
		// 				{ name: 'ip', type: 'STRING' },
		// 				{ name: 'ips', type: 'STRING', mode: 'REPEATED' },
		// 				{ name: 'host', type: 'STRING' },
		// 			]
		// 		}
		// 	],
		// },
		autodetect: true,
		location: 'EU',
		writeDisposition: 'WRITE_TRUNCATE'
	};

	// Load data from a Google Cloud Storage file into the table
	const [job] = await bigquery
		.dataset(dataset_id)
		.table(table_id)
		.load(storage.bucket(cs_bucket).file(cs_filename), metadata);
	// load() waits for the job to finish
	console.log(`Job ${job.id} completed.`);

	// Check the job's status for errors
	const errors = job.status?.errors;
	if (errors && errors.length > 0) {
		throw errors;
	}
}

function toTimestamp(ts: number) {
	return (new Date(ts)).toJSON();
}