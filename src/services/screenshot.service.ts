import * as puppeteer from 'puppeteer';
import { Cluster } from 'puppeteer-cluster';
import * as HB from 'handlebars';


const PUPPETEER_MIN_ARGS = [
	'--autoplay-policy=user-gesture-required',
	// '--disable-background-networking',
	'--disable-background-timer-throttling',
	'--disable-backgrounding-occluded-windows',
	'--disable-breakpad',
	'--disable-client-side-phishing-detection',
	'--disable-component-update',
	'--disable-default-apps',
	'--disable-dev-shm-usage',
	'--disable-domain-reliability',
	'--disable-extensions',
	'--disable-features=AudioServiceOutOfProcess',
	'--disable-hang-monitor',
	'--disable-ipc-flooding-protection',
	'--disable-notifications',
	'--disable-offer-store-unmasked-wallet-cards',
	'--disable-popup-blocking',
	'--disable-print-preview',
	'--disable-prompt-on-repost',
	'--disable-renderer-backgrounding',
	'--disable-setuid-sandbox',
	'--disable-speech-api',
	'--disable-sync',
	'--hide-scrollbars',
	'--ignore-gpu-blacklist',
	'--metrics-recording-only',
	'--mute-audio',
	'--no-default-browser-check',
	'--no-first-run',
	'--no-pings',
	'--no-sandbox',
	'--no-zygote',
	'--password-store=basic',
	// '--use-gl=swiftshader',
	'--use-mock-keychain',
];

export class ScreenshotService {

	private cluster: Cluster | null = null;

	private buffers: any[] | null = null;

	constructor(private puppeteerArgs: any) {
	}

	public async launchCluster() {
		this.cluster = await Cluster.launch({
			concurrency: Cluster.CONCURRENCY_CONTEXT,
			maxConcurrency: 2,
			puppeteerOptions: {
				...this.puppeteerArgs,
				headless: true,
				args: PUPPETEER_MIN_ARGS,
				userDataDir: './lib'
			}
		});
	}

	public async screenshot(options: IScreenshotSettings) {
		this.buffers = [];

		if (!this.cluster) {
			await this.launchCluster();
			await this.configureScreenshotTask();
		}

		if (!options.html) {
			throw Error('You must provide an html property.')
		}

		const shouldBatch = Array.isArray(options.content);
		const contents = shouldBatch ? options.content : [{ ...options.content }];

		contents.forEach((content: any) => {
			this.cluster?.queue({
				...options,
				_ts: (new Date().getTime()),
				content
			});
		});

		await this.cluster?.idle();

		return shouldBatch ? this.buffers : this.buffers[0];
	}

	private async configureScreenshotTask() {
		await this.cluster?.task(async (taskData: { page: puppeteer.Page, data: IScreenshotSettings }) => {
			const buffer = await this.makeScreenshot(taskData.page, taskData.data);
			this.buffers?.push(buffer);
		});
	}

	public async pdf(options: IScreenshotSettings) {
		this.buffers = [];

		if (!this.cluster) {
			await this.launchCluster();
		}
		await this.configurePdfTask();

		if (!options.html) {
			throw Error('You must provide an html property.')
		}

		const shouldBatch = Array.isArray(options.content);
		const contents = shouldBatch ? options.content : [{ ...options.content }];

		contents.forEach((content: any) => {
			this.cluster?.queue({
				...options,
				_ts: (new Date().getTime()),
				content
			});
		});

		await this.cluster?.idle();

		return shouldBatch ? this.buffers : this.buffers[0];
	}
	
	private async configurePdfTask() {
		await this.cluster?.task(async (taskData: { page: puppeteer.Page, data: IScreenshotSettings }) => {
			const buffer = await this.makePdf(taskData.page, taskData.data);
			this.buffers?.push(buffer);
		});
	}

	public async destroy() {
		if (this.cluster) {
			await this.cluster.close();
		}
	}

	private async makeScreenshot(page: puppeteer.Page, options: IScreenshotSettings) {
		if (options.content) {
			const template = HB.compile(options.html);
			options.html = template(options.content);
		}

		options.waitUntil = options.waitUntil || 'domcontentloaded'; // 'networkidle2';
		options.selector = options.selector || 'body';
		options.type = options.type || 'png';
		options.transparent = options.transparent === true;
		options.quality = options.quality ? options.quality : 80;

		await page.setViewport({ width: 640, height: 454});
		await page.setContent(options.html, { waitUntil: <any>options.waitUntil });
		// const element = await page.$(options.selector);

		// if (!element) {
		// 	throw Error('No element matches selector: ' + options.selector)
		// }

		// if (options.beforeScreenshot && typeof options.beforeScreenshot === "function") {
		// 	await options.beforeScreenshot(page);
		// }

		// await page.waitForSelector('.take_screenshot_now');

		const buffer = await page.screenshot({
			path: options.outputPath,
			type: options.type,
			omitBackground: options.transparent,
			encoding: options.encoding,
			quality: options.type == 'png' ? undefined : options.quality,
		});

		return buffer;
	}

	private async makePdf(page: puppeteer.Page, options: IScreenshotSettings) {
		if (options.content) {
			const template = HB.compile(options.html);
			options.html = template(options.content);
		}

		options.waitUntil = options.waitUntil || 'load'; // 'networkidle2';
		options.selector = options.selector || 'body';
		options.type = options.type || 'png';
		options.transparent = options.transparent === true;
		options.quality = options.quality ? options.quality : 80;

		await page.setContent(options.html, { waitUntil: <any>options.waitUntil });

		return await page.pdf({ format: 'a4' });
	}
}

export interface IScreenshotSettings {
	outputPath?: string;
	type?: string | any;
	quality?: number;
	encoding?: string | any;
	content?: any;
	html: string;
	selector?: string;
	beforeScreenshot?: any;
	transparent?: boolean;
	waitUntil?: string;
	_ts?: number;
}