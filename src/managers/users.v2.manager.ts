import { database as db } from 'firebase-admin';

import { callBotmotherWebhook, formatPhoneNumber } from '@bessolcin/coolstory.bots.lib';

import { RSAEnvConfig } from '../envconfig';
import { IUser, IUserShortInfo, UserStatus } from '../models/users';
import { TeamsRepository } from '../repositories';
import { IUsersManager } from '.';

import { BMID_TO_PHONE_KEY, ORGANIZATIONS_KEY, PHONE_TO_BMID_KEY, SCHOOLS_KEY, TEAMS_KEY, USERS_KEY, USER_TO_TEAM_KEY } from '../constants';
import { StatsManager } from './stats.manager';

export class UsersV2Manager implements IUsersManager {

    private users: db.Reference;

    /**
     *
     */
    constructor(
        private db: db.Database,
        private teamsRepository: TeamsRepository,
        private statsManager: StatsManager) {
        this.users = this.db.ref(USERS_KEY);
    }

    public async getUserByBmId(bmId: string): Promise<IUser | null> {
        if (!bmId) {
            return null;
        }

        return this.db.ref(`${BMID_TO_PHONE_KEY}/${bmId}`).once('value').then(phoneSnap => this.getUserByPhone(phoneSnap.val()));
    }

    public async getAllUserBmIds(bmId: string): Promise<string[]> {
        const phone = (await this.db.ref(`${BMID_TO_PHONE_KEY}/${bmId}`).once('value')).val();
        const bmIds = (await this.db.ref(`${PHONE_TO_BMID_KEY}/${phone}`).once('value')).val();

        return Object.keys(bmIds) || [];
    }

    public async createUser(userData: any): Promise<void> {
        const createdDate = new Date();
        const user: IUser = {
            ...this.mapToUser(userData),
            status: UserStatus.Leader,
            created_date: createdDate.getTime(),
            created_date_offset: createdDate.getTimezoneOffset(),
        };
        user.id = user.phone;

        const usersWithPhoneSnapshot = await this.getUserSnapshotByPhone(user.phone);
        if (usersWithPhoneSnapshot.hasChildren()) {
            const duplicateRef = await this.db.ref(`duplicates/${USERS_KEY}`).push(user);
            console.error(`User with phone number already exists ${user.id}. Saved duplicate with id = ${duplicateRef?.key}`, JSON.stringify(userData));
            throw new Error('user_already_exists');
        }

        const [newTeamId, newTeamCode] = await this.teamsRepository.getTeamIdAndCode();
        user.team_id = newTeamId;
        user.team_code = newTeamCode;

        let organization = null;
        let orgId = null;

        let targetTeam: any = null;
        if (userData.team) {
            targetTeam = await this.teamsRepository.getTeamByCode(userData.team);
            if (!targetTeam) {
                console.error('target_team_not_found, user data:', JSON.stringify(userData));
                throw Error('target_team_not_found');
            }
            orgId = targetTeam.org_id;
        }
        else {
            organization = this.getOrganizationInfo(userData);
            orgId = <string>this.db.ref(ORGANIZATIONS_KEY).push().key;
            if (!organization) {
                console.error('target_organization_not_found, user data:', JSON.stringify(userData));
                throw Error('target_organization_not_found');
            }
        }

        const update: any = {};
        update[`${TEAMS_KEY}/${newTeamId}`] = {
            leader: user.id,
            referral_code: newTeamCode,
            org_id: orgId
        };

        update[`${USERS_KEY}/${user.id}`] = user;
        update[`${USER_TO_TEAM_KEY}/${user.id}`] = newTeamId;

        if (organization) {
            update[`${ORGANIZATIONS_KEY}/${orgId}`] = organization;

            update[`${SCHOOLS_KEY}/${newTeamCode}`] = {
                ...organization,
                team_id: newTeamId,
                team_code: newTeamCode,
                points: 0,
                members: 1,
                tasks_count: 0
            };
        }

        const bmIds: string[] = [];
        const phoneToBmIdSnap = await this.db.ref(`${PHONE_TO_BMID_KEY}/${user.id}`).once('value');
        // Кажется это лишнее 29.08.2021
        // Если уже есть пользователь в чатботе
        if (phoneToBmIdSnap.exists() && phoneToBmIdSnap.hasChildren()) {
            const phoneToBmIds = phoneToBmIdSnap.val();

            Object.keys(phoneToBmIds).forEach((bmId: string) => {
                bmIds.push(bmId || '');
                // То заодно добавим индекс идБотмамы к юзерИД
                update[`${BMID_TO_PHONE_KEY}/${bmId}`] = user.id;
            });
        }

        await this.db.ref().update(update);

        if (targetTeam) {
            const joinResult = await this.joinUsersTeamByPhone(user.phone, userData.team);
        }

        if (bmIds.length) {
            console.info('Found user to bmid relation, calling webhook...');
            await callBotmotherWebhook(RSAEnvConfig.BOTMOTHER_USER_CREATED_CALLBACK, 'BOTMOTHER_USER_CREATED_CALLBACK', bmIds, user);
        }
    }

    private getOrganizationInfo(userData: any) {
        const data = {
            city: userData.city || null,
            country: userData.country || null,
            location: userData.location || null,
            region: userData.region || null,
            school: userData.school || null,
            settlement: userData.settlement || null
        };

        if (Object.values(data).every(v => !v)) {
            return null;
        }

        return data;
    }

    public async bindUserWithBmId(phone: string, bmId: string): Promise<IUser | null> {
        console.info(`User with bmid=${bmId} trying to get user with phone XXXXX${phone.substr(5)}.`);
        const update: any = {};

        const [currentBmIdToPhone, currentUser] = await Promise.all([
            this.db.ref(`${BMID_TO_PHONE_KEY}/${bmId}`).once('value'),
            this.getUserByPhone(phone)
        ]);

        if (currentBmIdToPhone.exists()) {
            const oldPhone = currentBmIdToPhone.val();
            update[`${PHONE_TO_BMID_KEY}/${oldPhone}/${bmId}`] = null;
        }

        update[`${PHONE_TO_BMID_KEY}/${phone}/${bmId}`] = true;
        update[`${BMID_TO_PHONE_KEY}/${bmId}`] = phone;
        await this.db.ref().update(update);

        console.info(`User with bmid=${bmId} got user with phone XXXXX${phone.substr(5)}.`);

        return currentUser;
    }

    public async notifyUserLogin(bmId: string, platform: string) {
        if (bmId) {
            const bmids = (await this.getAllUserBmIds(bmId)).filter(bmid => bmid != bmId);
            await callBotmotherWebhook(
                RSAEnvConfig.BOTMOTHER_REDIRECT_TO_SECTION_CALLBACK,
                'BOTMOTHER_REDIRECT_TO_SECTION_CALLBACK/login_notification',
                bmids,
                { redirect: 'login_notification' });
            console.warn(`User with bmid=${bmId} logged in in ${platform}`);
        }
        else {
            console.warn('BmId is empty in onUsersNotify');
        }
    }

    public async deleteUser(phone: string) {
        const update: any = {};
        update[`${PHONE_TO_BMID_KEY}/${phone}`] = null;

        const phoneToBmIdSnap = await this.db.ref(`${PHONE_TO_BMID_KEY}/${phone}`).once('value');

        phoneToBmIdSnap.forEach((s: db.DataSnapshot) => {
            update[`${BMID_TO_PHONE_KEY}/${s.key}`] = null;
        });

        const userSnapshot = await this.getUserSnapshotByPhone(phone);
        update[`${USERS_KEY}/${userSnapshot.key}`] = null;
        update[`${USER_TO_TEAM_KEY}/${userSnapshot.key}`] = null;

        await this.db.ref().update(update);
    }

    public async joinUsersTeam(currentUserBmId: string, teamCode: string) {
        let currentUserPhone: string = (await this.db.ref(`${BMID_TO_PHONE_KEY}/${currentUserBmId}`).once('value')).val();
        return await this.joinUsersTeamByPhone(currentUserPhone, teamCode);
    }

    public async joinUsersTeamByPhone(currentUserPhone: string, teamCode: string): Promise<string> {
        if (!teamCode) {
            console.warn(`TeamCode is incorrect!`);
            return TeamJoinResult.userNotExist;
        }

        console.info('Getting user membership for joining, id=', currentUserPhone);
        const currentUser = await this.getUserByPhone(currentUserPhone); // this.getUserTeamMembership(currentUserPhone);
        if (!currentUser) {
            console.warn(`Current user membership object not found!`);
            return TeamJoinResult.userNotExist;
        }

        const targetTeam = await this.teamsRepository.getTeamByCode(teamCode);
        if (targetTeam == null) {
            console.info(`Team with code=${teamCode} not found.`);
            return TeamJoinResult.teamNotFound;
        }

        // если текущий юзер капитан команды в которой есть кто-то кроме него
        // if (currentUserMembership &&
        //     currentUserMembership.userId == currentUserMembership.usersTeamLeaderId &&
        //     currentUserMembership.teamMembersCount > 1) {
        //     console.info(`User with bmid=${currentUserBmId} already leads team with members.`);
        //     return 'team_cannot_join_youre_leader';
        // }

        const org = (await this.db.ref(`${ORGANIZATIONS_KEY}/${targetTeam.org_id}`).once('value')).val();

        const update: any = {};
        update[`${USER_TO_TEAM_KEY}/${currentUser.id}`] = targetTeam.id;
        update[`${USERS_KEY}/${currentUser.id}/status`] = UserStatus.Common;
        update[`${USERS_KEY}/${currentUser.id}/team_id`] = targetTeam.id;
        update[`${USERS_KEY}/${currentUser.id}/team_code`] = teamCode;
        update[`${USERS_KEY}/${currentUser.id}/school`] = org.school;
        update[`${USERS_KEY}/${currentUser.id}/city`] = org.city || null;
        update[`${USERS_KEY}/${currentUser.id}/settlement`] = org.settlement || null;
        update[`${USERS_KEY}/${currentUser.id}/region`] = org.region;
        update[`${USERS_KEY}/${currentUser.id}/country`] = org.country;

        // if (!currentUser.school && !currentUser.location) {
        //     for (const key in targetTeam.organization) {
        //         if (Object.prototype.hasOwnProperty.call(targetTeam.organization, key)) {
        //             const val = targetTeam.organization[key];
        //             update[`${USERS_KEY}/${currentUser.id}/${key}`] = val;
        //         }
        //     }
        // }

        await this.db.ref().update(update);

        await this.sendNewTeamMemberNotification(currentUser, targetTeam);

        console.info(`User with id=${currentUserPhone} joined team of ${teamCode}.`);
        await this.statsManager.updateTeamStats(currentUser.id, currentUser.role);

        return TeamJoinResult.success;
    }

    private async sendNewTeamMemberNotification(currentUser: IUser, targetTeam: any) {
        const roleLabel = RSAEnvConfig.ROLE_LABELS[currentUser.role]; // (await this.db.ref(`role_labels/${currentUserMembership.user.role}`).once('value')).val();
        const teamBmIds = await this.teamsRepository.getTeamBmIds(targetTeam.id, currentUser.id);
        if (teamBmIds.length) {
            await callBotmotherWebhook(
                RSAEnvConfig.BOTMOTHER_NEW_TEAM_MEMBER_CALLBACK,
                'BOTMOTHER_NEW_TEAM_MEMBER_CALLBACK',
                teamBmIds,
                {
                    redirect: 'меню',
                    message_section: 'team_new_member',
                    new_member: `${currentUser.firstname} ${currentUser.lastname || ''} – ${roleLabel || ''}.`
                }
            );
        }
    }

    private getUserSnapshotByPhone(phone: string): Promise<db.DataSnapshot> {
        return this.users.child(phone).once('value');
    }

    public async getUserByPhone(phone: string): Promise<IUser | null> {
        if (!phone) {
            return null;
        }
        const userSnapshot = await this.users.child(phone).once('value');
        const user: IUser = userSnapshot.val();

        // if (user) {
        //     user.team_id = '-MUTiIVFTyFZO0vRkXBn'; // (await this.db.ref(`${USER_TO_TEAM_KEY}/${phone}`).once('value')).val();
        //     user.team_code = 'asd';// (await this.db.ref(`${TEAMS_KEY}/${user.teamId}/referral_code`).once('value')).val();
        // }

        return user;
    }

    public async getUserShortInfo(userId: string): Promise<IUserShortInfo> {
        const [firstname, lastname, role] = await
            Promise.all([
                this.db.ref(`${USERS_KEY}/${userId}/firstname`).once('value').then(ds => ds.val()),
                this.db.ref(`${USERS_KEY}/${userId}/lastname`).once('value').then(ds => ds.val()),
                this.db.ref(`${USERS_KEY}/${userId}/role`).once('value').then(ds => ds.val())
            ]);
        return {
            id: userId,
            firstname: firstname,
            lastname: lastname,
            role: role
        };
    }


    private mapToUser(userData: any): IUser {
        const user: any = {};
        for (const key in userData) {
            if (Object.prototype.hasOwnProperty.call(userData, key)) {
                const value = userData[key];
                const userKey = key.toLowerCase();
                user[userKey] = value;
                if (userKey === 'phone') {
                    user[userKey] = formatPhoneNumber(value.replace('+8', '8')).replace('+', '');
                }
                // if (userKey === 'role') {
                //     user[userKey] = rolesNameToCodeMapping[value];
                // }
            }
        }

        if (user.firstname) {
            user.firstname = user.firstname.trim();
        }

        if (user.lastname) {
            user.lastname = user.lastname.trim();
        }

        return user as IUser;
    }

}

export enum TeamJoinResult {
    success = 'team_join_success',
    userNotExist = 'team_user_notexist',
    teamNotFound = 'team_with_code_not_found'
}