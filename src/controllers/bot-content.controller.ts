// import { Express, Request, Response } from 'express';
// import { IUsersManager, PrizesManager, StatsManager } from './../managers';
// import { database } from 'firebase-admin';
// import { enrichMenuWithCompleteTasks, enrichMenuWithCompleteTaskResults, getCompleteTrackTasksLabel, getTaskDirectionsStatsView, SendAlertToTelegram } from '../helpers';
// import { EnvConfig } from '../envconfig';
// import { ICompleteTask, IMessage, IStats, ITeamStats, IUser, IUserShortInfo } from '../models';
// import { BotContentManager } from '@bessolcin/coolstory.bots.lib';


// export class BotContentController {

//     private cache: { timestamp: number, object: any } | null = null;

//     private cacheLifetime: number = 10;

//     constructor(app: Express,
//         db: database.Database,
//         private botContentManager: BotContentManager,
//         private usersManager: IUsersManager,
//         private statsManager: StatsManager,
//         private prizesManager: PrizesManager) {
//         this.configureRequestHandlers(app);
//     }

//     private configureRequestHandlers(app: Express) {
//         app.get('/bot/content/onboarding', this.onBotContentOnboarding.bind(this));

//         app.get('/bot/menu/search', this.onBotMenuSearch.bind(this));

//         app.get('/bot/menu/:sheetId', this.onBotMenuSheetId.bind(this));
//         app.get('/bot/menu/:sheetId/update', this.onBotMenuSheetIdUpdate.bind(this));
//         app.get('/bot/menu/:sheetId/:code', this.onBotMenuAsyncCode.bind(this));

//         app.get('/bot/tasks/update', this.onUpdateTasks.bind(this));
//     }

    

//     private validateCache() {
//         if (!this.cache) {
//             return;
//         }
//         const now = (new Date()).getTime();
//         const diffInMinutes = Math.floor((now - this.cache.timestamp) / (1000 * 60));
//         if (diffInMinutes > this.cacheLifetime) {
//             this.cache = null;
//         }
//     }

//     public async onBotMenuSearch(req: Request, res: Response) {
//         try {
//             let query = req.query['input'] || '';
//             if (Array.isArray(query)) {
//                 query = query[0];
//             }
//             if (!query) {
//                 return res.json({ result: null });
//             }

//             const searchResult = await this.botContentManager.searchByKeyword(query);

//             return res.json({ result: searchResult });
//         } catch (error) {
//             console.log('Error while searching keywords', { url: req.url, error: error });

//             await SendAlertToTelegram(EnvConfig.ALERT_BOT_TOKEN, EnvConfig.ALERT_CHAT_ID, `
//             Error while searching keywords.
//             url: ${req.originalUrl}`);

//             return res.json({ result: null });
//         }
//     }

//     public async onBotMenuSheetId(req: Request, res: Response) {
//         try {
//             const firstname = req.query.firstname as string || null;
//             const bmId = req.query.bmid as string;

//             let [menu] = await Promise.all([
//                 this.botContentManager.getBotMenu(req.params.sheetId),
//             ]);

//             // const user = await this.usersManager.getUserByBmId(bmId)
//             // let stats = await this.statsManager.getTeamStats(user?.teamId);

//             // menu = await this.enrichMenuWithTeamStats(menu, firstname, user, stats);

//             return res.json(menu);
//         } catch (error) {
//             console.error('Error while getting bot menu', { url: req.url, error: error });

//             await SendAlertToTelegram(EnvConfig.ALERT_BOT_TOKEN, EnvConfig.ALERT_CHAT_ID, `
//             Error while getting bot menu.
//             url: ${req.originalUrl}`);
//         }
//         return res.json({
//             "меню": {
//                 messages: [{
//                     "type": "text",
//                     "text": "Это мое главное меню и, кажется, я потерял память...",
//                     "buttons": [],
//                     "buttons_count": 0
//                 }]
//             }
//         });
//     }

//     public async onBotMenuSheetIdUpdate(req: Request, res: Response) {
//         try {
//             const sheetId = req.params.sheetId;
//             if (sheetId) {
//                 console.info(`Updating bot menu cache for doc ${sheetId}`);
//                 let menu = await this.botContentManager.updateBotMenuCache(req.params.sheetId);
//                 console.info(`Updated bot menu cache for doc ${sheetId}`);
//             }
//             else {
//                 console.warn(`Updating bot menu: SHEETID is empty!`);
//             }

//         } catch (error) {
//             console.error('Error while updating bot menu cache', { url: req.url, error: error });

//             await SendAlertToTelegram(EnvConfig.ALERT_BOT_TOKEN, EnvConfig.ALERT_CHAT_ID, `
//             Error while getting bot menu.
//             url: ${req.originalUrl}`);
//         }

//         res.json({ result: 'ok' });
//     }

//     public async onBotMenuAsyncCode(req: Request, res: Response) {
//         const bmId = req.query.bmid as string;
//         const messageCode = req.params.code;

//         if (messageCode) {
//             const user = await this.usersManager.getUserByBmId(bmId);
//             if (!user) {
//                 return res.json({ result: null });
//             }
//             const stats = await this.statsManager.getTeamAndPersonalStats(user.teamId, user.id);
//             if (!stats || !stats.team_stats || !stats.personal_stats) {
//                 await this.statsManager.updateTeamStats(user.id, user.role);
//                 return res.json({ result: null });
//             }

//             const menu = await this.botContentManager.getBotMenu(req.params.sheetId);
//             let messageTemplate = menu[messageCode];

//             if (!messageTemplate) {
//                 return res.json({ result: '' });
//             }

//             // Отображение статистики команды
//             if (messageCode == 'team_stats_tpl') {
//                 const trackLabel = getCompleteTrackTasksLabel(user.role);

//                 messageTemplate.messages[0].text = messageTemplate.messages[0].text
//                     .replace('{{team.members.length}}', stats.team_stats.members.length)
//                     .replace('{{team.complete_tasks.length}}', stats.team_stats.complete_tasks_codes ? stats.team_stats.complete_tasks_codes.length : 0)
//                     .replace('{{personal.complete_tasks_count}}', stats.personal_stats.complete_tasks_count || 0)
//                     .replace('{{personal.current_role_complete_tasks_count}}', trackLabel ? `${trackLabel} ${stats.personal_stats.current_role_complete_tasks_count}` : '')
//                     .replace('{{personal.points.total}}', stats.personal_stats.points.total)
//                     .replace('{{team.points.total}}', stats.team_stats.points.total)
//                     .replace('{{team.directions}}', getTaskDirectionsStatsView(stats.team_stats));

//                 return res.json({ result: messageTemplate });
//             }

//             // Отображение участников команды
//             if (messageCode == 'team_members_tpl') {
//                 const teamUsers = await Promise.all(stats.team_stats.members.map(uid => this.usersManager.getUserShortInfo(uid)));
//                 const teamMembers = teamUsers.map(u => `${u.firstname.trim()} ${u.lastname.trim()} - ${EnvConfig.ROLE_LABELS[u.role]}`).sort();

//                 const chunks: any[] = [];
//                 const chunkSize = 60;
//                 let step = 1;
//                 while (chunkSize * step <= teamMembers.length) {
//                     chunks.push(teamMembers.slice(chunkSize * (step - 1), chunkSize * step).join('\n'));
//                     step++;
//                 }
//                 chunks.push(teamMembers.slice(chunkSize * (step - 1), chunkSize * step).join('\n'));

//                 const newMsgTemplate = Object.assign({}, messageTemplate);
//                 const msg = messageTemplate.messages[0];

//                 newMsgTemplate.messages = chunks.map((s: string, i: number) => {
//                     const message = Object.assign({}, msg);
//                     message.text = (i == 0 ? message.text.replace('{{team.members}}', s) : s);
//                     return message;
//                 });

//                 return res.json({ result: newMsgTemplate });
//             }

//             if (messageCode == 'team_feed') {

//                 const teamUsers = await Promise.all(stats.team_stats.members.map(uid => this.usersManager.getUserShortInfo(uid)));

//                 const teamFeed = await this.statsManager.getTeamFeed(stats.team_stats.members);
//                 const tasks = await this.statsManager.getTasks();

//                 let taskRowsCount = 0;
//                 const messageRowLimitSplitter = '||';

//                 const userResultsBlocks = teamUsers.map((user: IUserShortInfo) => {
//                     let userTaskResults = teamFeed[user.id];
//                     if (userTaskResults) {
//                         userTaskResults = userTaskResults.filter(tr => tasks[tr.code].CreateTicket == 'TRUE');

//                         if (userTaskResults.length) {
//                             taskRowsCount += userTaskResults.length;
//                             const resultsRows = userTaskResults.map((taskResult: ICompleteTask) =>
//                                 taskResult.date ? `📆 ${taskResult.date}\n✅ ${tasks[taskResult.code].Condition}` : `✅ ${tasks[taskResult.code].Condition}`
//                             );
//                             const resultRow = `${EnvConfig.ROLE_LABELS[user.role]} ${user.firstname} ${user.lastname}\n${resultsRows.join('\n')}${taskRowsCount > 50 ? messageRowLimitSplitter : ''}`
//                             if (taskRowsCount > 50) {
//                                 taskRowsCount = 0;
//                             }
//                             return resultRow;
//                         }
//                     }
//                     return null;
//                 });

//                 const teamFeedStr = userResultsBlocks.filter(s => !!s).join('\n\n').split(messageRowLimitSplitter);
//                 const msg = messageTemplate.messages[0];

//                 const newMsgTemplate = Object.assign({}, messageTemplate);
//                 newMsgTemplate.messages = teamFeedStr.map((s: string, i: number) => {
//                     const message = Object.assign({}, msg);
//                     message.text = (i == 0 ? message.text.replace('{{team.feed}}', s) : s);
//                     return message;
//                 });

//                 return res.json({ result: newMsgTemplate });
//             }

//             // Отображение призов если больше 300 кристаллов
//             if (messageCode == 'challenge_prize') {
//                 if (stats && stats.team_stats.points.total >= 1500) {
//                     return res.json({ result: menu['final_prizes'] });
//                 }
//                 else if (stats && stats.team_stats.points.total >= 900) {
//                     return res.json({ result: menu['c3_prizes'] });
//                 }
//                 else if (stats && stats.team_stats.points.total >= 300) {
//                     return res.json({ result: menu['c1_prizes'] });
//                 }
//                 else {
//                     return res.json({ result: menu['challenge_prize'] });
//                 }
//             }

//             // Отображение промокодов если они есть, а если нет то дефолтное сообщение
//             if (messageCode == 'c1_prizes_promocodes') {
//                 let [promocodes, teamPromocodes] = await Promise.all([
//                     this.prizesManager.getUserPromocodes(user.id),
//                     this.prizesManager.getTeamPromocodes(user.teamId)
//                 ]);

//                 promocodes = promocodes || { v2_mybook: '' };
//                 teamPromocodes = teamPromocodes || {};

//                 Object.assign(promocodes, teamPromocodes);

//                 if (promocodes.v2_mybook) {
//                     const menuPromocodesSectionKey = 'c1_prizes_promocodes_list';
//                     for (const promoType in promocodes) {
//                         if (Object.prototype.hasOwnProperty.call(promocodes, promoType)) {
//                             menu[menuPromocodesSectionKey].messages[0].text = menu[menuPromocodesSectionKey].messages[0].text
//                                 .replace(`{{promocodes.${promoType}}}`, promocodes[promoType]);
//                         }
//                     }
//                     return res.json({ result: menu[menuPromocodesSectionKey] });
//                 }

//                 return res.json({ result: menu['c1_prizes_promocodes'] })
//             }

//             if (messageCode == 'special_challenges') {
//                 const roles: string[] = ['teacher', 'student', 'exstudent', 'parent'];

//                 if (stats.team_stats.points.total >= 900 &&
//                     roles.indexOf(user.role) != -1 &&
//                     stats.personal_stats?.current_role_complete_tasks_count >= 5) {

//                     return res.json({ result: menu[`special_challenges_${user.role}`] });
//                 }

//                 return res.json({ result: menu[messageCode] });
//             }

//             if (messageCode.startsWith('task_week_C')) {
//                 enrichMenuWithCompleteTasks(menu, stats.team_stats)
//                 return res.json({ result: menu[messageCode] });
//             }
//         }

//         return res.json({ result: '' });
//     }

//     private async onUpdateTasks(req: Request, res: Response) {
//         const tasks = await this.statsManager.updateTasks();

//         return res.json(tasks);
//     }


// }
