import { IUser, IUserShortInfo } from "../models";

export interface IUsersManager {
    getUserByBmId(bmId: string): Promise<IUser | null>;
    getAllUserBmIds(bmId: string): Promise<string[]>;
    createUser(userData: any): Promise<void>;
    bindUserWithBmId(phone: string, bmId: string): Promise<IUser | null>;
    notifyUserLogin(bmId: string, platform: string): Promise<void>;
    deleteUser(phone: string): Promise<void>;
    joinUsersTeam(currentUserBmId: string, teamCode: string): Promise<string>;
    joinUsersTeamByPhone(currentUserPhone: string, teamCode: string): Promise<string>;
    getUserByPhone(phone: string): Promise<IUser | null>;
    getUserShortInfo(userId: string): Promise<IUserShortInfo>;
}