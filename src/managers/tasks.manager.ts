import { callBotmotherWebhook } from '@bessolcin/coolstory.bots.lib';
import { database as db } from 'firebase-admin';
import got from 'got';

import { StatsManager } from '.';
import { ORGANIZATIONS_KEY, SCHOOLS_KEY, TEAMS_KEY } from '../constants';
import { RSAEnvConfig } from '../envconfig';
import { createTaskTicket, getTicketMessage, isSocialLink } from '../helpers';
import { IFormData, IPartner, ITask, ITeam, IUser, TaskResultState } from '../models';
import { TeamsRepository } from '../repositories';

const APPROVED_STATUS = 'задание зачтено';
const DECLINED_STATUS = 'сняли кристаллы';

export class TasksManager {
	/**
	 *
	 */
	constructor(private db: db.Database, private statsManager: StatsManager, private teams: TeamsRepository) {

	}

	public async handleTaskResult(formData: IFormData, id: string, user?: IUser) {
		console.info(`Checking if need create ticket for task result`);

		const tasks = await this.statsManager.getTasks();
		const currentTask = tasks[formData.form_name];

		if (formData.form_name == 'M1T2L1') {
			await this.handleM1N2L1Task(formData, id);
		}

		if (formData.form_name == 'M4T4L1') {
			await this.handlePratnershipFormData(formData, id);
		}

		if (currentTask['CreateTicket'] === 'TRUE') {
			await this.createTicketForResult(formData, id, currentTask);
		}
		else {
			await this.db.ref(`${RSAEnvConfig.PARTICIPANTS_TASKS_RESULTS_PATH}/${id}/state`).set(TaskResultState.Approved);
			await this.statsManager.updateTeamStats(formData.user_id, formData.role);
		}
	}

	/** Задание с заполнением соцсетей */
	private async handleM1N2L1Task(formData: IFormData, id: string) {
		const update: any = {};

		const links = {
			vk: isSocialLink(formData.form_value.link_vk) ? formData.form_value.link_vk : null,
			instagram: isSocialLink(formData.form_value.link_instagram) ? formData.form_value.link_instagram : null,
			fb: isSocialLink(formData.form_value.link_fb) ? formData.form_value.link_fb : null,
			tiktok: isSocialLink(formData.form_value.link_tiktok) ? formData.form_value.link_tiktok : null
		};
		const superpower = formData.form_value.superpower;

		update[`${SCHOOLS_KEY}/${formData.team_code}/links`] = links;
		update[`${SCHOOLS_KEY}/${formData.team_code}/superpower`] = superpower;

		await this.db.ref().update(update);
	}

	//-MnVRVaGSxnkVxKAdPXI
	public async handlePratnershipFormData(formData: IFormData, recordKey: string): Promise<void> {
		const [targetPartner, currentTeam] = <ITeam[]>await Promise.all([
			this.teams.getTeamByCode(formData.form_value.team_code),
			this.teams.getTeamByCode(formData.team_code)
		]);

		const [targetOrg, currentOrg]: any = await Promise.all([
			this.teams.getOrganization(targetPartner.org_id),
			this.teams.getOrganization(currentTeam.org_id)
		]);

		const targetPartnerPartners: IPartner[] = targetPartner?.partners || [];
		if (targetPartnerPartners.some(p => p.code == currentTeam?.referral_code)) {
			console.warn(`Target team ${targetPartner.referral_code} already has partner ${currentTeam.referral_code}`);
			return;
		}

		targetPartnerPartners.push({
			code: currentTeam?.referral_code,
			name: `${currentOrg.school}, ${currentOrg.city || currentOrg.settlement}, ${currentOrg.region}`
		});

		const currentTeamPartners: IPartner[] = currentTeam.partners || [];
		currentTeamPartners.push({
			code: targetPartner.referral_code,
			name: `${targetOrg.school}, ${targetOrg.city || targetOrg.settlement}, ${targetOrg.region}`
		});

		await Promise.all([
			this.teams.updateTeam(targetPartner.id, { partners: targetPartnerPartners }),
			this.teams.updateTeam(currentTeam.id, { partners: currentTeamPartners })
		]);
	}

	/**
	 * Задание с инструментом диагностики
	 */
	public async handleDiagnosticsToolForm(formData: IFormData, id: string, user?: IUser) {
		const team = <ITeam>await this.teams.getTeamByCode(formData.team_code);
		const organization = (await this.db.ref(`${ORGANIZATIONS_KEY}/${team.org_id}`).once('value')).val();

		let response;
		try {
			response = await this.registerCommunityInDiagnosticsTool(formData, organization, user);
		} catch (error) {
			console.warn(`Error while registering team in DT. U:${formData.user_id} RecId: ${id}.  Retring...`, error);
			response = await this.registerCommunityInDiagnosticsTool(formData, organization, user);
			console.log(`Registered team in DT after retry. U:${formData.user_id} RecId: ${id}.`);
		}

		if ((<any>response.body).result != 'error') {
			await this.db.ref(`${TEAMS_KEY}/${team.id}/diagnostics_tool`).set(true);
			console.log(`Created diagnostics tool account for team ${(<any>response.body).result.id}`);
		}
		else {
			throw new Error(`Error while creating survey: ${JSON.stringify(response.body)}`);
		}
	}

	private async registerCommunityInDiagnosticsTool(formData: IFormData, organization: any, user?: IUser) {
		return await got.post(`https://europe-west1-coolstoryprointegration.cloudfunctions.net/rfDiagnosticsToolCommunity?schoolId=${formData.team_code}`,
			{
				json: {
					country: organization.country,
					region: organization.region,
					city: organization.city || organization.settlement,
					// settlement: organization.settlement || null,
					school: organization.school,
					email: user?.email || '',
					name: `${user?.firstname} ${user?.lastname}`,

					students_count: parseInt(formData.form_value.students_count.replace(/\D/g, '')),
					teachers_count: parseInt(formData.form_value.teachers_count.replace(/\D/g, '')),
					graduate_estimate: parseInt(formData.form_value.graduate_estimate.replace(/\D/g, ''))
				},
				responseType: 'json'
			}
		);
	}

	public async changeStateTaskResult(request: { update: ITaskResultUpdate }) {
		if (request.update && request.update.ticket_id) {
			const userResults = (await this.db.ref(`${RSAEnvConfig.PARTICIPANTS_TASKS_RESULTS_PATH}`)
				.orderByChild('ticket_id').equalTo(parseInt(request.update.ticket_id))
				.once('value')).val();

			if (userResults) {
				const resultId = Object.keys(userResults)[0];
				const result: IFormData = userResults[resultId];
				const update: any = {};
				update[`${RSAEnvConfig.PARTICIPANTS_TASKS_RESULTS_PATH}/${resultId}/state`] = request.update.result === APPROVED_STATUS ? TaskResultState.Approved : TaskResultState.Declined;
				await this.db.ref().update(update);
				await this.statsManager.updateTeamStats(result.user_id, result.role);

				if (request.update.result === APPROVED_STATUS) {
					callBotmotherWebhook(RSAEnvConfig.BOTMOTHER_RESULT_ACCEPTED_CALLBACK, 'BOTMOTHER_RESULT_ACCEPTED_CALLBACK', [result.bm_id], { section: `task_${result.form_name}_accepted` });
				}
			}
			else {
				throw new Error(`User result not found: ${JSON.stringify(request)}`);
			}
		}
		else {
			throw new Error(`Invalid task update: ${JSON.stringify(request)}`);
		}
	}

	public async commentTaskResult(request: { update: ITaskResultUpdate }) {
		if (request.update && request.update.ticket_id) {
			const userResults = (await this.db.ref(`${RSAEnvConfig.PARTICIPANTS_TASKS_RESULTS_PATH}`)
				.orderByChild('ticket_id').equalTo(parseInt(request.update.ticket_id))
				.once('value')).val();

			if (userResults) {
				const resultId = Object.keys(userResults)[0];
				const result: IFormData = userResults[resultId];

				const comment = request.update.last_message
					.replace(/<\/{0,1}(br|p)>/g, '\n')
					.replace(/<\/{0,1}[a-z \\"0-9=\/.:-]+>/g, '')
					.replace(/&nbsp;/g, ' ');
				await this.db.ref(`${RSAEnvConfig.PARTICIPANTS_TASKS_RESULTS_PATH}/${resultId}/comments`).push(comment);

				callBotmotherWebhook(RSAEnvConfig.BOTMOTHER_RESULT_COMMENT_CALLBACK, 'BOTMOTHER_RESULT_COMMENT_CALLBACK', [result.bm_id], { comment: comment });
			}
			else {
				throw new Error(`User result not found: ${JSON.stringify(request)}`);
			}
		}
		else {
			throw new Error(`Invalid task update: ${JSON.stringify(request)}`);
		}
	}

	private async createTicketForResult(formData: IFormData, id: string, currentTask: ITask): Promise<void> {
		console.info(`Creating tikcket for task ${formData.form_name}`);
		const response = await createTaskTicket(await this.getCreateTicketRequest(formData, id, currentTask));
		console.info(`Created ticket for task ${formData.form_name}`);

		if ((<any>response.body).status == 'success') {
			const update: any = {};
			update[`${RSAEnvConfig.PARTICIPANTS_TASKS_RESULTS_PATH}/${id}/ticket_id`] = (<any>response.body).ticket_id;
			update[`${RSAEnvConfig.PARTICIPANTS_TASKS_RESULTS_PATH}/${id}/state`] = TaskResultState.Review;
			await this.db.ref().update(update)
		}
		else {
			console.log('Error while creating ticket:', response);
			throw new Error(`Error while creating ticket: ${JSON.stringify(formData)}`);
		}
	}

	private async getCreateTicketRequest(formData: IFormData, recordKey: string, task: any) {
		const clientName = `${formData.firstname} ${formData.lastname}`;

		const message = getTicketMessage(formData, recordKey, task.Condition);

		return {
			api_token: RSAEnvConfig.USEDESK_API_TOKEN,
			client_id: 'new_client', //formData.,
			subject: `Результат ${formData.form_name} - ${clientName}`,
			from: 'client',
			message: message,
			client_name: clientName,
			client_phone: formData.user_id,
			additional_id: recordKey,
			field_id: '',
			field_value: '',
			tag: `task,${formData.form_name},marathon${formData.form_name[1]},number${formData.form_name[3]},level${formData.form_name[5]}`,
			type: 'task',
			lists: []
		};
	}

	public async handleResultCancellation(userId: string, userRole: string, resultId: string) {
		console.info(`Updating team stats on cancel... userid=${userId}`);

		await this.statsManager.updateTeamStats(userId, userRole);
		console.info(`Updated team stats for userid=${userId}.`);
	}
}

export interface ITaskResultUpdate {
	title: string;
	ticket_id: string;
	additional_id: string;
	client_id: string;
	client_phone: string;
	status: string;
	assignee_id: string;
	tag: string;
	result: string;
	last_message: string;
}