import * as HB from 'handlebars';
import { Request } from 'express';

import { getDiffInMinutes, IBotMenuHandler, ILogger, IMenu, IMessage } from '@bessolcin/coolstory.bots.lib';

import { RSAEnvConfig } from '../envconfig';
import { getCompleteTrackTasksLabel, getTaskDirectionsStatsView } from '../helpers';
import { IUsersManager, PrizesManager, StatsManager } from '../managers';
import { IUser, ITeamStats, IStats, ITeam } from '../models';
import { database } from 'firebase-admin';
import { SCHOOLS_KEY, TEAMS_KEY } from '../constants';

export class BotMenuHandler implements IBotMenuHandler {

    private menuTemplate: HandlebarsTemplateDelegate<any> = <HandlebarsTemplateDelegate<any>><unknown>null;
    private menuTemplateTS: number | null = null;

    /**
     *
     */
    constructor(
        private db: database.Database,
        private logger: ILogger,
        private usersManager: IUsersManager,
        private statsManager: StatsManager,
        private prizesManager: PrizesManager) {

    }

    public async handle(menu: IMenu, bmid: string, request: Request): Promise<IMenu> {
        const user = await this.usersManager.getUserByBmId(bmid);
        let [stats, team] = await Promise.all(
            [
                this.statsManager.getTeamAndPersonalStats(user?.team_id, user?.id),
                (user ? this.db.ref(`${TEAMS_KEY}/${user.team_id}`).once('value').then(ds => ds.val()) : Promise.resolve({ diagnostics_tool: false }))
            ]);
        const username = <string>(user?.firstname || request.query.firstname);
        menu = await this.enrichMenuWithTeamStats(menu, user, stats, username, team);

        return menu;
    }

    private async enrichMenuWithTeamStats(
        menu: any,
        user: IUser | null,
        stats: IStats | null,
        username: string,
        team: ITeam): Promise<any> {

        const now = (new Date()).getTime();
        if (!this.menuTemplate || this.menuTemplateTS && Math.abs(getDiffInMinutes(this.menuTemplateTS, now)) > RSAEnvConfig.MENU_TEMPLATE_CACHE_LIFETIME) {
            this.menuTemplate = HB.compile(JSON.stringify(menu));
            this.menuTemplateTS = (new Date()).getTime();
        }

        if (user) {
            try {
                user.school = user.school?.replace(/"/g, '\\"');
                user.city = user.city || user.settlement;
                user.role = RSAEnvConfig.ROLE_LABELS[user.role];
                const statsDirections = ((stats?.team_stats) ? getTaskDirectionsStatsView(stats.team_stats) : '').replace(/\n/g, '\\n');
                const role_tasks_label = getCompleteTrackTasksLabel(user.role);

                // Костыль чтобы показывать меньше кристаллов
                // let totalTeamPoints = stats?.team_stats?.points?.total || 0;
                // if (stats?.team_stats?.complete_tasks) {
                //     Object.values(stats.team_stats.complete_tasks)
                //         .filter(t => t.code.startsWith('M8'))
                //         .forEach(t => totalTeamPoints -= t.points.total);
                // }
                // let totalPersPoints = stats?.personal_stats?.points?.total || 0;
                // if (stats?.personal_stats?.complete_tasks) {
                //     Object.values(stats.personal_stats.complete_tasks)
                //         .filter(t => t.code.startsWith('M8'))
                //         .forEach(t => totalPersPoints -= t.points.total);
                // }
                let finalist = false;
                let certId = null;
                if ((stats?.team_stats?.points?.total || 0) >= 1500) {
                    finalist = true;
                    const school = (await this.db.ref(`${SCHOOLS_KEY}/${team.referral_code}`).once('value')).val();
                    certId = await this.prizesManager.getSchoolMarathonCertificate(school, 'finalist');
                }
                const rendered = this.menuTemplate({
                    team: {
                        ...team,
                        directions: statsDirections,
                        finalist: finalist,
                        cert_id: certId
                    },
                    user: user,
                    this_user: {
                        firstname: username
                    },
                    stats: stats,
                    role_tasks_label: role_tasks_label,
                    // actual_points: totalTeamPoints,
                    // actual_pers_points: totalPersPoints,
                });
                menu = JSON.parse(
                    rendered
                );
            } catch (error) {
                const errorText = `Error while enriching menu ${JSON.stringify(user)}`;
                console.error(errorText, error);
                this.logger.alert(errorText);

                this.replaceUserFirstname(menu, username);

                const inviteSection = menu['➕🙋‍♂️ Пригласить'];
                if (inviteSection?.messages) {
                    for (let i = 0; i < inviteSection.messages.length; i++) {
                        inviteSection.messages[i].text = inviteSection.messages[i].text
                            .replace('{{team.referral_code}}', user.team_code);
                    }

                }
                const aboutGame = menu['about_game1'];
                if (aboutGame?.messages) {
                    for (let i = 0; i < aboutGame.messages.length; i++) {
                        aboutGame.messages[i].text = aboutGame.messages[i].text
                            .replace('{{team.referral_code}}', user.team_code);
                    }

                }

                const teamSection = menu['👨‍👩‍👧‍👦 Команда'];
                if (teamSection?.messages) {
                    for (let i = 0; i < teamSection.messages.length; i++) {
                        teamSection.messages[i].text = teamSection.messages[i].text
                            .replace('{{user.school}}', user.school)
                            .replace('{{user.city}}', user.city || user.settlement)
                            .replace('{{user.team_code}}', user.team_code);
                    }

                }
            }

            // if (stats && stats.team_stats) {
            // enrichMenuWithCompleteTaskResults(menu, stats.team_stats);
            // }
        }
        else {
            try {
                menu = JSON.parse(
                    this.menuTemplate({
                        team: { referral_code: '' },
                        this_user: {
                            firstname: username.replace(/"/g, '')
                        }
                    })
                );
            } catch (error) {
                const errorText = `Error while enriching menu ${JSON.stringify(user)}`;
                console.error(errorText);
                this.logger.alert(errorText);

                this.replaceUserFirstname(menu, username);
            }
        }



        return menu;
    }

    private replaceUserFirstname(menu: any, firstname: string) {
        for (const key in menu) {
            if (Object.prototype.hasOwnProperty.call(menu, key)) {
                const section = menu[key];
                for (let i = 0; i < section.messages.length; i++) {
                    const message = section.messages[i] as IMessage;
                    message.text = message.text.replace('{{this_user.firstname}}', firstname);
                }
            }
        }
    }

}