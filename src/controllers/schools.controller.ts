import { getDiffInMinutes } from '@bessolcin/coolstory.bots.lib';
import { Express, Request, Response } from 'express';
import { database as db } from 'firebase-admin';

import { SCHOOLS_KEY, TEAM_STATS_KEY, USER_TO_TEAM_KEY } from '../constants';
import { RSAEnvConfig } from '../envconfig';
import { StatsManager } from '../managers';
import { HtmlToImageService } from '../services';
import { IBadge, ISchool, ISchoolInfo, ITasksDictionary, ITeamStats } from '../models';
import { getBadgesForSchool } from '../helpers/badges';

export class SchoolsController {

	private schoolsRef: db.Reference;

	private schoolsCache: ISchool[] | null = null;
	private schoolsCacheTimestamp!: number;

	/**
	 *
	 */
	constructor(
		app: Express,
		private db: db.Database,
		private statsManager: StatsManager,
		private pdfGenService: HtmlToImageService) {
		this.configureRequestHandlers(app);
		this.schoolsRef = db.ref(SCHOOLS_KEY);
	}

	private configureRequestHandlers(app: Express) {
		app.get('/api/schools', this.onSchools.bind(this));
		app.get('/api/schools/update', this.onSchoolsUpdate.bind(this));

		app.get('/api/certificate/:certId', this.onApiCertificate.bind(this));

		app.get('/api/schools/survey/:teamId', this.onApiSurvey.bind(this));
		app.get('/api/schools/case/:teamId', this.onApiCase.bind(this));
		app.get('/api/schools/vote/:teamId', this.onApiVote.bind(this));
	}

	/**
	 * 
	 * @param req 
	 * @param res 
	 * @returns 
	 */
	private async onSchools(req: Request, res: Response) {
		try {
			const badges: IBadge[] = (await this.db.ref('bot/content/badges').once('value')).val();
			const badgesDict = badges.reduce((o: any, b) => { o[b.code] = b; return o; }, {});

			const teamCode = <string>(req.query.code || null);
			if (teamCode) {
				const school = (await this.schoolsRef.child(teamCode).once('value')).val();
				if (school) {
					const vm = this.mapSchoolToViewModel(school, badgesDict);

					if (teamCode == 'PQYMDL') {
						vm.bages = (await this.db.ref('bot/content/badges').once('value')).val();
					}
					return res.json(vm);
				}
				return res.json(null);
			}

			if (!this.schoolsCache || Math.abs(getDiffInMinutes(this.schoolsCacheTimestamp, (new Date()).getTime())) > RSAEnvConfig.BOT_MENU_CACHE_LIFETIME) {
				const schoolsTable = (await this.schoolsRef.once('value')).val();
				const schoolsList =
					Object.values(schoolsTable)
						.filter((sdb: any) => (sdb.hidden != true) && (sdb.links?.vk || sdb.links?.fb || sdb.links?.instagram || sdb.links?.tiktok))
						.map(sdb => this.mapSchoolToViewModel(sdb, badgesDict))
						.filter(s => !!s.lat);

				const fondTeam = schoolsList.filter(s => s.team_code == 'PQYMDL')[0];
				fondTeam.bages = (await this.db.ref('bot/content/badges').once('value')).val();

				this.schoolsCache = schoolsList;
				this.schoolsCacheTimestamp = (new Date()).getTime();
			}

			return res.json(this.schoolsCache);
		} catch (error) {
			console.log(error);
			return res.status(400).json({ result: 'error', error: error });
		}
	}

	private async onSchoolsUpdate(req: Request, res: Response) {
		try {
			const update: any = {};
			const schools = Object.values((await this.schoolsRef.once('value')).val()).filter((s: any) => !s.hidden).map(s => this.mapSchoolToViewModel(s, {}));

			const teamsUsers = await Promise.all(schools.map(s => this.db.ref(`${USER_TO_TEAM_KEY}`).orderByValue().equalTo(s.team_id).once('value').then(d => ({ school: s, team_users: d.val() }))));

			const existingTeams: any[] = []; //teamsUsers.filter(tu => !!tu.team_users);
			for (let i = 0; i < teamsUsers.length; i++) {
				const teamUsers = teamsUsers[i];
				if (!teamUsers.team_users) {
					update[`${SCHOOLS_KEY}/${teamUsers.school.team_code}/hidden`] = true;
				}
				else {
					existingTeams.push(teamUsers);
				}
			}

			const teamUsersStats: ISchoolInfo[] = await Promise.all(existingTeams.map(et => this.db.ref(`${TEAM_STATS_KEY}/${et.school.team_id}`).once('value').then(d => { et.stats = d.val(); return et; })));
			const tasks = await this.statsManager.getTasks();

			const bunchSize = 50;
			let bunch = teamUsersStats.slice(0, bunchSize);
			let i = 0;
			while (bunch.length > 0) {
				await Promise.all(bunch.map(schoolInfo => getBadgesForSchool(this.db, schoolInfo, tasks)
					.then((b: string[]) => update[`${SCHOOLS_KEY}/${schoolInfo.school.team_code}/badges`] = b)))
				i++;
				bunch = teamUsersStats.slice(i * bunchSize, (i + 1) * bunchSize);
			}
			// for (let i = 0; i < teamUsersStats.length; i++) {
			// 	const schoolInfo = teamUsersStats[i];

			// 	const badges = await getBadgesForSchool(this.db, schoolInfo, tasks);

			// 	update[`${SCHOOLS_KEY}/${schoolInfo.school.team_code}/badges`] = badges;
			// }

			await this.db.ref().update(update);

			return res.json(teamUsersStats);
		} catch (error) {
			const message = `Error while updating badges: ${(<any>error).message}`;
			console.error(message, error);
			return res.status(400).json({ result: 'error', error: error });
		}
	}

	private mapSchoolToViewModel(schoolDb: any, badgesDict: { [key: string]: IBadge }): ISchool {
		const address = schoolDb.location.school?.address;
		const location = address?.data || schoolDb.location.settlement || null;

		let city = (schoolDb.city || schoolDb.settlement);
		let region = schoolDb.region;
		if (address) {
			if (address.data.city) {
				city = `${address.data.city_type}. ${address.data.city}`;
			}
			if (address.data.settlement) {
				city = `${address.data.settlement_type}. ${address.data.settlement}`;
			}
			if (address.data.region) {
				region = `${address.data.region_type}. ${address.data.region}`;
			}
		}

		return {
			team_id: schoolDb.team_id,
			team_code: schoolDb.team_code,
			lat: (location?.geo_lat ? location.geo_lat + randomInteger(0, 3).toString() : null) as string,
			lon: (location?.geo_lon ? location.geo_lon + randomInteger(0, 3).toString() : null) as string,
			kladr_id: schoolDb.kladr_id,
			school: schoolDb.school?.replace(/ "/g, ' «').replace(/"/g, '»'),
			country: schoolDb.country,
			region: region,
			city: city,
			points: schoolDb.points,
			members_count: schoolDb.members_count,
			tasks_count: schoolDb.tasks_count,
			inn: schoolDb.location?.school?.inn,
			bages: schoolDb.badges?.map((b: string) => (badgesDict[b] || b)) || [],
			badges: schoolDb.badges,
			superpower: schoolDb.superpower || null,
			links: {
				vk: schoolDb.links?.vk || null,
				instagram: schoolDb.links?.instagram || null,
				fb: schoolDb.links?.fb || null,
				tiktok: schoolDb.links?.tiktok || null
			}
		}
	}

	private async onApiCertificate(req: Request, res: Response) {
		try {
			const id = req.params.certId;

			const certData = (await this.db.ref(`v3/certs/data/${id}`).once('value')).val();

			if (certData && certData.template_id) {

				const buffer = await this.pdfGenService.generatePDFFromTemplate(certData.template_id, certData.data);
				res.writeHead(200, {
					'Content-Type': 'application/pdf',
					'Content-Disposition': `attachment; filename="${encodeURIComponent(certData.name)}.pdf"`
				});

				res.end(buffer);
			}
			else {
				res.status(400).send(`Сертификат ${id} не найден`);
			}
		} catch (ex) {
			console.error(`Error while generating certificate ${req.params.certId}`, ex);
			res.status(400).send(`Ошибка, попробуйте открыть страницу позже.`);
		}
	}

	private async onApiSurvey(req: Request, res: Response) {
		const teamId = req.params.teamId;

		const schoolName = (await this.db.ref(`v3/schools/${teamId}/school`).once('value')).val();

		const link = `https://docs.google.com/forms/d/e/1FAIpQLSdN6pHq-JFjkPX4NIQanz2IRJItofHuEJ7kWVrnb3N9Tk30mQ/viewform?usp=pp_url&entry.1859201103=${schoolName}&entry.1895253810=${teamId}`;

		return res.redirect(link);
	}


	private async onApiCase(req: Request, res: Response) {
		const teamId = req.params.teamId;

		const school = (await this.db.ref(`v3/schools/${teamId}`).once('value')).val();

		const link = `https://airtable.com/shrP1qPulD8ncYIUa?prefill_Населенный%20пункт=${school.city || school.settlement}&prefill_Название%20организации=${school.school}&prefill_Код%20команды=${teamId}`;

		return res.redirect(link);
	}

	private async onApiVote(req: Request, res: Response) {
		const teamId = req.params.teamId;

		const school = (await this.db.ref(`v3/schools/${teamId}`).once('value')).val();

		const link = school ? `https://airtable.com/shrLX0EyuQmrIOGc6?hide_stid=true&prefill_stid=${teamId}` : 'https://rybakovschoolaward.ru';

		return res.redirect(link);
	}
}

/**
 * min and max included 
 * @param min 
 * @param max 
 * @returns 
 */
export function randomInteger(min: number, max: number) {
	return Math.floor(Math.random() * (max - min + 1) + min)
}