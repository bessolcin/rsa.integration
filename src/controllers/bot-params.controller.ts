import { Express, Request, Response } from 'express';
import { database as db } from 'firebase-admin';
import { RSAEnvConfig } from '../envconfig';

export class BotParamsController {
    /**
     *
     */
    constructor(app: Express, private db: db.Database) {
        this.configureRequestHandlers(app);
    }

    private configureRequestHandlers(app: Express) {
        app.get('/params/utm/:id', this.onParamsUtmGet.bind(this));
        app.post('/params/utm', this.onParamsUtmPost.bind(this));

        app.get('/params/parse/:paramStr', this.onParamsParseGet.bind(this));

        app.get('/settings/:docId/:bmid', this.onSettings.bind(this));
    }

    private async onParamsUtmPost(req: Request, res: Response) {
        const utm: any = {
            utm_source: req.query.utm_source || null,
            utm_medium: req.query.utm_medium || null,
            utm_campaign: req.query.utm_campaign || null,
            utm_content: req.query.utm_content || null,
            utm_term: req.query.utm_content || null,
        };

        const utmRef = this.db.ref(`utm`);
        const newUtmKey = utmRef.push().key;

        if (newUtmKey) {
            if (Object.values(utm).some(v => v != null)) {
                const flatUtmId = newUtmKey.replace(/[-_]/g, '');
                utm.id = flatUtmId;

                await utmRef.child(newUtmKey).update(utm);
                return res.json({ id: flatUtmId });
            }
        }

        return res.json({ id: null });
    }

    private async onParamsUtmGet(req: Request, res: Response) {
        const utmId = req.params.id || null;
        if (utmId) {
            const utm = (await this.db.ref(`utm`).orderByChild('id').equalTo(utmId).once('value')).val();

            return res.json({ utm: utm ? Object.values(utm)[0] : null });
        }

        return res.json({ utm: { utm_source: 'direct', utm_medium: '', utm_campaign: '' } });
    }

    /**
     * Парсит строку вида t_ABCD1234__utm_MU91Aoh7nPTBUEC3Au в json
     * @param req 
     * @param res 
     */
    private async onParamsParseGet(req: Request, res: Response) {
        const paramStr = req.params.paramStr;
        const params: any = {};

        if (paramStr) {
            paramStr.split('__').forEach(kv => {
                const kvArray = kv.split('_');
                params[kvArray[0]] = kvArray[1];
            });
        }

        return res.json(params);
    }

    private async onSettings(req: Request, res: Response) {
        const password = req.query.p || '';
        if (password != '1623924296900') {
            return res.status(401).send('Unauthorized');
        }

        const utm_compaign_params = req.query.utm_compaign_params || '';
        const platform = req.query.platform || '';
        const bmid = req.params.bmid;
        const appUrl = RSAEnvConfig.CURRENT_APP_URL;
        const docId = req.params.docId;

        const settings = {
            "viber_token": `${RSAEnvConfig.BOT_VIBER_TOKEN}`,

            "viber_send_message": `https://chatapi.viber.com/pa/send_message`,

            "vk_send_message": `https://api.vk.com/method/messages.send?access_token=${RSAEnvConfig.BOT_VK_TOKEN}&v=5.111`,

            "fb_send_message": `https://graph.facebook.com/v7.0/me/messages?access_token=`,

            "send_tg_message": `https://api.telegram.org/bot${RSAEnvConfig.BOT_TELEGRAM_TOKEN}/sendMessage`,

            "send_tg_photo": `https://api.telegram.org/bot${RSAEnvConfig.BOT_TELEGRAM_TOKEN}/sendPhoto`,

            "send_tg_video": `https://api.telegram.org/bot${RSAEnvConfig.BOT_TELEGRAM_TOKEN}/sendVideo`,

            "ga_send_pageview": `https://www.google-analytics.com/collect?v=1&t=pageview&tid=${RSAEnvConfig.GA_ID}&cid=${bmid}&uid=${bmid}&hl=ru&dh=rsa.app.botmother.com&${utm_compaign_params}&dp=%2F${platform}%2F`,

            "ga_send_event": `https://www.google-analytics.com/collect?v=1&t=event&tid=${RSAEnvConfig.GA_ID}&${utm_compaign_params}&cid=${bmid}&uid=${bmid}&hl=ru&`,

            "get_botmenu": `${appUrl}/bot/menu/${docId}?bmid=${bmid}`,

            "get_botsection": `${appUrl}/bot/menu/${docId}`,

            "save_form": `https://europe-west1-coolstoryprointegration.cloudfunctions.net/rsaForms/form`,

            "get_user": `https://europe-west1-coolstoryprointegration.cloudfunctions.net/rsaForms/users/bind`,

            "join_team": `${appUrl}/users/join`,

            "notify_user_login": `${appUrl}/users/notify`,

            "alert_telegram": `https://api.telegram.org/bot1398128667:AAHvpPKOfyYIPJfBZIpd-Pd5ATP7qLXW2tQ/sendMessage`
        }

        return res.json(settings);
    }
}