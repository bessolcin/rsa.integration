export interface IFormData {
    date: string;
    time: string;
    platform: string;
    dialog_url: string;
    bm_id: string;
    platform_id: string;
    user_id: string;
    team_id: string;
    team_code: string;
    role: string;
    firstname: string;
    lastname: string;
    form_name: string;
    form_value: any;
}