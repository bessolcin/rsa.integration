import { database, initializeApp, credential } from 'firebase-admin';
import { SpreadsheetManager } from '@bessolcin/coolstory.bots.lib';

//#endregion

import { StatsManager, UsersV2Manager, IUsersManager } from './managers';
import { TeamsRepository } from './repositories';

import { RSAEnvConfig } from './envconfig';
import { IPersonalStats, ITeamStats } from './models';
import { PHONE_TO_BMID_KEY, TEAMS_KEY, USERS_KEY, USER_TO_TEAM_KEY } from './constants';

initializeApp({
    credential: credential.cert(RSAEnvConfig.DB_SERVICE_ACCOUNT),
    databaseURL: RSAEnvConfig.DB_DATABASE_URL
});

const db = database();

const teamsRepository = new TeamsRepository(db);


const spreadsheetManager = new SpreadsheetManager(RSAEnvConfig.SPREADSHEET_CLIENT_SECRET);
// const usersManager: IUsersManager = new UsersManager(db, teamsRepository);
const statsManager = new StatsManager(db, spreadsheetManager);
const usersManager: IUsersManager = new UsersV2Manager(db, teamsRepository, statsManager);

async function test() {
    const empTeamIds = ["0BIKGB1Q4FFUXDL", "0UTJ8XB4MOVMNYB", "478XLQOOW7EEY5S", "48X0NWVUW14E1YT", "4BAFBW5NEQG3PMK", "5C0MZFEKATYKAX1", "5EV5U3V2XWJHVTP", "5FJFCDARRHIX1C3", "68OQP3MKQX9ZC5N", "6DWYUZDTWVWG5OH", "6PQG4GFPBZVZSZX", "7KU2LLWPPGELQGE", "7NB2S9ZJXYWI6MI", "80NBMMHGUACSZYP", "9ET1OUV94EEQ2WK", "9WMUS68RJXK24MO", "A253HIZMXAJLJGS", "ADPRDLZKPYCIBVX", "AQWE7SGKQIMW5XF", "AYAP2QHWGSKTSJQ", "BEDWKA6LHKPZ8KK", "BEMMX2A7Q7JE5NK", "BZXOU2O9TPMTHYQ", "C8SVNNCCQCPHAU8", "CNUBQEN3YXRO219", "CWOHVNC1RJQ9W6B", "DBZCIWF6O7YAT5U", "DE6XOE0CRPEIOHD", "DTY2H2MKW57NKWV", "E1UK2WKXXVQFVXK", "E89CXW4VIBHD0QB", "EPY7QWVOYENS9B6", "FA97B4TBGIMK75R", "FQGKCSGGT5SK3MG", "FUV6YHTWQ2ANVHX", "G3WTHLI8VJHEPXN", "GHDRFRO1ONVYWPQ", "GYX4MLMBPCXBKMC", "H54QYUVAWEZKAI0", "HC5JKJITIQODBK3", "HGISE1IB3AJWXCN", "HHCYKOYPOKWLNSI", "HJERA5IEGKI60WR", "HNLW6TUVMKXWSYS", "I32IJHYTDMKRDYK", "IPCQMHROF0S8IJA", "JIP8IJ93HAIO3KM", "JJHCAXRCKFAMTJA", "K1A3TRCJLJYLODF", "KITVDI4W3DVAA2U", "KSNUDC3AQQNHEHL", "KXUTQ4ALUZXJ4MN", "L4S2DUQ2QRQX54J", "LFPSNPX5VDNJABI", "NEOPJ7JKV2YBXPP", "NKB3VDSI9Q5HSUJ", "NKJXVTJ8B61XOGM", "NPBNZN2DFGNPXZ6", "NU3UBYYS9TIMVLG", "NXHPWZRV7QMCOC1", "P7TMFCOIL4RJAG7", "PA7J4Y5BQIT89WL", "PZHSAPCHWWEABIG", "QMDXGJS0FKIJJQP", "QX262TYMBQBZZ6U", "RIWXYML4UNPF3BR", "RRCXIADNOW8R9WY", "RYGPPIPESOA49VF", "ULDE64GKVCTQ03X", "UVLOJZOMULYSAQI", "UYHDRMV7SIDLZI7", "V0Z2HFNK3Z7PYNF", "VN2QEHAK4GG0RSM", "VRTRRD9NXJVZ2VD", "W9ZXSRCSIN27HZU", "WG5HXXAVZQSZBFI", "XADTL7SPDP1TDQI", "XASKJQCRMDSGRQ8", "XLJ2DV53ITFANPC", "YCIAX7N9NHTB1ZI", "YUBLGR1XJNOMGTZ", "ZJDHUY7TEM9AMUX"];
    const users = Object.values((await db.ref(USERS_KEY).orderByChild('team').once('value')).val());

    const usersWOCountry = users.filter((u: any) => !u.country);
    // const usersWTeam = usersWOCountry.filter((u: any) => !!u.team);

    const teamIds = await Promise.all(
        usersWOCountry.map((u: any) => db.ref(`${USER_TO_TEAM_KEY}/${u.id}`).once('value')
            .then((ds: database.DataSnapshot) => {
                const d = ds.val();
                return d;
                // let suka = Object.values(d)[0];
                // return { id: Object.keys(d)[0], ...(<any>suka) }
            })
        )
    );
    const uniqTeams = getUniq(teamIds);

    let teams = await Promise.all(uniqTeams.map((id: string) => db.ref(`${TEAMS_KEY}/${id}`).once('value').then(d => d.val())));
    teams = teams.filter(t => !t.organization);

    const teamsStats = await Promise.all(uniqTeams.map((id: string) => statsManager.getTeamStats(id)));
    const teamLeaders = await Promise.all(teams.map((t: any) => usersManager.getUserByPhone(t.leader)));
    const teamLeadersBmIds = await Promise.all(teamLeaders.map((u: any) => db.ref(`${PHONE_TO_BMID_KEY}/${u.id}`).once('value').then(d => d.val())));

    // const update: any = {};
    // teamLeaders.forEach(u => {
    //     update[`users_to_fix_location/${u?.id || ''}`] = true;
    // });
    // await db.ref().update(update);

    const empTeams = [];
    const leaders = await Promise.all(
        empTeamIds.map((tid: string) => db.ref(`v2_teams`).orderByChild('referral_code').equalTo(tid).once('value')
            .then((ds: database.DataSnapshot) => (<any>Object.values(ds.val())[0]))
            .then((t: any) => {
                empTeams.push(t);
                return usersManager.getUserByPhone(t);
            })
        )
    );


    console.log(leaders);

};

function getUniq(arr: any[]): any[] {
    const red = arr.reduce((prev, cur) => { prev[cur] = true; return prev; }, {})
    return Object.keys(red);
}

async function fix() {
    const fixUsers = (await db.ref('users_to_fix_location').once('value')).val();
    const fixUserIds = ["79232302686"]; //Object.keys(fixUsers).filter(k => fixUsers[k] == true);
    let fixValues = await Promise.all(
        fixUserIds.map((id: string) => {
            return db.ref('v2_users').orderByChild('phone').equalTo(id).once('value').then(d => d.val())
        })
    );

    fixValues = fixValues.filter(v => !!v);
    fixValues = fixValues.map(v => Object.values(v).filter((d: any) => !!d.country)[0] || null);
    fixValues = fixValues.filter(v => !!v);

    const update: any = {};

    for (let i = 0; i < fixValues.length; i++) {
        const v = fixValues[i];
        const organization: any = getOrganizationInfo(v);
        const team = (await db.ref(TEAMS_KEY).orderByChild('leader').equalTo(v.phone).once('value')).val();

        update[`${TEAMS_KEY}/${Object.keys(team)[0]}/organization`] = organization;

        for (const key in organization) {
            if (Object.prototype.hasOwnProperty.call(organization, key)) {
                const val = organization[key];
                update[`${USERS_KEY}/${v.phone}/${key}`] = val;
            }
        }
        update[`${USERS_KEY}/${v.phone}/team`] = null;
        update[`users_to_fix_location/${v.phone}`] = false;
    }

    console.log(update);

    await db.ref().update(update);



    // duplicates.forEach((duplicate: any) => {
    //     if (!duplicate) {
    //         return;
    //     }
    //     const dupUsers = Object.values(duplicate);
    //     const user: any = dupUsers[dupUsers.length - 1];

    //     if (!user) {
    //         return;
    //     }
    //     const organization: any = this.getOrganizationInfo(user);
    //     const teamKey = teamOvj.filter((t: string) => (<any>t).leader == user.phone)[0];
    //     if (teamKey) {
    //         update[`${TEAMS_KEY}/${teamKey.id}/organization`] = organization;
    //     }

    //     for (const key in organization) {
    //         if (Object.prototype.hasOwnProperty.call(organization, key)) {
    //             const val = organization[key];
    //             update[`${USERS_KEY}/${user.id}/${key}`] = val;
    //         }
    //     }
    //     update[`${USERS_KEY}/${user.id}/team`] = null;
    // });

}

function getOrganizationInfo(userData: any) {
    const data = {
        city: userData.city || null,
        country: userData.country || null,
        location: userData.location || null,
        region: userData.region || null,
        school: userData.school || null,
        settlement: userData.settlement || null
    };

    if (Object.values(data).every(v => !v)) {
        return null;
    }

    return data;
}

async function getDups() {
    // const dups = (await db.ref('duplicates/v2_users').once('value')).val();
    const dups = (await db.ref('v2_users').orderByChild('firstname').equalTo('Ильсия').once('value')).val();
    const phones = Object.values(dups).map((d: any) => d.phone).filter((s: string) => s.length != 11);
    console.log(phones);
}

async function uploadPromocodes() {
    const promocodes: string[] = [];

    const update: any = {};

    for (let i = 0; i < promocodes.length; i++) {
        const p = promocodes[i];
        update[`promocodes/v2_mybook/${p}`] = false;
    }

    console.log(update);

    await db.ref().update(update);


}

async function getUsers() {

    // const t = (await db.ref(TEAMS_KEY).orderByChild('referral_code').equalTo('5Q0ZQHV3OF8L978').once('value')).val();
    // const us = (await db.ref(USER_TO_TEAM_KEY).orderByValue().equalTo(Object.keys(t)[0]).once('value')).val();

    // const team = await teamsRepository.getTeamByCode('8I0GD08EUITIY3M');
    const userIds: string[] = ["797",
        "79039043626",
        "79069944258",
        "79132065446",
        "79133701141",
        "79133715427",
        "79133737115",
        "79134618399",
        "79137062431",
        "79137145018",
        "79137518333",
        "79137526035",
        "79137717924",
        "79137749152",
        "79137750183",
        "79139082089",
        "79231226312",
        "79231508306",
        "79231915528",
        "79232248826",
        "79232302686",
        "79232313299",
        "79607838808",
        "79618739649",
        "79628311458",
        "79639479744",
        "79658280770",
        "79833007218",
        "79963793365",
        "79963821636"];


    const users = await Promise.all(userIds.map(uid => db.ref(`${USERS_KEY}/${uid}`).once('value').then(d => d.val())));
    const usersBmIds = await Promise.all(userIds.map(uid => db.ref(`${PHONE_TO_BMID_KEY}/${uid}`).once('value').then(d => d.val())));


    const roles = Object.values(users).map((u: any) => `${u.phone} ${u.email} ${u.firstname} ${u.lastname} ${u.role}`);

    // const uniqueRoles = getUniq(roles);

    // const roleStats = Object.keys(EnvConfig.ROLE_LABELS).map(role => `${role} ${roles.filter(r => r == role).length}`)

    // console.log(roleStats);
}//C2W1T5

async function getTasks() {
    const userIds: string[] = [];
    const date = new Date(2021, 2, 1);
    const tasks = (await db.ref(`forms/results`).orderByChild('timestamp').startAt(date.getTime()).once('value')).val();

    // const parents = (await db.ref(USERS_KEY).orderByChild('role').equalTo('parent').once('value')).val();

    // const allUserTasks = await Promise.all(userIds.map(uid => db.ref(`forms/results`).orderByChild('user_id').equalTo(uid).once('value').then(d => d.val())));

    // const tasks = (await db.ref(`forms/results`).orderByChild('user_id').equalTo('79160410479').once('value')).val();

    const roles = Object.values(tasks).filter((t: any) => !t.canceled && t.form_name == 'C1W1T2'); //(t.form_name == 'C2W2T1')
    // const uniqueRoles = getUniq(roles);

    console.log(JSON.stringify(roles));

    console.log(tasks);
}

async function getStats() {

    const userIds = [];

    // usersManager.getAllUserBmIds()

    const tstats = (await db.ref('v2_team_stats').orderByChild('points/total').startAt(1500).once('value')).val();
    // const pstats = (await db.ref('v2_personal_stats').orderByChild('points/total').endAt(6).once('value')).val();

    const teamMembers: string[] = [];
    (<ITeamStats[]>Object.values(tstats)).filter((ts: ITeamStats) => ts.complete_tasks_codes.indexOf('C1W1T1') != -1).forEach((t: any) => teamMembers.push(...t.members));

    const allBmIds = await Promise.all(teamMembers.map(p => db.ref(`v2_phone_to_bmid/${p}`).once('value').then(d => d.val())));
    let allbmidsArray: string[] = [];

    allBmIds.filter(id => !!id).forEach(ids => allbmidsArray.push(...Object.keys(ids)));
    allbmidsArray = getUniq(allbmidsArray);


    let bunch = 0;

    while (allbmidsArray.slice(bunch, bunch + 1000).length) {
        const chunk = allbmidsArray.slice(bunch, bunch + 1000);

        // await callBotmotherWebhook('https://app.botmother.com/api/bot/action/PLazPqei9/CyByBBCb4BQCXBaBngDWCHDOKD8sD6CqBjyCWB4FcB1BKtCnDYgD4WBDgBiFC4CA', 'megachallenge', chunk, {});

        bunch += 1000;
    }


    console.log(tstats);
}

async function getTeamTasksAndRoles() {
    const userIds: string[] = [];

    const tasks = await statsManager.getTasks();
    const taskCodes = Object.keys(tasks).sort();

    const teamStats = await Promise.all(userIds.map(
        uid => db.ref(`${USER_TO_TEAM_KEY}/${uid}`)
            .once('value')
            .then(d => d.val())
            .then(teamId => statsManager.getTeamStats(teamId))
            .then((stats: ITeamStats | null) => { return { stats: stats, id: uid }; })
    ));

    const tasksRow = taskCodes.reduce((o: any, tc) => { o[tc] = tasks[tc].Condition; return o; }, { leader: null });

    const fullStats = await Promise.all(teamStats.filter(ts => !!ts.stats).map(
        ts => getRoles((<ITeamStats>ts.stats).members)
            .then((teamMembersRoles: string[]) => {
                const fullStats: any = { leader: ts.id, total_points: ts.stats?.points.total || '' };

                for (let i = 0; i < taskCodes.length; i++) {
                    const taskCode = taskCodes[i];
                    fullStats[taskCode] = (ts.stats && ts.stats.complete_tasks && ts.stats.complete_tasks[taskCode]) ? 'true' : '';
                }

                for (const roleKey in RSAEnvConfig.ROLE_LABELS) {
                    if (Object.prototype.hasOwnProperty.call(RSAEnvConfig.ROLE_LABELS, roleKey)) {

                        if (roleKey != 'director' && roleKey != 'notfromschool') {
                            fullStats['r_' + roleKey] = teamMembersRoles?.indexOf(roleKey) != -1 ? 'true' : '';
                        }

                    }
                }

                return fullStats;
            })
        // .then(fs => getTeamMembers(<ITeamStats>ts.stats).then((teamMembers => {
        //     fs['team_members'] = teamMembers;
        //     return fs;
        // })))
    ));

    const fullStatsWithTasks = [tasksRow, ...fullStats];

    console.log(fullStatsWithTasks);
    console.log(JSON.stringify(fullStatsWithTasks));

}

function getRoles(userIds: string[]) {
    return Promise.all(
        userIds.map(uid => db.ref(`${USERS_KEY}/${uid}/role`).once('value')
            .then(d => d.val()))
    );
}

async function getUserStatsByTracks() {
    const tstats = (await db.ref('v2_team_stats').orderByChild('points/total').startAt(900).once('value')).val();
    // const pstats = (await db.ref('v2_personal_stats').orderByChild('points/total').endAt(6).once('value')).val();

    const userIds: string[] = [];
    (<ITeamStats[]>Object.values(tstats)).forEach((t: any) => userIds.push(...t.members));

    const personalStats = await Promise.all(userIds.map(
        uid => statsManager.getPersonalStats(uid).then((stats: IPersonalStats | null) => { return { stats: <IPersonalStats>stats, id: uid }; })
            .then((ts: any) => usersManager.getUserByPhone(uid)
                .then(user => { ts['user'] = user; return ts; })
            )
    ));

    const tasksByRoles: { [key: string]: string[] } = {
        'teacher': ['C1W1T3', 'C2W1T3', 'C2W3T3', 'C3W2T2', 'C3W3T3'],
        'student': ['C1W1T4', 'C2W1T4', 'C2W3T4', 'C3W2T3', 'C3W3T4'],
        'exstudent': ['C1W1T5', 'C2W1T5', 'C2W3T5', 'C3W2T4', 'C3W3T5'],
        'parent': ['C1W1T6', 'C2W1T6', 'C2W3T6', 'C3W2T5', 'C3W3T6']
    }

    let completeChallengeStats = personalStats.filter((ps) => !!ps.stats && !!ps.user && ps.stats.complete_tasks_count)
        .filter(ps =>
            tasksByRoles[ps.user.role] && tasksByRoles[ps.user.role].every(code => ps.stats.complete_tasks_codes.indexOf(code) != -1)
        );

    const totalStats = [];

    completeChallengeStats = completeChallengeStats.map(ps => {
        const row: any = {};
        Object.assign(row, ps.user);
        delete row.location;

        row['total_points'] = ps.stats.points.total;
        row['total_tasks'] = ps.stats.complete_tasks_count;

        tasksByRoles[ps.user.role].forEach(code => row[code] = ps.stats.complete_tasks[code].value.value1);
        return row;
    });

    console.log()

    console.log(completeChallengeStats);

}

async function getTeamMembers(team_stats: ITeamStats) {
    const teamUsers = await Promise.all(team_stats.members.slice(0, 10).map(uid => usersManager.getUserShortInfo(uid)));
    const teamMembers = teamUsers.map(u => `${u.id} ${u.firstname.trim()} ${u.lastname.trim()} - ${RSAEnvConfig.ROLE_LABELS[u.role]}`).sort().join('\n');

    return teamMembers;
}

async function getTeamsGeo() {
    const tstats = (await db.ref('v2_team_stats').orderByChild('points/total').startAt(50).once('value')).val();

    await Promise.all((<string[]>Object.keys(tstats)).map((t: any) => db.ref(`${TEAMS_KEY}/${t}/organization`).once('value').then(d => d.val()).then(org => tstats[t].org = org)));

    const teams = [];
    for (const tid in tstats) {
        if (Object.prototype.hasOwnProperty.call(tstats, tid)) {
            const t: ITeamStats = tstats[tid];

            const tInfo = {
                "team_id": tid,
                "lat": (<any>t).org?.location?.settlement?.geo_lat || null,
                "lon": (<any>t).org?.location?.settlement?.geo_lon || null,
                "kladr_id": (<any>t).org?.location?.settlement?.kladr_id || null,
                "school": (<any>t).org?.school,
                "country": (<any>t).org?.country,
                "region": (<any>t).org?.region,
                "city": (<any>t).org?.city || (<any>t).org?.settlement || null,
                "points": t.points.total,
                "members_count": t.members.length,
                "tasks_count": t.complete_tasks_codes.length
            };

            teams.push(tInfo);
        }
    }

    console.log(teams);
}

// fix().then(() => { });
// test().then(() => { });
// getDups().then(() => { });
// uploadPromocodes().then(() => { });
// getUsers().then(() => { });
// getTasks().then(() => { });
// getStats().then(() => { });
// getUserStatsByTracks().then(() => { });
// getUserStatsByTracks().then(() => { });

getTeamsGeo().then(() => { });

// 633 - p
// 1076 - t
// 1259 - s
// 445 - a
// 203 - partner
// 364 - exstud
