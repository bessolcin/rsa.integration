import { Database, getDatabase } from 'firebase-admin/database';
import { initializeApp, cert, App } from 'firebase-admin/app';
import { RSAEnvConfig } from './envconfig';

export const firebaseApp: App = initializeApp({
    credential: cert(RSAEnvConfig.DB_SERVICE_ACCOUNT),
    databaseURL: RSAEnvConfig.DB_DATABASE_URL,
    storageBucket: RSAEnvConfig.CS_STORAGE_BUCKET_URL
});

export const db: Database = getDatabase();
