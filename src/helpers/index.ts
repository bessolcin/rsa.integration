import got from 'got';
import { Response } from 'got';

export * from './menu.helpers';
export * from './menu.hb.helpers';

import { IFormData } from '../models';

export async function createTaskTicket(ticketRequest: any): Promise<Response> {

    return <Response><unknown>await got.post('https://api.usedesk.ru/create/ticket',
        {
            json: ticketRequest,
            responseType: 'json'
        }
    );
}

export function getTicketMessage(formData: IFormData, recordKey: string, taskCondition: string) {
    return `Задание: ${formData.form_name}<br/>
Суть задания: ${taskCondition}<br/>
Ответ участника:<br/>
________________<br/>
${formData.form_value.value1}<br/>
${formData.form_value.value2 ? formData.form_value.value2 + '<br/>' : ''}
${formData.form_value.value3 ? formData.form_value.value3 + '<br/>' : ''}
${formData.form_value.value4 ? formData.form_value.value4 + '<br/>' : ''}
${formData.form_value.value5 ? formData.form_value.value5 + '<br/>' : ''}
${formData.form_value.value6 ? formData.form_value.value6 + '<br/>' : ''}
${formData.form_value.value7 ? formData.form_value.value7 + '<br/>' : ''}
________________<br/>
<br/>
<a href="${formData.dialog_url}" target="_blank">Ссылка на диалог в ботмаме</a><br/><br/>
<a href="https://europe-west1-coolstoryprointegration.cloudfunctions.net/rsaForms/form/results/cancel?res=${recordKey}" target="_blank">Снять кристаллы за задание</a><br/><br/>
Роль: ${formData.role}<br/>
Код команды: ${formData.team_code}<br/>
Мессенджер: ${formData.platform}<br/>
ИД в мессенджере: ${formData.platform_id}<br/>
UID: ${formData.user_id}<br/>
TID: ${formData.team_id}<br/>
BMID: ${formData.bm_id}<br/>`;
}

export const SOCIAL_DOMAINS = [
    'vk.com',
    'instagram.com',
    'tiktok.com',
    'facebook.com',
    'fb.com',
    'ok.ru',
    't.me',
    'zen.yandex.ru'
];
export function isSocialLink(str: string): boolean {
    return SOCIAL_DOMAINS.some((d: string) => str.indexOf(d) != -1)
}